SCA2Amalthea Headless Tool

Launch via executable jar (uses the bnd launcher)

    java -jar sca.jar <-pdir> [-astp] [-cfilesTxtPath] [-hdirlist] [-taskinfo] [-lockinfo] [-enableStructMember] [-xmlCallTreePath] [-outdir]

    Options:
      -cfilesTxtPath       (optional) path of the text file containing list of c file paths.

      -hdirlist            (optional) list of directory paths that contain header files
                                      The directory paths should be separated by the path separator ie ";"

      -taskinfo            (optional) path of the task/isr information file

      -lockinfo            (optional) path of a csv file containing information of lock functions

      -enableStructMember  (optional) boolean value for reporting struct member accesses in Amalthea model

      -xmlCallTreePath     (optional) path of the "XMLCallTree.xml" to be converted to Amalthea model

      -outdir              (optional) path of the directory where the Amalthea model has to be stored

    Mandatory Parameter:
      -pdir               (mandatory) path of the root directory of the C project

      -astp               (mandatory) path to the LLVM ASTParser executable (if xmlCallTreePath option is not provided)

 Examples:

    Displaying sca2Amalthea help
    java -jar sca.jar -h

    Generate Amalthea model for all source files in project directory
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir

    Generate Amalthea model for all source files in project directory with task information
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -taskinfo C:\\Temp\\project_dir\\task_info\\taskInfo.csv

    Generate Amalthea model from xmlCallTree.xml file
    java -jar sca.jar -pdir C:\\Temp\\project_dir -xmlCallTreePath C:\\Temp\\project_dir\\_gen\\sca2Amalthea\\XMLCallTree.xml

    Generate Amalthea model for all source files in project directory with struct members enabled
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -enableStructMember

    Generate Amalthea model for some c files and for some header directory paths
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -cfilesTxtPath C:\\Temp\\project_dir\\cfiles.txt -hdirlist C:\\Temp\\project_dir\\header_dir1;C:\\Temp\\project_dir\\header_dir2

    Generate Amalthea model for all source files in some output directory
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -outdir C:\\Desktop\\outdir

    Open the osgi shell
    java -jar -Dosgi.console= sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir