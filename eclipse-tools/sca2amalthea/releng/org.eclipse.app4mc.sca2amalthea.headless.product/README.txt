SCA2Amalthea Headless Tool

The sca2Amalthea headless tool can be started in different ways:

1. Launch via Equinox launcher executable, e.g. on Windows (sca2Amalthea.exe)

  sca2Amalthea <-pdir> [-astp] [-cfilesTxtPath] [-hdirlist] [-taskinfo] [-lockinfo] [-enableStructMember] [-xmlCallTreePath] [-outdir] [-consoleLog] [-console]

    Options:
      -cfilesTxtPath       (optional) path of the text file containing list of c file paths.

      -hdirlist            (optional) list of directory paths that contain header files
                                      The directory paths should be separated by the path separator ie ";"

      -taskinfo            (optional) path of the task/isr information file

      -lockinfo            (optional) path of a csv file containing information of lock functions

      -enableStructMember  (optional) boolean value for reporting struct member accesses in Amalthea model

      -xmlCallTreePath     (optional) path of the "XMLCallTree.xml" to be converted to Amalthea model

      -outdir              (optional) path of the directory where the Amalthea model has to be stored

      -h                   Shows the help

      -consoleLog          (optional) When using the launcher, the console outputs are not printed to the shell.
                                      By using this option a separate shell is opened to which the results are printed.

      -console             (optional) Opens the OSGi console. This option has to be provided in addition to the options listed above.

  Mandatory Parameter:
       -pdir              (mandatory) path of the root directory of the C project

       -astp              (mandatory) path to the LLVM ASTParser executable (if xmlCallTreePath option is not provided)

    Examples:

    Displaying sca2Amalthea help
    sca2Amalthea.exe -h -console

    Generate amalthea model for all source files in project directory
    sca2Amalthea.exe -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir

    Generate amalthea model for all source files in project directory with task information
    sca2Amalthea.exe -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -taskinfo C:\\Temp\\project_dir\\task_info\\taskInfo.csv

    Generate amalthea model from xmlCallTree.xml file
    sca2Amalthea.exe -pdir C:\\Temp\\project_dir -xmlCallTreePath C:\\Temp\\project_dir\\_gen\\sca2Amalthea\\XMLCallTree.xml

    Generate amalthea model for all source files in project directory with struct members enabled
    sca2Amalthea.exe -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -enableStructMember

    Generate amalthea model for some c files and with some header directory paths
    sca2Amalthea.exe -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -cfilesTxtPath C:\\Temp\\project_dir\\cfiles.txt -hdirlist C:\\Temp\\project_dir\\header_dir1;C:\\Temp\\project_dir\\header_dir2

    Generate amalthea model for all source files in some output directory
    sca2Amalthea.exe -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -outdir C:\\Desktop\\outdir


2. Launch via executable jar (uses the bnd launcher)

    java -jar sca.jar <-pdir> [-astp] [-cfilesTxtPath] [-hdirlist] [-taskinfo] [-lockinfo] [-enableStructMember] [-xmlCallTreePath] [-outdir]

    Options:
      -cfilesTxtPath       (optional) path of the text file containing list of c file paths.

      -hdirlist            (optional) list of directory paths that contain header files
                                      The directory paths should be separated by the path separator ie ";"

      -taskinfo            (optional) path of the task/isr information file

      -lockinfo            (optional) path of a csv file containing information of lock functions

      -enableStructMember  (optional) boolean value for reporting struct member accesses in Amalthea model

      -xmlCallTreePath     (optional) path of the "XMLCallTree.xml" to be converted to Amalthea model

      -outdir              (optional) path of the directory where the Amalthea model has to be stored

    Mandatory Parameter:
        -pdir             (mandatory) path of the root directory of the C project

        -astp             (mandatory) path to the LLVM ASTParser executable (if xmlCallTreePath option is not provided)


    Examples:

    Displaying sca2Amalthea help
    java -jar sca.jar -h

    Generate amalthea model for all source files in project directory
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir

    Generate amalthea model for all source files in project directory with task information
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -taskinfo C:\\Temp\\project_dir\\task_info\\taskInfo.csv

    Generate amalthea model from xmlCallTree.xml file
    java -jar sca.jar -pdir C:\\Temp\\project_dir -xmlCallTreePath C:\\Temp\\project_dir\\_gen\\sca2Amalthea\\XMLCallTree.xml

    Generate amalthea model for all source files in project directory with struct members enabled
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -enableStructMember

    Generate amalthea model some c file and for header directory paths
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -cfilesTxtPath C:\\Temp\\project_dir\\cfiles.txt -hdirlist C:\\Temp\\project_dir\\header_dir1;C:\\Temp\\project_dir\\header_dir2

    Generate amalthea model for all source files in some output directory
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -outdir C:\\Desktop\\outdir

    Open the osgi shell
    java -jar -Dosgi.console= sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir


3.  Launch via Equinox Framework JAR

  java -jar plugins/org.eclipse.osgi_3.15.100.v20191114-1701.jar -configuration ./configuration <-pdir> [-astp] [-cfilesTxtPath] [-hdirlist] [-taskinfo] [-lockinfo] [-enableStructMember] [-xmlCallTreePath] [-outdir]

   Options:
       -cfilesTxtPath      (optional) path of the text file containing list of c file paths.

      -hdirlist            (optional) list of directory paths that contain header files
                                      The directory paths should be separated by the path separator ie ";"

      -taskinfo            (optional) path of the task/isr information file

      -lockinfo            (optional) path of a csv file containing information of lock functions

      -enableStructMember  (optional) boolean value for reporting struct member accesses in Amalthea model

      -xmlCallTreePath     (optional) path of the "XMLCallTree.xml" to be converted to Amalthea model

      -outdir              (optional) directory where the Amalthea model has to be stored

    Mandatory Parameter:
        -pdir             (mandatory) path of the root directory of the c project.

        -astp             (mandatory) path to the LLVM ASTParser executable (if xmlCallTreePath option is not provided)

        -configuration<location>
                            This parameter is needed to specify the location of the config.ini file.
                            When starting the via Equinox Framework JAR the location needs to be provided this way to startup correctly.


    Examples:

    Displaying sca2Amalthea help
    java -jar sca.jar -h

    Generate amalthea model for all source files in project directory
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir

    Generate amalthea model for all source files in project directory with task information
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -taskinfo C:\\Temp\\project_dir\\task_info\\taskInfo.csv

    Generate amalthea model from xmlCallTree.xml file
    java -jar sca.jar -pdir C:\\Temp\\project_dir -xmlCallTreePath C:\\Temp\\project_dir\\_gen\\sca2Amalthea\\XMLCallTree.xml

    Generate amalthea model for all source files in project directory with struct members enabled
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -enableStructMember

    Generate amalthea model for some c files and for some header directory paths
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -cfilesTxtPath C:\\Temp\\project_dir\\cfiles.txt -hdirlist C:\\Temp\\project_dir\\header_dir1;C:\\Temp\\project_dir\\header_dir2

    Generate amalthea model for all source files in some output directory
    java -jar sca.jar -pdir C:\\Temp\\project_dir -astp C:\\Desktop\\ast_dir -outdir C:\\Desktop\\outdir