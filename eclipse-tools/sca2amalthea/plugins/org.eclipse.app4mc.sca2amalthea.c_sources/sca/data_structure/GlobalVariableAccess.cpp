/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
*/  
#include "GlobalVariableAccess.h"

GlobalVariableAccess::GlobalVariableAccess(GlobalVariable* globalVariable, std::string accessType, unsigned line, unsigned col, unsigned basicBlockID){
	this->globalVariable = globalVariable;
	this->accessType = accessType;
	this->line = line;
	this->col = col;
	this->basicBlockID = basicBlockID;
}

GlobalVariable* GlobalVariableAccess::getGlobalVariable(){
	return this->globalVariable;
}
std::string GlobalVariableAccess::getAccessType(){
	return this->accessType;
}
unsigned GlobalVariableAccess::getLine(){
	return this->line;
}
unsigned GlobalVariableAccess::getColumn(){
	return this->col;
}

unsigned GlobalVariableAccess::getBasicBlockID(){
	return this->basicBlockID;
}

void GlobalVariableAccess::setGlobalVariable(GlobalVariable* gA){
	this->globalVariable = gA;
}
