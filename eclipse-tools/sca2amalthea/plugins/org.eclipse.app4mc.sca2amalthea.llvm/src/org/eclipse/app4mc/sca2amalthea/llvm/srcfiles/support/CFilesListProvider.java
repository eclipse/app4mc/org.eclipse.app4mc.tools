/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.sca2amalthea.llvm.srcfiles.support;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.sca.logging.manager.Logmanager;
import org.eclipse.app4mc.sca.logging.util.LogInformation;
import org.eclipse.app4mc.sca.util.util.SCAUtils;
import org.eclipse.app4mc.sca2amalthea.llvm.Activator;

/**
 * This class parses the c files text file and generates a list of c files to be parsed.
 *
 */
public class CFilesListProvider implements ISourceFileListProvider {

  private final String cfilesListPath;
  private final String pverPath;

  /**
   * Constructor
   *
   * @param cfilesListPath path to the c files text file.
   * @param projectPath path to the root directory of the PVER (project)
   */
  public CFilesListProvider(final String cfilesListPath, final String projectPath) {
    super();
    this.cfilesListPath = cfilesListPath;
    this.pverPath = projectPath;
  }

  /**
   * This method generates the list of c-Files from the c files list text file
   */
  @Override
  public List<String> generateSourceFileList() {
    Set<String> cfileList = new LinkedHashSet<String>();

    try {
      BufferedReader inFileBuffer = new BufferedReader(new FileReader(this.cfilesListPath));
      String line;
      while ((line = inFileBuffer.readLine()) != null) {
        if(SCAUtils.isAbsolutePath(line)) {
        	addFilePath(cfileList, line);
        }
        else {
        	addFilePath(cfileList, this.pverPath + "/" +line);
        }
      }
      inFileBuffer.close();
    }
    catch (IOException e) {
      Logmanager.getInstance().logException(this.getClass(), e, Activator.PLUGIN_ID);
      return new ArrayList<String>();
    }
    return new ArrayList<String>(cfileList);
  }

private void addFilePath(Set<String> cfileList, String line) {
	if(new File(line).exists()) {
		cfileList.add(line);
	}
	else {
		Logmanager.getInstance().log(new LogInformation("PATH-ERROR", org.eclipse.app4mc.sca.logging.manager.LogFactory.Severity.ERROR, "Path of the c file "+line+" does not exist",this.getClass()));
	}
}

}
