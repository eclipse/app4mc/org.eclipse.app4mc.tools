/**
 ******************************************************************************** Copyright (c) 2017-2020 Robert Bosch GmbH and others. This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0 which is available at https://www.eclipse.org/legal/epl-2.0/
 * SPDX-License-Identifier: EPL-2.0 Contributors: Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.sca2amalthea.llvm.util;


/**
 * Constants for all the LLVM operations
 */
public final class SCA2AmaltheaHeadlessConstants {

  /**
   * Private constructor
   */
  private SCA2AmaltheaHeadlessConstants() {
    // Private constructor
  }

  /**
  *
  */
  public static final String PDIR = "-pdir";
  /**
   *
   */
  public static final String TASKINFO = "-taskinfo";
  /**
   *
   */
  public static final String LOCKINFO = "-lockinfo";
  /**
   *
   */
  public static final String OUTDIR = "-outdir";
  /**
   *
   */
  public static final String HDIR_LIST = "-hdirlist";
  /**
   *
   */
  public static final String ASTP = "-astp";
  /**
   *
   */
  public static final String CFILES_LST_TEXT_PATH = "-cfilesTxtPath";
  /**
  *
  */
  public static final String DLIST = "-dlist";

  /**
   *
   */
  public static final String OUT_FOLDER = "-out";

  /**
   *
   */
  public static final String ENABLE_STRUCT_MEMBER = "-enableStructMember";

  /**
   *
   */
  public static final String XMLCALLTREE_PATH = "-xmlCallTreePath";

  /**
   *
   */
  public static final String HELP = "-h";

  /**
   *
   */
  public static final String C_PROJECT_DIR_HELP =
      "(Mandatory) path of the root directory of the C project";

  /**
  *
  */
  public static final String AST_PARSER_HELP =
      "(Mandatory) path to the LLVM ASTParser executable (if xmlCallTreePath option is not provided)";

  /**
   *
   */
  public static final String XMLCALLTREE_HELP =
      "(optional) path of the \"XMLCallTree.xml\" to be converted to Amalthea model";

  /**
  *
  */
  public static final String CFILES_TEXT_FILE_HELP = "(optional) path of the text file containing list of c file paths.";

  /**
  *
  */
  public static final String HDIR_LIST_HELP =
      "(optional) list of directory paths that contain header files.The directory paths should be separated by the path separator ie ;";


  /**
  *
  */
  public static final String TASKINFO_HELP =
      "(optional) path of the task/isr information file";

  /**
  *
  */
  public static final String LOCKINFO_HELP =
      "(optional) path of a csv file containing information of lock functions";

  /**
  *
  */
  public static final String ENABLE_STRUCT_MEMBER_HELP =
      "(optional) boolean value for reporting struct member accesses in Amalthea model";

  /**
  *
  */
  public static final String OUTDIR_HELP =
      "(optional) path of the directory where the Amalthea model has to be stored";

}
