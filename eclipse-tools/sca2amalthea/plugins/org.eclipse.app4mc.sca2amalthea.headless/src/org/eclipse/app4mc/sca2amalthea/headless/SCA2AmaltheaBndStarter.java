/**
 ******************************************************************************** Copyright (c) 2017-2020 Robert Bosch GmbH and others. This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0 which is available at https://www.eclipse.org/legal/epl-2.0/
 * SPDX-License-Identifier: EPL-2.0 Contributors: Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.sca2amalthea.headless;

import java.util.Arrays;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class gets activated when the launcher arguments provided by the bnd launcher are available.This class is
 * responsible for handling the command line parameters when invoked through a executable jar generated using bnd
 * export.
 */
@Component(immediate = true)
public class SCA2AmaltheaBndStarter extends AbstractSCA2AmaltheaStarter {

  private String[] launcherArguments;

  @Reference(target = "(launcher.arguments=*)")
  void setLauncherArguments(final Object object, final Map<String, Object> launcherMap) {
    this.launcherArguments = (String[]) launcherMap.get("launcher.arguments");
  }

  /**
   * Extracts the command line arguments and processes them to generate a amalthea model.
   */
  @Activate
  public void runSCA2Amalthea() {
    try {
      String consoleProperty = System.getProperty("osgi.console");
      boolean isInteractive = (consoleProperty != null) && (consoleProperty.length() == 0);

      String[] args = Arrays.stream(this.launcherArguments)
          .filter(arg -> !"-console".equals(arg) && !"-consoleLog".equals(arg)).toArray(String[]::new);

      runSCA2Amalthea(args, isInteractive, false);

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}

