/**
 ******************************************************************************** Copyright (c) 2017-2020 Robert Bosch GmbH and others. This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0 which is available at https://www.eclipse.org/legal/epl-2.0/
 * SPDX-License-Identifier: EPL-2.0 Contributors: Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.sca2amalthea.headless;

import java.util.Arrays;

import org.eclipse.osgi.service.environment.EnvironmentInfo;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for handling the command line parameters when sca2Amalthea is invoked through the equinox
 * executable.
 */
@Component(immediate = true)
public class SCA2AmaltheaEquinoxStarter extends AbstractSCA2AmaltheaStarter {

  @Reference
  EnvironmentInfo environmentInfo;


  @Activate
  void activate() {
    if ((this.environmentInfo.getFrameworkArgs() != null) && (this.environmentInfo.getNonFrameworkArgs() != null)) {    	
    	boolean isThisAProduct =
				Arrays.asList(environmentInfo.getNonFrameworkArgs()).stream().anyMatch(arg->arg.equals("-product"))
				|| this.environmentInfo.getProperty("eclipse.product") != null;
		boolean isThisApplication =
				Arrays.asList(environmentInfo.getNonFrameworkArgs()).stream().anyMatch(arg->arg.equals("-application"))
				|| this.environmentInfo.getProperty("eclipse.application") != null;
		
	  if(!isThisAProduct && !isThisApplication) {
		  boolean showConsoleLog =
          Arrays.stream(this.environmentInfo.getFrameworkArgs()).anyMatch(arg -> "-consoleLog".equals(arg));
		  boolean isInteractive =
          Arrays.stream(this.environmentInfo.getFrameworkArgs()).anyMatch(arg -> "-console".equals(arg));

		  String args[] = this.environmentInfo.getNonFrameworkArgs();
		  runSCA2Amalthea(args, isInteractive, showConsoleLog);
	  }
    }
  }
}
