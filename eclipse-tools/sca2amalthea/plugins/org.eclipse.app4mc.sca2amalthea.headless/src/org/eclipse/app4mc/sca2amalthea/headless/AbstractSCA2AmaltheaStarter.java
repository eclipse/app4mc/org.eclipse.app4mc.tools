/**
 ******************************************************************************** Copyright (c) 2017-2020 Robert Bosch GmbH and others. This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0 which is available at https://www.eclipse.org/legal/epl-2.0/
 * SPDX-License-Identifier: EPL-2.0 Contributors: Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.sca2amalthea.headless;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.app4mc.sca.logging.manager.LogFactory.Severity;
import org.eclipse.app4mc.sca.logging.manager.Logmanager;
import org.eclipse.app4mc.sca.logging.util.LogInformation;
import org.eclipse.app4mc.sca2amalthea.llvm.headless.GenerateAmaltheaModelFromLLVM;
import org.eclipse.app4mc.sca2amalthea.llvm.headless.SCA2AMALTHEAStarterProperties;
import org.eclipse.app4mc.sca2amalthea.llvm.util.SCA2AmaltheaHeadlessConstants;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import com.google.common.base.Strings;

/**
 * Common class for both the BndStarter and Equinox starter. The class parses the command line parameters and generates
 * amalthea model.
 */
public class AbstractSCA2AmaltheaStarter {

  private String pdir;
  private String astPath;
  private String cfilesListTxtPath;
  private String hdirList;
  private String taskInfo;
  private String lockInfo;
  private String enableStructMember;
  private String xmlCallTreePath;
  private String outDir;

  /**
   * Performs static code analysis and generates a amalthea model.
   *
   * @param args command line args provided by the user.
   * @param isInteractive if the OSGI console has to stay open.
   * @param showConsoleLog true if a seperate shell has to be opened and if it should stay open.
   */
  protected void runSCA2Amalthea(final String[] args, final boolean isInteractive, final boolean showConsoleLog) {
    boolean isParseCmdArgsSuccessfull = parseCommandLineParameters(args);
    if (isParseCmdArgsSuccessfull) {
      SCA2AMALTHEAStarterProperties sca2AmaltheaStarterProperties = new SCA2AMALTHEAStarterProperties();
      sca2AmaltheaStarterProperties.setProjectPath(this.pdir);
      sca2AmaltheaStarterProperties.getLlvmStarterProperties().setTraverseAstFile(this.astPath);
      sca2AmaltheaStarterProperties.setCFilesListTxtFilePath(this.cfilesListTxtPath);
      sca2AmaltheaStarterProperties.setHDirFilePath(this.hdirList);
      sca2AmaltheaStarterProperties.setTaskListFile(this.taskInfo);
      sca2AmaltheaStarterProperties.setLockinfoFile(this.lockInfo);
      sca2AmaltheaStarterProperties.setIsStructMemberEnabled(Boolean.parseBoolean(this.enableStructMember));
      sca2AmaltheaStarterProperties.setPathToxmlCallTree(this.xmlCallTreePath);
      sca2AmaltheaStarterProperties.setOutPutPath(this.outDir);
      boolean isValidationSuccessfull = validateInputParameters(sca2AmaltheaStarterProperties);
      try {
        if (isValidationSuccessfull) {
          IStatus status = new GenerateAmaltheaModelFromLLVM(sca2AmaltheaStarterProperties).run();
          if (status == Status.OK_STATUS) {
            logOnCmdLine("Amalthea model generated successfully");
          }
          else {
            logOnCmdLine("Amalthea model was not successfull.");
          }
        }
      }
      catch (Exception e) {
        logOnCmdLine("Exception while generating amalthea model " + e.getMessage());
      }
    }

    if (showConsoleLog && !isInteractive) {
      logOnCmdLine("Press Enter to exit the shell");
      try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
        reader.readLine();
      }
      catch (IOException e) {
        System.out.println("Error on waiting for user input " + e);
      }
    }
    if (!isInteractive) {
      System.exit(0);
    }
  }

  private boolean parseCommandLineParameters(final String args[]) {
    boolean parseSuccess = true;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals(SCA2AmaltheaHeadlessConstants.PDIR)) {
        i++;
        this.pdir = args[i];
      }
      else if (args[i].equals(SCA2AmaltheaHeadlessConstants.ASTP)) {
        i++;
        this.astPath = args[i];
      }
      else if (args[i].equals(SCA2AmaltheaHeadlessConstants.CFILES_LST_TEXT_PATH)) {
        i++;
        this.cfilesListTxtPath = args[i];
      }
      else if (args[i].equals(SCA2AmaltheaHeadlessConstants.HDIR_LIST)) {
        i++;
        this.hdirList = args[i];
      }
      else if (args[i].equals(SCA2AmaltheaHeadlessConstants.TASKINFO)) {
        i++;
        this.taskInfo = args[i];
      }
      else if (args[i].equals(SCA2AmaltheaHeadlessConstants.LOCKINFO)) {
        i++;
        this.lockInfo = args[i];
      }
      else if (args[i].equals(SCA2AmaltheaHeadlessConstants.ENABLE_STRUCT_MEMBER)) {
        this.enableStructMember = "True";
      }
      else if (args[i].equals(SCA2AmaltheaHeadlessConstants.XMLCALLTREE_PATH)) {
        i++;
        this.xmlCallTreePath = args[i];
      }
      else if (args[i].equals(SCA2AmaltheaHeadlessConstants.OUTDIR)) {
        i++;
        this.outDir = args[i];
      }
      else if (args[i].equals(SCA2AmaltheaHeadlessConstants.HELP)) {
       printHelp();
        parseSuccess = false;
        break;
      }
      else {
        logOnCmdLine("Invalid option " + args[i]);
        parseSuccess = false;
        break;
      }
    }
    return parseSuccess;
  }

private void printHelp(){

   System.out.println("java -jar sca.jar <-pdir> [-astp] [-cfilesTxtPath] [-hdirlist] [-taskinfo] [-lockinfo] [-enableStructMember] [-xmlCallTreePath] [-outdir]\n");
        System.out.println("Mandatory parameters:");
        System.out.println("\t"+ SCA2AmaltheaHeadlessConstants.PDIR + "  " + SCA2AmaltheaHeadlessConstants.C_PROJECT_DIR_HELP);
        System.out.println("\t"+ SCA2AmaltheaHeadlessConstants.ASTP + "  " + SCA2AmaltheaHeadlessConstants.AST_PARSER_HELP+"\n");
        System.out.println("Optional parameters:");
        System.out.println("\t"+ SCA2AmaltheaHeadlessConstants.CFILES_LST_TEXT_PATH + "       " + SCA2AmaltheaHeadlessConstants.CFILES_TEXT_FILE_HELP);
        System.out.println("\t"+SCA2AmaltheaHeadlessConstants.HDIR_LIST + "            " + SCA2AmaltheaHeadlessConstants.HDIR_LIST_HELP);
        System.out.println("\t"+SCA2AmaltheaHeadlessConstants.TASKINFO + "            " + SCA2AmaltheaHeadlessConstants.TASKINFO_HELP);
        System.out.println("\t"+SCA2AmaltheaHeadlessConstants.LOCKINFO + "            " + SCA2AmaltheaHeadlessConstants.LOCKINFO_HELP);
        System.out.println("\t"+SCA2AmaltheaHeadlessConstants.ENABLE_STRUCT_MEMBER + "  " +
            SCA2AmaltheaHeadlessConstants.ENABLE_STRUCT_MEMBER_HELP);
        System.out.println("\t"+SCA2AmaltheaHeadlessConstants.XMLCALLTREE_PATH + "     " + SCA2AmaltheaHeadlessConstants.XMLCALLTREE_HELP);
        System.out.println("\t"+SCA2AmaltheaHeadlessConstants.OUTDIR + "              " + SCA2AmaltheaHeadlessConstants.OUTDIR_HELP);
}
  private boolean validateInputParameters(final SCA2AMALTHEAStarterProperties properties) {
    boolean validationSuccess = true;
    if (properties != null) {
      if (Strings.isNullOrEmpty(properties.getProjectPath())) {
        logOnCmdLine("Manadatory argument is missing");
        printHelp();
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getProjectPath()) && !new File(properties.getProjectPath()).exists()) {
        logOnCmdLine("Project Path " + properties.getProjectPath() + " does not exist ");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getProjectPath()) &&
          !new File(properties.getProjectPath()).isDirectory()) {
        logOnCmdLine("Project Path " + properties.getProjectPath() + "  is not a directory");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getPathToxmlCallTree()) &&
          !new File(properties.getPathToxmlCallTree()).exists()) {
        logOnCmdLine("XMLCalltree Path " + properties.getPathToxmlCallTree() + " does not exist ");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getPathToxmlCallTree()) &&
          !new File(properties.getPathToxmlCallTree()).isFile()) {
        logOnCmdLine("XMLCalltree Path " + properties.getPathToxmlCallTree() + "  is not a file");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getPathToxmlCallTree()) &&
          !properties.getPathToxmlCallTree().endsWith(".xml")) {
        logOnCmdLine("XMLCalltree Path " + properties.getPathToxmlCallTree() + "  should have .xml extension");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getPathToxmlCallTree()) &&
          !new File(properties.getPathToxmlCallTree()).getName().equals("XMLCalltree.xml")) {
        logOnCmdLine("The xml file name should be XMLCalltree.xml");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getPathToxmlCallTree()) &&
          !Strings.isNullOrEmpty(properties.getTraverseAstFile())) {
        logOnCmdLine("-astp parameter should not be provided if xmlCallTreePath option is provided ");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getPathToxmlCallTree()) &&
          !Strings.isNullOrEmpty(properties.getcFilesListTxtFilePath())) {
        logOnCmdLine("-cfilesTxtPath parameter should not be provided if xmlCallTreePath option is provided ");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getPathToxmlCallTree()) &&
          !Strings.isNullOrEmpty(properties.gethDirFilePath())) {
        logOnCmdLine("-hdirlist parameter should not be provided if xmlCallTreePath option is provided ");
        validationSuccess = false;
      }
      else if (Strings.isNullOrEmpty(properties.getPathToxmlCallTree()) &&
          Strings.isNullOrEmpty(properties.getTraverseAstFile())) {
        logOnCmdLine("-astp parameter is mandatory");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getTraverseAstFile()) &&
          !new File(properties.getTraverseAstFile()).exists()) {
        logOnCmdLine("Ast Path " + properties.getTraverseAstFile() + "  does not exist");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getcFilesListTxtFilePath()) &&
          !new File(properties.getcFilesListTxtFilePath()).exists()) {
        logOnCmdLine("C Files List text file path " + properties.getcFilesListTxtFilePath() + " does not exist");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getcFilesListTxtFilePath()) &&
          !new File(properties.getcFilesListTxtFilePath()).isFile()) {
        logOnCmdLine("C Files List text file path " + properties.getcFilesListTxtFilePath() + "  is not a file");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getcFilesListTxtFilePath()) && !properties.getcFilesListTxtFilePath().endsWith(".txt")) {
        logOnCmdLine("C Files List text file " + properties.getcFilesListTxtFilePath() + " should have a .txt extension");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getTaskListFile()) &&
          !new File(properties.getTaskListFile()).exists()) {
        logOnCmdLine("Task info file path " + properties.getTaskListFile() + " does not exist ");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getTaskListFile()) &&
          !new File(properties.getTaskListFile()).isFile()) {
        logOnCmdLine("Task info file path " + properties.getTaskListFile() + " is not a file ");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getTaskListFile()) && !properties.getTaskListFile().endsWith(".csv")) {
        logOnCmdLine("Task info file path " + properties.getTaskListFile() + " should have a .csv extension");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getLockinfoFile()) &&
          !new File(properties.getLockinfoFile()).exists()) {
        logOnCmdLine("Lock info file path " + properties.getLockinfoFile() + " does not exist");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getLockinfoFile()) &&
          !new File(properties.getLockinfoFile()).isFile()) {
        logOnCmdLine("Lock info file path " + properties.getLockinfoFile() + " is not a file ");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getLockinfoFile()) && !properties.getLockinfoFile().endsWith(".csv")) {
        logOnCmdLine("Lock info file path " + properties.getLockinfoFile() + " should have a .csv extension");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getOutPutPath()) && !new File(properties.getOutPutPath()).exists()) {
        logOnCmdLine("Output directory path " + properties.getOutPutPath() + " does not exist");
        validationSuccess = false;
      }
      else if (!Strings.isNullOrEmpty(properties.getOutPutPath()) &&
          !new File(properties.getOutPutPath()).isDirectory()) {
        logOnCmdLine("Output directory path " + properties.getOutPutPath() + " is not a directory");
        validationSuccess = false;
      }
      if (!Strings.isNullOrEmpty(properties.gethDirFilePath())) {
        String[] pathList = properties.gethDirFilePath().split(File.pathSeparator);
        for (String path : pathList) {
          if (!new File(path).exists()) {
            logOnCmdLine("Header file path " + path + " does not exist");
            validationSuccess = false;
            break;
          }
        }
      }
    }
    else {
      validationSuccess = false;
    }
    return validationSuccess;
  }

  private void logOnCmdLine(final String message) {
    LogInformation logInformation = new LogInformation();
    logInformation.setMessage(message);
    logInformation.setSeverity(Severity.INFO);
    logInformation.setMessageId("DEFAULT_ID");
    logInformation.setLogClass(this.getClass());
    Logmanager.getInstance().logOnCommandLine(logInformation);
  }
}
