/**
 * *******************************************************************************
 *  Copyright (c) 2017-2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */ 

package org.eclipse.app4mc.sca2amalthea.ir.scair;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Label</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.sca2amalthea.ir.scair.Label#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.sca2amalthea.ir.scair.Label#getCat <em>Cat</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.sca2amalthea.ir.scair.scairPackage#getLabel()
 * @model
 * @generated
 */
public interface Label extends IdentifiableElement {
  /**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see org.eclipse.app4mc.sca2amalthea.ir.scair.scairPackage#getLabel_Type()
	 * @model required="true"
	 * @generated
	 */
  String getType();

  /**
	 * Sets the value of the '{@link org.eclipse.app4mc.sca2amalthea.ir.scair.Label#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
  void setType(String value);

  /**
	 * Returns the value of the '<em><b>Cat</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.app4mc.sca2amalthea.ir.scair.ETypeCategory}.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cat</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Cat</em>' attribute.
	 * @see org.eclipse.app4mc.sca2amalthea.ir.scair.ETypeCategory
	 * @see #setCat(ETypeCategory)
	 * @see org.eclipse.app4mc.sca2amalthea.ir.scair.scairPackage#getLabel_Cat()
	 * @model required="true"
	 * @generated
	 */
  ETypeCategory getCat();

  /**
	 * Sets the value of the '{@link org.eclipse.app4mc.sca2amalthea.ir.scair.Label#getCat <em>Cat</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cat</em>' attribute.
	 * @see org.eclipse.app4mc.sca2amalthea.ir.scair.ETypeCategory
	 * @see #getCat()
	 * @generated
	 */
  void setCat(ETypeCategory value);

} // Label
