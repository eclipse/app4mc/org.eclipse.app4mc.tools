/**
 * *******************************************************************************
 *  Copyright (c) 2017-2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */ 

package org.eclipse.app4mc.sca2amalthea.ir.scair;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IIdentifiable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.sca2amalthea.ir.scair.IIdentifiable#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.app4mc.sca2amalthea.ir.scair.IIdentifiable#getUniqueName <em>Unique Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.sca2amalthea.ir.scair.scairPackage#getIIdentifiable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IIdentifiable extends EObject {
  /**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.app4mc.sca2amalthea.ir.scair.scairPackage#getIIdentifiable_Name()
	 * @model
	 * @generated
	 */
  String getName();

  /**
	 * Sets the value of the '{@link org.eclipse.app4mc.sca2amalthea.ir.scair.IIdentifiable#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
  void setName(String value);

  /**
	 * Returns the value of the '<em><b>Unique Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Unique Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Unique Name</em>' attribute.
	 * @see #setUniqueName(String)
	 * @see org.eclipse.app4mc.sca2amalthea.ir.scair.scairPackage#getIIdentifiable_UniqueName()
	 * @model default="" id="true" transient="true" volatile="true" derived="true"
	 * @generated
	 */
  String getUniqueName();

  /**
	 * Sets the value of the '{@link org.eclipse.app4mc.sca2amalthea.ir.scair.IIdentifiable#getUniqueName <em>Unique Name</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unique Name</em>' attribute.
	 * @see #getUniqueName()
	 * @generated
	 */
  void setUniqueName(String value);

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\r\n\t\tString _name = this.getName();\r\n\t\tString _encode = this.encode(_name);\r\n\t\tString _plus = (_encode + \"?type=\");\r\n\t\tEClass _eClass = this.eClass();\r\n\t\tString _name_1 = _eClass.getName();\r\n\t\treturn (_plus + _name_1);\r\n'"
	 * @generated
	 */
  String computeUniqueName();

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @model strUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\r\n\t\t\tString expr= null;\r\n\t\t\tboolean _isNullOrEmpty = (str == null || str.length() == 0);\r\n\t\t\tif (_isNullOrEmpty) {\r\n\t\t\t\texpr = \"no-name-id\";\r\n\t\t\t}\r\n\t\t\telse {\r\n\t\t\t\tString _string = StandardCharsets.UTF_8.toString();\r\n\t\t\t\ttry {\r\n\t\t\t\t\texpr = URLEncoder.encode(str, _string);\r\n\t\t\t\t}\r\n\t\t\t\tcatch (UnsupportedEncodingException e) {\r\n\t\t\t\t\t// TODO Auto-generated catch block\r\n\t\t\t\t\te.printStackTrace();\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t\treturn expr;\r\n\t\t'"
	 * @generated
	 */
  String encode(String str);

} // IIdentifiable
