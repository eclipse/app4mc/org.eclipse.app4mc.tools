/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.emf.viewer.plantuml.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.eclipse.app4mc.emf.viewer.plantuml.builders.BuilderResult;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;

public final class DiagramCreator {

	private static final Logger LOGGER = LoggerFactory.getLogger(DiagramCreator.class);

	private DiagramCreator() {
		// empty default constructor for static helper class
	}
	
	public static String generateSVG(
			BuilderResult result, 
			String genFolder, 
			String fileNamePrefix,
			EObject selectedElement) {
		
		SourceStringReader reader = new SourceStringReader(result.getPlantUMLText());

		String genFileLocation = genFolder + "/" + fileNamePrefix;	// always use standard delimiter !

		// write plantuml file
		try (FileOutputStream fileOutputStream = new FileOutputStream(new File(genFileLocation + ".plantuml"));
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fileOutputStream));) {
			bw.write(result.getPlantUMLText());
		} catch (IOException e) {
			LOGGER.error("Error on PlantUML persistence", e);
		}

		// write SVG file
		File svgFile = new File(genFileLocation + ".svg");
		try (FileOutputStream fileOutputStream = new FileOutputStream(svgFile);) {
			reader.outputImage(fileOutputStream, new FileFormatOption(FileFormat.SVG));
		} catch (IOException e) {
			LOGGER.error("Error on SVG persistence", e);
		}

		if (selectedElement != null) {
			URI uri = selectedElement.eResource().getURI();
			String platformString = uri.toPlatformString(true);
			if (platformString != null) {
				IPath path = new Path(platformString);
				IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
				
				try {
					file.getParent().refreshLocal(2, new NullProgressMonitor());
				} catch (CoreException e) {
					LOGGER.error("Error on refreshing workspace folder", e);
				}
			}
		}
		
		return svgFile.getAbsolutePath();
	}
	
	public static String getGenFolder(String genFolder, Resource resource) {
		// extract the gen folder from the BuilderResult if no preference value is set
		if ((genFolder == null || genFolder.length() == 0) && resource != null) {
			URI uri = resource.getURI();
			String platformString = uri.toPlatformString(true);
			if (platformString != null) {
				IPath path = new Path(platformString);
				IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
				
				IContainer parentFolder = file.getParent();
				if (parentFolder != null) {
					// **** Find visualization folder (create if necessary)
					
					IFolder folder = parentFolder.getFolder(new Path(".emf_viewer_diagrams"));
					
					if (!folder.exists()) {
						try {
							folder.create(IResource.NONE, true, null);
						} catch (CoreException e) {
							LOGGER.error("Error on creating folder for generated diagrams.", e);
						}
					}
					
					genFolder = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString()
							+ folder.getFullPath().toString();
				}
			}
		}

		return genFolder;
	}

}
