/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */ 
package org.eclipse.app4mc.emf.viewer.plantuml.handlers;

import org.eclipse.app4mc.emf.viewer.plantuml.views.DiagramView;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectToolItem;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBarElement;

public class LinkWithEditorHandler {
	
	@Execute
	public void execute(@Active MPart activePart) {
		
		if (activePart.getObject() instanceof DiagramView) {
			for (MToolBarElement element : activePart.getToolbar().getChildren()) {
				if (element.getElementId().equals("org.eclipse.app4mc.emf.viewer.plantuml.directtoolitem.select")) {
					MDirectToolItem toolItem = (MDirectToolItem) element;
					
					DiagramView diagramView = (DiagramView) activePart.getObject();
					
					boolean isLinked = diagramView.isLinkedWithEditor();
					diagramView.setLinkedWithEditor(!isLinked);
					toolItem.setSelected(!isLinked);
				}
			}
		}
	}
}