/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.builders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class EClassAllReferencesBuilder extends AbstractPlantUMLBuilder {

	@Override
	public BuilderResult generatePlantUML(final EClass selectedObjClass) {

		resetCaches();

		final StringBuilder plantuml = new StringBuilder();

		plantuml.append("@startuml\n\n");
		
		plantuml.append("' Created by EClassAllReferencesBuilder (" + timestamp() + ")\n\n");

		plantuml.append("hide empty members\n\n");

		plantuml.append("skinparam class {\n");
		plantuml.append("  BackgroundColor<<selected>> Moccasin\n");
		plantuml.append("}\n\n");
		
		plantuml.append("' ===== Main Class =====\n\n");
		
		appendClass(plantuml, selectedObjClass, "selected", false, null, null);

		plantuml.append("\n' ===== Super Classes =====\n\n");

		populateSuperClasses(plantuml, selectedObjClass);

		plantuml.append("\n' ===== Sub Classes =====\n\n");

		populateSubClasses(plantuml, selectedObjClass, BuilderUtil.getSubClasses(selectedObjClass));

		plantuml.append("\n@enduml");

		return new BuilderResult(getId2ObjectMap(), plantuml.toString());
	}

	@Override
	public BuilderResult generatePlantUML(EObject eObject) {
		return generatePlantUML(eObject.eClass());
	}
	
	private void populateSuperClasses(final StringBuilder plantuml, final EClass eClass) {

		populateEClassReferences(plantuml, eClass);

		for (final EClass superClass : eClass.getESuperTypes()) {

			appendClass(plantuml, superClass, null, true, null, null);

			appendInheritance(plantuml, superClass, eClass, "#darkblue");

			populateSuperClasses(plantuml, superClass);
		}
	}

	private void populateSubClasses(final StringBuilder plantuml, final EClass eClass, final Set<EClass> subClasses) {

		for (final EClass subClass : subClasses) {

			if (subClass.getESuperTypes().contains(eClass)) {

				appendClass(plantuml, subClass, null, true, null, null);
				
				appendInheritance(plantuml, eClass, subClass, "#darkGreen");
				
				populateEClassContainerInfo(plantuml, subClass);

				populateSubClasses(plantuml, subClass, subClasses);
			}
		}
	}

	private void populateEClassReferences(final StringBuilder plantuml, final EClass selectedObjClass) {
		final List<EReference> result = new ArrayList<>();

		final Object searchScope = (selectedObjClass.eResource().getResourceSet() == null)
				? selectedObjClass.eResource()
				: selectedObjClass.eResource().getResourceSet();

		/*- getting all the elements from the selected objects EClass --> by supplying the EResource of the corresponding EClass (i.e. ecore or Xcore) */

		Collection<Setting> allRefSettings = Collections.emptyList();
		if (searchScope instanceof Resource) {
			allRefSettings = EcoreUtil.UsageCrossReferencer.find(selectedObjClass, (Resource) searchScope);
		} else if (searchScope instanceof ResourceSet) {
			allRefSettings = EcoreUtil.UsageCrossReferencer.find(selectedObjClass, (ResourceSet) searchScope);
		}

		if (allRefSettings.isEmpty()) {
			/*- no references found */
			
			appendClassifier(plantuml, selectedObjClass, true);
			
		} else {
			/*- references found -> fill the plantUMLBuffer with the appropriate contents */

			for (final EStructuralFeature.Setting refSetting : allRefSettings) {

				if (refSetting.getEObject() instanceof EReference
						&& !isElementContained(result, (EReference) refSetting.getEObject())) {
					result.add((EReference) refSetting.getEObject());
				}
			}

			for (final EReference eReference : result) {

				final EClass sourceClass = eReference.getEContainingClass();

				appendClassifier(plantuml, sourceClass, true);
				
				appendReference(plantuml, eReference, sourceClass, selectedObjClass);
			}
		}
	}

	private void populateEClassContainerInfo(final StringBuilder plantuml, final EClass selectedObjClass) {
		final List<EReference> result = new ArrayList<>();

		final Object searchScope = (selectedObjClass.eResource().getResourceSet() == null)
				? selectedObjClass.eResource()
				: selectedObjClass.eResource().getResourceSet();

		/*- getting all the elements from the selected objects EClass --> by supplying the EResource of the corresponding EClass (i.e. ecore or Xcore) */

		Collection<Setting> allRefSettings = Collections.emptyList();
		if (searchScope instanceof Resource) {
			allRefSettings = EcoreUtil.UsageCrossReferencer.find(selectedObjClass, (Resource) searchScope);
		} else if (searchScope instanceof ResourceSet) {
			allRefSettings = EcoreUtil.UsageCrossReferencer.find(selectedObjClass, (ResourceSet) searchScope);
		}

		if (allRefSettings.isEmpty()) {
			/*- no references found */

			appendClassifier(plantuml, selectedObjClass, true);

		} else {
			/*- references found -> fill the plantUMLBuffer with the appropriate contents */

			for (final EStructuralFeature.Setting refSetting : allRefSettings) {

				if (refSetting.getEObject() instanceof EReference) {
					final EReference eObject = (EReference) refSetting.getEObject();
					if (eObject.isContainment() && !isElementContained(result, eObject)) {
						result.add(eObject);
					}
				}
			}

			for (final EReference eReference : result) {
				
				final EClass sourceClass = eReference.getEContainingClass();

				appendClassifier(plantuml, sourceClass, true);
				
				appendReference(plantuml, eReference, sourceClass, selectedObjClass);	
			}
		}
	}

	private boolean isElementContained(final List<EReference> l, final EReference eRef) {
		boolean isElementFound = false;
		final Iterator<EReference> it = l.iterator();
		while (it.hasNext() && !isElementFound) {
			final EReference isContained = it.next();
			if (isContained.equals(eRef)) {
				isElementFound = true;
			}
		}
		return isElementFound;
	}

}
