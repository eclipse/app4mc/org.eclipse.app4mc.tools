/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.preferences;

import java.io.IOException;

import org.eclipse.app4mc.emf.viewer.plantuml.Activator;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPersistentPreferenceStore;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This class represents a preference page that is contributed to the Preferences dialog. By subclassing
 * <samp>FieldEditorPreferencePage</samp>, we can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the preference store that belongs to the main
 * plug-in class. That way, preferences can be accessed directly via the preference store.
 */

public class AmaltheaGraphicalRepresentationPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	public AmaltheaGraphicalRepresentationPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Preference page to specify the directory in which the diagrams should be generated.");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks needed to manipulate various
	 * types of preferences. Each field editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		addField(new DirectoryFieldEditor(PreferenceConstants.P_FOLDER_PATH,
				"&Path to the directory where SVG files shall be generated:", getFieldEditorParent()));

	}

	@Override
	public void init(final IWorkbench workbench) {
	}

	@Override
	protected void performApply() {
		super.performApply();

		IPreferenceStore preferenceStore = getPreferenceStore();

		if (preferenceStore instanceof IPersistentPreferenceStore) {
			try {
				((IPersistentPreferenceStore) preferenceStore).save();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}