/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.handlers;

import java.util.Iterator;

import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class CopyContentsHandler {

	@Execute
	public void execute(Shell shell,@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection) {
	
		Display display = shell.getDisplay();
		
		final Clipboard clipBoard = new Clipboard(display);

		StringBuilder stringBuilder = new StringBuilder();

		Iterator<?> iterator = selection.iterator();

		while (iterator.hasNext()) {
			Object next = iterator.next();

			if (next instanceof ENamedElement) {
				String name = ((ENamedElement) next).getName();
				stringBuilder.append(name);
				stringBuilder.append("\n");
			}
		}

		TextTransfer textTransfer = TextTransfer.getInstance();
 
		clipBoard.setContents(new Object[] {  stringBuilder.toString() },
		new Transfer[] { textTransfer });


	}

}
