/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.utils;

public class ExecutionUtil {

	private static ExecutionCategory category = ExecutionCategory.dummy;

	public static void setExecutionCategory(final ExecutionCategory category) {
		ExecutionUtil.category = category;
	}

	public static ExecutionCategory getCategory() {
		return category;
	}

	public static boolean isExecuting(final ExecutionCategory currentExecutionCategory) {

		return currentExecutionCategory == category;
	}
}
