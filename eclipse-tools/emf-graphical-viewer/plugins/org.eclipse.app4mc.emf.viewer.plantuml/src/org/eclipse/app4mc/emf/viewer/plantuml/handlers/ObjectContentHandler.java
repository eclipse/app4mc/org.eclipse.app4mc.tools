/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.handlers;

import java.lang.reflect.Type;

import javax.inject.Named;

import org.eclipse.app4mc.emf.viewer.plantuml.builders.AbstractPlantUMLBuilder;
import org.eclipse.app4mc.emf.viewer.plantuml.builders.ObjectContentBuilder;
import org.eclipse.app4mc.emf.viewer.plantuml.preferences.PreferenceConstants;
import org.eclipse.app4mc.emf.viewer.plantuml.utils.ExecutionCategory;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.di.extensions.Preference;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;

import net.sourceforge.plantuml.eclipse.utils.PlantumlConstants;

public class ObjectContentHandler extends AbstractPlantUMLHandler {
	
	@Override
	protected AbstractPlantUMLBuilder createPlantUMLBuilder(UISynchronize sync) {
		return new ObjectContentBuilder(sync); 
	}
	
	@Override
	protected ExecutionCategory getExecutionCategory() {
		return ExecutionCategory.dummy;
	}
	
	@Override
	public String getFileNamePrefix(Object eObject) {
		if (eObject instanceof EList) {
			EList<?> eList = (EList<?>) eObject;
			String typeString = "";
			if (!eList.isEmpty()) {
				Type[] genericInterfaces = eList.getClass().getGenericInterfaces();
				
				typeString = genericInterfaces.length > 0 ? genericInterfaces[0].getTypeName() : "";
				
				// replace generic information
				typeString = typeString.replaceAll("<[a-zA-Z]+>", "");
			}
			return "Collection" + "_" + typeString + "__object_Content";
		}
		return eObject.getClass().getName() + "__object_Content";
	}
	
	@Override
	@Execute
	public void execute(
			Shell shell,
			UISynchronize sync,
			@Optional
			@Preference(
					nodePath = "net.sourceforge.plantuml.eclipse", 
					value = PlantumlConstants.GRAPHVIZ_PATH) 
			String dotPath,
			@Optional
			@Preference(
					nodePath = "org.eclipse.app4mc.emf.viewer.plantuml", 
					value = PreferenceConstants.P_FOLDER_PATH) 
			String genFolderPath,
			@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
			EPartService partService) {
		
		Object element = selection.getFirstElement();
		if (element instanceof EObject) {
			execute(
					shell,
					sync,
					dotPath,
					genFolderPath,
					partService,
					element,
					(EObject)element);
		} else {
			openErrorDialog(shell);
		}
	}
}
