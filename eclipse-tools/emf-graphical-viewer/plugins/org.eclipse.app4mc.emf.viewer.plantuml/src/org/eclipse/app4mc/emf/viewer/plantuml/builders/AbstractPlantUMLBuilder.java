/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.builders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

public abstract class AbstractPlantUMLBuilder {

	private Map<String, Object> id2ObjectMap;
	private Map<Object, String> object2IdMap;
	private Set<Object> nodes;
	private Set<String> connections;

	public BuilderResult generatePlantUML(Object selectedObject) {
		if (selectedObject instanceof EClass) {
			return generatePlantUML((EClass) selectedObject);
		} else if (selectedObject instanceof EObject) {
			return generatePlantUML((EObject) selectedObject);
		}
		return null;
	}
	
	public abstract BuilderResult generatePlantUML(EClass selectedObjClass);
	
	public abstract BuilderResult generatePlantUML(EObject eObject);
	
	protected void resetCaches() {
		id2ObjectMap = null;
		object2IdMap = null;
		nodes = null;
		connections = null;
	}

	protected Map<String, Object> getId2ObjectMap() {
		if (id2ObjectMap == null) {
			id2ObjectMap = new HashMap<>();
		}
		return id2ObjectMap;
	}

	protected Map<Object, String> getObject2IdMap() {
		if (object2IdMap == null) {
			object2IdMap = new HashMap<>();
		}
		return object2IdMap;
	}

	protected String getIdForObject(Object obj) {
		if (getObject2IdMap().containsKey(obj)) {
			return getObject2IdMap().get(obj);
		}

		// create new UUID
		final String uuid = EcoreUtil.generateUUID();
		getId2ObjectMap().put(uuid, obj);
		getObject2IdMap().put(obj, uuid);

		return uuid;
	}

	protected Set<Object> getNodes() {
		if (nodes == null) {
			nodes = new HashSet<>();
		}
		return nodes;
	}

	protected Set<String> getConnections() {
		if (connections == null) {
			connections = new HashSet<>();
		}
		return connections;
	}

	protected void addNode(Object obj) {
		getNodes().add(obj);
	}

	protected void addConnection(String str) {
		getConnections().add(str);
	}

	protected String timestamp() {
		final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		return dateFormat.format(new Date());
	}

	protected void appendSkinParams(final StringBuilder plantuml) {
		plantuml.append("skinparam class {");
		plantuml.append("\n  BackgroundColor PaleGreen");
		plantuml.append("\n  ArrowColor SeaGreen");
		plantuml.append("\n  BorderColor SpringGreen");
		plantuml.append("\n  BackgroundColor<<H>> Wheat");
		plantuml.append("\n  BorderColor<<H>> Tomato");
		
		plantuml.append("\n  BackgroundColor<<SelectedElement>> DarkOrange ");
		plantuml.append("\n  BorderColor<<SelectedElement>> Tomato");
		plantuml.append("\n}");
		plantuml.append("\nskinparam stereotypeCBackgroundColor YellowGreen");
		plantuml.append("\nskinparam stereotypeCBackgroundColor<<H>> DimGray");
		plantuml.append("\n");
	}

	protected void appendInheritance(final StringBuilder plantuml, final EClass superClass, final EClass subClass, final String color) {
		
		String connector = superClass.getName() + " <|-- " + subClass.getName();
		String style = (color != null && color.length() > 0) ? ("  " + color) : "";
		
		appendConnection(plantuml, connector, style);
	}

	protected void appendAsContainment(final StringBuilder plantuml, final EStructuralFeature feature) {
		final EClass source = feature.getEContainingClass();
		final EClassifier target = feature.getEType();

		appendAsContainment(plantuml, feature, source, target);
	}

	protected void appendAsContainment(final StringBuilder plantuml, final EStructuralFeature feature, final EClass source, final EClassifier target) {
		
		final String cardinality = feature.isMany() ? " (0-*) " : "";
		final String refName = feature.getName();
		
		String connector = source.getName() + " *--> " + target.getName() + " : "  + refName + cardinality;
		
		appendConnection(plantuml, connector, "");
	}

	protected void appendReference(final StringBuilder plantuml, final EReference reference) {
		final EClass source = reference.getEContainingClass();
		final EClass target = (EClass) reference.getEType();

		appendReference(plantuml, reference, source, target);
	}

	protected void appendReference(final StringBuilder plantuml, final EReference ref, final EClass source, final EClass target) {
		
		final String relation = ref.isContainment() ? " *--> " : " --> ";
		final String lowerBound = ref.getLowerBound() == -1 ? "*" : Integer.toString(ref.getLowerBound());
		final String upperBound = ref.getUpperBound() == -1 ? "*" : Integer.toString(ref.getUpperBound());
		final String cardinality = " [" + lowerBound + ":" + upperBound + "]";
		final String refName = ref.getName();
	
		String connector = source.getName() + relation + target.getName() + " : "+ refName + cardinality ;
		
		appendConnection(plantuml, connector, "");
	}

	private void appendConnection(final StringBuilder plantuml, String connection, String style) {
		
		if (!getConnections().contains(connection)) {
			addConnection(connection); // mark as created
			
			plantuml.append(connection + style);
			plantuml.append("\n");
		}
	}

	protected void appendClassifier(final StringBuilder plantuml, final EClassifier eClassifier, final boolean withUUID) {

		if (!getNodes().contains(eClassifier)) {
			addNode(eClassifier); // mark as created
			
			plantuml.append("class \"" + eClassifier.getName() + "\"");
			
			if (withUUID) {
				final String uuid = getIdForObject(eClassifier);
				plantuml.append(" [[" + uuid + "]]");
			}

			plantuml.append("\n");
		}
	}

	protected void appendEnum(final StringBuilder plantuml, final EEnum eEnum) {

		if (!getNodes().contains(eEnum)) {
			addNode(eEnum); // mark as created
			
			plantuml.append("enum \"" + eEnum.getName() + "\"");
			plantuml.append(" {");

			for (final EEnumLiteral literal : eEnum.getELiterals()) {

				plantuml.append("\n  ");
				plantuml.append(literal.getName());
			}

			plantuml.append("\n}\n");
		}
	}

	protected void appendDataType(final StringBuilder plantuml, final EDataType eDataType) {

	}

	protected void appendClass(final StringBuilder plantuml, final EClass eClass, final String stereoType,
			final boolean withUUID, final EList<EAttribute> attributes, final EList<EReference> references) {

		if (!getNodes().contains(eClass)) {
			addNode(eClass); // mark as created
			
			appendClassHeader(plantuml, eClass, stereoType, withUUID);
			
			boolean withAttributes = attributes != null && !attributes.isEmpty();
			boolean withReferences = references != null && !references.isEmpty();
					
			if (withAttributes || withReferences) {
				plantuml.append(" {");
				
				if (withAttributes) {
					appendClassAttributes(plantuml, attributes);			
				}
				
				if (withReferences) {
					appendClassReferences(plantuml, references);			
				}
				
				plantuml.append("\n}");
			}
			
			plantuml.append("\n");
		}
	}

	private void appendClassHeader(final StringBuilder plantuml, final EClass eClass, final String stereoType, final boolean withUUID) {

		if (eClass.isAbstract() && eClass.isInterface()) {
			plantuml.append("interface \"" + eClass.getName() + "\"");
		} else if (eClass.isAbstract()) {
			plantuml.append("abstract \"" + eClass.getName() + "\"");
		} else if (eClass.isInterface()) {
			plantuml.append("interface \"" + eClass.getName() + "\"");
		} else {
			plantuml.append("class \"" + eClass.getName() + "\"");
		}

		if (stereoType != null) {
			plantuml.append("<<" + stereoType + ">>");
		}

		if (withUUID) {
			final String uuid = getIdForObject(eClass);
			plantuml.append(" [[" + uuid + "]]");
		}
	}

	private void appendClassAttributes(final StringBuilder plantuml, EList<EAttribute> attributeList) {
		
		/* listing attributes belonging to the specific EMF element */
		
		for (final EAttribute attribute : attributeList) {

			plantuml.append("\n  ");

			final String lowerBound = attribute.getLowerBound() == -1 ? "*" : attribute.getLowerBound() + "";
			final String upperBound = attribute.getUpperBound() == -1 ? "*" : attribute.getUpperBound() + "";

			plantuml.append(
					attribute.getEType().getName() + " "
					+ attribute.getName()
					+ " [" + lowerBound + ":" + upperBound + "]");
		}
	}
	
	private void appendClassReferences(final StringBuilder plantuml, EList<EReference> referenceList) {

		/* listing references belonging to the specific EMF element */

		for (final EReference reference : referenceList) {
			
				plantuml.append("\n  ");

				final String lowerBound = reference.getLowerBound() == -1 ? "*" : reference.getLowerBound() + "";
				final String upperBound = reference.getUpperBound() == -1 ? "*" : reference.getUpperBound() + "";

				plantuml.append(
						reference.getEType().getName() + " "
						+ reference.getName()
						+ " [" + lowerBound + ":" + upperBound + "]");
		}
	}

}


