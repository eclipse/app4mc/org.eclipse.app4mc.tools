/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.builders;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

public class EClassContentsAndHierarchyBuilder extends AbstractPlantUMLBuilder {

	@Override
	public BuilderResult generatePlantUML(final EClass selectedObjClass) {

		this.resetCaches();

		final StringBuilder plantuml = new StringBuilder();

		plantuml.append("@startuml\n\n");

		plantuml.append("' Created by EClassContentsAndHierarchyBuilder (" + timestamp() + ")\n\n");

		plantuml.append("hide empty members\n\n");

		appendSkinParams(plantuml);

		plantuml.append("\n' ===== Main Class =====\n\n");

		createClass(plantuml, selectedObjClass, "SelectedElement");

		plantuml.append("\n' ===== Super Classes =====\n\n");

		populateSuperClasses(plantuml, selectedObjClass);

		plantuml.append("\n' ===== Sub Classes =====\n\n");

		populateSubClassInfo(plantuml, selectedObjClass);

		plantuml.append("\n@enduml");

		return new BuilderResult(getId2ObjectMap(), plantuml.toString());
	}

	@Override
	public BuilderResult generatePlantUML(EObject eObject) {
		return generatePlantUML(eObject.eClass());
	}

	private void populateSubClassInfo(final StringBuilder plantuml, final EClass eClass) {

		for (final EClass subClass : BuilderUtil.getSubClasses(eClass)) {

			createClass(plantuml, subClass, "H");

			appendInheritance(plantuml, eClass, subClass, null);
		}
	}

	private void populateSuperClasses(final StringBuilder plantuml, final EClass eClass) {

		for (final EClass superClass : eClass.getESuperTypes()) {

			createClass(plantuml, superClass, "H");

			appendInheritance(plantuml, superClass, eClass, null);

			populateSuperClasses(plantuml, superClass);
		}
	}

	private void createClass(final StringBuilder plantuml, final EClass eClass, final String stereoType) {

		if (!getNodes().contains(eClass)) {

			boolean createUUID = !("SelectedElement".equals(stereoType));
			
			appendClass(plantuml, eClass, stereoType, createUUID, eClass.getEAttributes(), eClass.getEReferences());

			/*- managing EEnums belonging to the specific EMF element */

			for (final EAttribute attribute : eClass.getEAttributes()) {
				if (attribute.getEAttributeType() instanceof EEnum) {

					EEnum eEnum = (EEnum) attribute.getEType();
					
					appendEnum(plantuml, eEnum);

					appendAsContainment(plantuml, attribute, eClass, eEnum);
				}
			}

			/*- managing EReferences belonging to the specific EMF element */

			for (final EReference reference : eClass.getEReferences()) {
				if (reference.getEType() instanceof EClass) {

					EClass target = (EClass) reference.getEType();

					createClass(plantuml, target, null);

					appendReference(plantuml, reference, eClass, target);
				}
			}

			plantuml.append("\n");
		}
	}

}
