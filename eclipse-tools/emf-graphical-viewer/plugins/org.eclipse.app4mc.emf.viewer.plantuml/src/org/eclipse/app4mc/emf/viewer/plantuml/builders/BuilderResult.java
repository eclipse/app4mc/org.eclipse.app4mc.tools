/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.builders;

import java.util.HashMap;
import java.util.Map;

public class BuilderResult {
	
	private Map<String, Object> id2Object;
	private String plantUML;

	public BuilderResult(Map<String, Object> id2ObjectMap, String plantUMLText) {
		this.id2Object = new HashMap<>();
		if (id2ObjectMap != null && !id2ObjectMap.isEmpty()) {
			id2Object.putAll(id2ObjectMap);
		}
		this.plantUML = plantUMLText;
	}

	public Map<String, Object> getId2ObjectMap() {
		return id2Object;
	}

	public String getPlantUMLText() {
		return plantUML;
	}
}
