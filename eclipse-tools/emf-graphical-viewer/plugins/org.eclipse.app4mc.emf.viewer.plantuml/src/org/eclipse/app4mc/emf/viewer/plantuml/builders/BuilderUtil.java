/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.builders;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;

public class BuilderUtil {

	private BuilderUtil() {
		// empty default constructor for static helper class
	}

	public static Set<EClass> getSubClasses(final EClass eClass) {

		final Set<EClass> allSubClasses = new HashSet<>();

		final Resource eResource = eClass.eResource();
		if (eResource != null && eResource.getResourceSet() != null) {

			// Find the subclasses for the supplied EClass in all the packages belonging to the resource set

			for (final Resource resource : eResource.getResourceSet().getResources()) {
				for (EObject eObj : resource.getContents()) {
					if (eObj instanceof EPackage) {
						allSubClasses.addAll(getSubClasses(eClass, (EPackage) eObj));
					}
				}
			}

		} else {

			return getSubClasses(eClass, eClass.getEPackage());
		}

		return allSubClasses;
	}

	public static Set<EClass> getSubClasses(final EClass eClass, final EPackage ePackage) {
		final Set<EClass> allSubClasses = new HashSet<>();

		for (final EClassifier eClassifier : ePackage.getEClassifiers()) {

			if (eClassifier instanceof EClass
				&& eClass != eClassifier
				&& eClass.isSuperTypeOf((EClass) eClassifier)) {
					allSubClasses.add((EClass) eClassifier);
			}
		}
		return allSubClasses;
	}

}
