/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.viewer.plantuml.builders;

import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

public class EClassHierarchyBuilder extends AbstractPlantUMLBuilder {

	@Override
	public BuilderResult generatePlantUML(EClass selectedObjClass) {

		resetCaches();

		final StringBuilder plantuml = new StringBuilder();

		plantuml.append("@startuml\n\n");
		
		plantuml.append("' Created by EClassHierarchyBuilder (" + timestamp() + ")\n\n");

		plantuml.append("hide empty members\n\n");

		plantuml.append("skinparam class {\n");
		plantuml.append("  BackgroundColor<<selected>> Moccasin\n");
		plantuml.append("}\n\n");
		
		plantuml.append("' ===== Main Class =====\n\n");
		
		appendClass(plantuml, selectedObjClass, "selected", false, null, null);
		
		plantuml.append("\n' ===== Super Classes =====\n\n");

		populateSuperClasses(plantuml, selectedObjClass);

		plantuml.append("\n' ===== Sub Classes =====\n\n");

		populateSubClasses(plantuml, selectedObjClass, BuilderUtil.getSubClasses(selectedObjClass));

		plantuml.append("\n@enduml");

		return new BuilderResult(getId2ObjectMap(), plantuml.toString());
	}

	@Override
	public BuilderResult generatePlantUML(EObject eObject) {
		return generatePlantUML(eObject.eClass());
	}

	private void populateSuperClasses(final StringBuilder plantuml, final EClass eClass) {
		
		for (final EClass superClass : eClass.getESuperTypes()) {
	
			appendClass(plantuml, superClass, null, true, null, null);

			appendInheritance(plantuml, superClass, eClass, null);
	
			populateSuperClasses(plantuml, superClass);
		}
	}

	private void populateSubClasses(final StringBuilder plantuml, final EClass eClass, final Set<EClass> subClasses) {

		for (final EClass subClass : subClasses) {

			if (subClass.getESuperTypes().contains(eClass)) {

				appendClass(plantuml, subClass, null, true, null, null);
				
				appendInheritance(plantuml, eClass, subClass, null);

				populateSubClasses(plantuml, subClass, subClasses);
			}
		}
	}

}
