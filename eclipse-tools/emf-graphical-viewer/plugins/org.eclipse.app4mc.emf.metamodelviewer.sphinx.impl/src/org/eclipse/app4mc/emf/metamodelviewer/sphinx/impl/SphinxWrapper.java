/**
 ********************************************************************************
 * Copyright (c) 2017-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.sphinx.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.sphinx.emf.metamodel.IMetaModelDescriptor;
import org.eclipse.sphinx.emf.metamodel.MetaModelDescriptorRegistry;

import wrapper.ISphinxWrapper;

public class SphinxWrapper implements ISphinxWrapper {

	@Override
	public Object getMetaModelDescriptor(String id) {

		return MetaModelDescriptorRegistry.INSTANCE.getDescriptor(id);
	}

	@Override
	public List<String> getListOfMetaModelDescriptorIDs() {

		List<String> ls = new ArrayList<>();

		final List<IMetaModelDescriptor> descriptors = MetaModelDescriptorRegistry.INSTANCE
				.getDescriptors(MetaModelDescriptorRegistry.ANY_MM);

		for (final IMetaModelDescriptor iMetaModelDescriptor : descriptors) {
			final String identifier = iMetaModelDescriptor.getIdentifier();
			ls.add(identifier);
		}

		return ls;
	}

	@Override
	public Collection<EPackage> getListOfEpackagesFromMetaModelDescriptor(Object metaModelDescriptor) {

		if (isInstanceOfMetaModelDescriptor(metaModelDescriptor)) {
			return ((IMetaModelDescriptor) metaModelDescriptor).getEPackages();
		}
		return null;
	}

	@Override
	public boolean isInstanceOfMetaModelDescriptor(Object descriptor) {

		return (descriptor instanceof IMetaModelDescriptor);
	}

}
