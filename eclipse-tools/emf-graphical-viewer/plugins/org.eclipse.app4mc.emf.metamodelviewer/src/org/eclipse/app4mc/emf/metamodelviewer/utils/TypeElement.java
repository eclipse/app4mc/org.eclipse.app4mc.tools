/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.utils;

import org.eclipse.emf.ecore.EClassifier;

public class TypeElement {

	private EClassifier classifier;

	public TypeElement(final EClassifier classifier) {
		this.classifier = classifier;
	}

	public EClassifier getClassifier() {
		return this.classifier;
	}

	public void setClassifier(final EClassifier classifier) {
		this.classifier = classifier;
	}

}