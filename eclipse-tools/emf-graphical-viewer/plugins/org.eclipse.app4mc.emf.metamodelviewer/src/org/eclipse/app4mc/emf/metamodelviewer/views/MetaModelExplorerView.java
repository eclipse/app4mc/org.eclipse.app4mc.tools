/**
 ********************************************************************************
  * Copyright (c) 2017-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.views;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.emf.metamodelviewer.base.Activator;
import org.eclipse.app4mc.emf.metamodelviewer.dialogs.SelectedElementContentDialog;
import org.eclipse.app4mc.emf.metamodelviewer.preferences.MetaModelViewerPreferenceConstants;
import org.eclipse.app4mc.emf.metamodelviewer.utils.ElementSearchPatternFilter;
import org.eclipse.app4mc.emf.metamodelviewer.utils.PluginUtils;
import org.eclipse.app4mc.emf.metamodelviewer.utils.RegisterEcoreFileAction;
import org.eclipse.app4mc.emf.metamodelviewer.views.providers.ViewContentProvider;
import org.eclipse.app4mc.emf.metamodelviewer.views.providers.ViewLabelProvider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.FilteredTree;
import org.eclipse.ui.part.ViewPart;

import wrapper.ISphinxWrapper;

public class MetaModelExplorerView extends ViewPart {
	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.eclipse.app4mc.emf.metamodelviewer.views.Explorer";
	private static int viewCounter = 0;
	private TreeViewer viewer;
	private Action filterClassesAction;
	private Action registerEcoreFilesAction;
	private Action filterAttributesAction;
	private Action cloneViewAction;
	private boolean filterForEClassifiersBoolean = true;
	private boolean filterForAttributesBoolean;
	private ElementSearchPatternFilter patternFilter;
	private ComboViewer ecoreCombo;

	private ISphinxWrapper sphinxWrapper;

	private static int getNewViewCounter() {
		viewCounter++;
		return viewCounter;
	}

	@Override
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
		sphinxWrapper = PluginUtils.getObjectOfSphinxWrapper();
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize it.
	 */
	@Override
	public void createPartControl(final Composite parent) {
		parent.setLayout(new GridLayout(2, false));

		final Label sphinxLabel = new Label(parent, 0);
		final Combo sphinxCombo = new Combo(parent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.READ_ONLY);

		Label ecoreLabel = new Label(parent, 0);
		ecoreLabel.setText("ECORE Registry");
		ecoreLabel.setLayoutData(new GridData());

		ecoreCombo = new ComboViewer(parent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.READ_ONLY);

		final Registry ePackageRegistry = EPackage.Registry.INSTANCE;
		final Set<String> keySet = ePackageRegistry.keySet();
		ArrayList<String> arrayList = new ArrayList<>(keySet);
		Collections.sort(arrayList);

		ecoreCombo.setContentProvider(ArrayContentProvider.getInstance());
		ecoreCombo.setLabelProvider(new LabelProvider());
		ecoreCombo.setInput(arrayList.toArray());

		ecoreCombo.getCombo().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				sphinxCombo.deselectAll();

				final String text = ecoreCombo.getCombo().getText();
				final EPackage ePackage = ePackageRegistry.getEPackage(text);
				getViewer().setInput(ePackage);
			}
		});

		if (sphinxWrapper == null) {
			// hide Sphinx row
			hideControl(sphinxLabel);
			hideControl(sphinxCombo);
		} else {
			// initialize
			sphinxLabel.setText("SPHINX Registry");
			sphinxLabel.setLayoutData(new GridData());

			for (final String identifier : sphinxWrapper.getListOfMetaModelDescriptorIDs()) {
				sphinxCombo.add(identifier);
			}

			sphinxCombo.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					ecoreCombo.getCombo().deselectAll();
					
					final String text = sphinxCombo.getText();
					
					if (sphinxWrapper != null) {
						final Object descriptor = sphinxWrapper.getMetaModelDescriptor(text);
						getViewer().setInput(descriptor);
					}
				}
			});
		}

		this.patternFilter = new ElementSearchPatternFilter(this.filterForEClassifiersBoolean,
				this.filterForAttributesBoolean);

		final FilteredTree filteredTree = new FilteredTree(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER,
				this.patternFilter, true, false);

		filteredTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 0));

		this.viewer = filteredTree.getViewer();

		final GridData gridLayoutData = new GridData();
		gridLayoutData.horizontalSpan = 2;
		gridLayoutData.grabExcessHorizontalSpace = true;
		gridLayoutData.grabExcessVerticalSpace = true;
		gridLayoutData.horizontalAlignment = GridData.FILL;
		gridLayoutData.verticalAlignment = GridData.FILL;

		this.viewer.getTree().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 0));
		this.viewer.setContentProvider(new ViewContentProvider(sphinxWrapper));
		this.viewer.setInput(getViewSite());
		this.viewer.setLabelProvider(new ViewLabelProvider());

		getSite().setSelectionProvider(this.viewer);

		makeActions();

		hookContextMenu();

		hookDoubleClickAction();

		contributeToActionBars();
	}

	private void hideControl(Control control) {

		control.setVisible(false);

		GridData data = (GridData) control.getLayoutData();
		if (data == null) {
			data = new GridData();
		}

		data.exclude = true;
		control.setLayoutData(data);
		control.setSize(new Point(0, 0));
	}

	public TreeViewer getViewer() {
		return this.viewer;
	}

	private void hookContextMenu() {
		final MenuManager menuMgr = new MenuManager("#PopupMenu") {
			@Override
			public IContributionItem[] getItems() {
				IContributionItem[] menuItems = super.getItems();

				final List<IContributionItem> filteredContributionItems = new LinkedList<>();

				for (final IContributionItem contributionItem : menuItems) {
					if ((contributionItem != null) && ((contributionItem.getId() != null) && (!(contributionItem.getId()
							.startsWith("org.eclipse.emf.ecore.editor.CreateDynamicInstance"))))) {
						filteredContributionItems.add(contributionItem);
					}
				}

				menuItems = new IContributionItem[filteredContributionItems.size()];

				return filteredContributionItems.toArray(menuItems);
			}
		};

		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(MetaModelExplorerView.this::fillContextMenu);

		final Menu menu = menuMgr.createContextMenu(this.viewer.getControl());
		this.viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, this.viewer);
	}

	private void contributeToActionBars() {
		final IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(final IMenuManager manager) {
		manager.add(this.filterClassesAction);
		manager.add(new Separator());
		manager.add(this.filterAttributesAction);
		manager.add(new Separator());
		manager.add(this.cloneViewAction);
	}

	void fillContextMenu(final IMenuManager manager) {
		// manager.add(this.filterClassesAction);
		// manager.add(this.filterAttributesAction);
		// manager.add(new Separator());
		// this.drillDownAdapter.addNavigationActions(manager);
		// Other plug-ins can contribute there actions here
		// manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalToolBar(final IToolBarManager manager) {
		manager.add(this.filterClassesAction);
		manager.add(this.filterAttributesAction);
		manager.add(new Separator());

		manager.add(this.registerEcoreFilesAction);
	}

	private void makeActions() {
		this.filterClassesAction = new Action("Filter for EClassifers", IAction.AS_CHECK_BOX) {
			@Override
			public void run() {
				setFilterForEClassifiersBoolean(isChecked());
			}
		};

		this.filterClassesAction.setEnabled(true);

		this.filterClassesAction.setToolTipText("Filter for EClassifiers");
		this.filterClassesAction.setImageDescriptor(PluginUtils.getImageDescriptor("icons/class_obj.png"));

		this.filterAttributesAction = new Action("Filter for Attributes", IAction.AS_CHECK_BOX) {
			@Override
			public void run() {
				setFilterForAttributesBoolean(isChecked());
			}
		};
		this.filterAttributesAction.setToolTipText("Filter for Attributes");
		this.filterAttributesAction.setImageDescriptor(PluginUtils.getImageDescriptor("icons/prop_ps.gif"));

		this.cloneViewAction = new Action("Clone View", IAction.AS_CHECK_BOX) {
			@Override
			public void run() {
				try {
					getViewSite().getPage().showView(ID, "" + getNewViewCounter(), IWorkbenchPage.VIEW_VISIBLE);
				} catch (PartInitException e) {
					e.printStackTrace();
				}
			}
		};
		this.cloneViewAction.setToolTipText("Clone View");
		this.cloneViewAction.setImageDescriptor(PluginUtils.getImageDescriptor("icons/prop_ps.gif"));

		this.registerEcoreFilesAction = new Action("Register ECORE files") {
			@Override
			public void run() {

				FileDialog dialog = new FileDialog(Display.getDefault().getActiveShell(), SWT.OPEN | SWT.MULTI);
				dialog.setFilterExtensions(new String[] { "*.ecore" });
				dialog.open();

				if ((dialog.getFileName() == null) || (dialog.getFileNames() == null)) {
					return;
				}

				List<String> files = new ArrayList<>();
				for (String fileName : dialog.getFileNames()) {
					String filePath = dialog.getFilterPath() + File.separator + fileName;
					files.add(filePath);
				}

				List<String> keyList = Collections.emptyList();
				try {
					keyList = RegisterEcoreFileAction.loadEcoreFiles(files);
				} catch (Exception e) {
					Display.getDefault().asyncExec(() -> MessageDialog.openError(Display.getDefault().getActiveShell(),
							"Error", e.getMessage()));
				}
				ecoreCombo.setInput(keyList.toArray());
			}
		};
		this.registerEcoreFilesAction.setToolTipText("Register ECORE files");
		this.registerEcoreFilesAction.setImageDescriptor(PluginUtils.getImageDescriptor("icons/register_ecores.png"));
	}

	private void hookDoubleClickAction() {
		this.viewer.addDoubleClickListener(event -> {
			final ISelection selection = event.getSelection();

			if (selection instanceof TreeSelection) {
				final Object firstElement = ((TreeSelection) selection).getFirstElement();

				if (firstElement instanceof EObject) {
					Shell parentShell = null;

					String displayDialogStyle = (Activator.getDefault().getPreferenceStore()
							.getString(MetaModelViewerPreferenceConstants.P_DIALOG_DISPLAY));

					if ((displayDialogStyle != null)
							&& displayDialogStyle.equals(MetaModelViewerPreferenceConstants.V_ON_TOP)) {
						parentShell = new Shell(SWT.ON_TOP);
					} else {
						parentShell = new Shell();
					}

					// final String name = ((firstElement instanceof EClass) ? ((EClass)
					// firstElement).getName() :
					// "");
					//
					// parentShell.setText("Details of : " + ((EClass)
					// firstElement).eClass().getName() + " " +
					// name);
					//
					//
					// final TaskBar systemTaskBar = parentShell.getDisplay().getSystemTaskBar();
					// final TaskItem taskItem = systemTaskBar.getItem(parentShell);
					//
					// taskItem.setData(parentShell);
					// taskItem.setOverlayText(name);
					// taskItem.setText(name);
					new SelectedElementContentDialog(parentShell, getSite(), (EObject) firstElement).open();
				}
			}
		});
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		this.viewer.getControl().setFocus();
	}

	public Action getFilterClassesAction() {
		return this.filterClassesAction;
	}

	public Action getFilterAttributesAction() {
		return this.filterAttributesAction;
	}

	public boolean isFilterForEClassifiersBoolean() {
		return this.filterForEClassifiersBoolean;
	}

	public boolean isFilterForAttributesBoolean() {
		return this.filterForAttributesBoolean;
	}

	public void setFilterClassesAction(final Action filterClassesAction) {
		this.filterClassesAction = filterClassesAction;
	}

	public void setFilterAttributesAction(final Action filterAttributesAction) {
		this.filterAttributesAction = filterAttributesAction;
	}

	public void setFilterForEClassifiersBoolean(final boolean filterForEClassifiersBoolean) {
		this.filterForEClassifiersBoolean = filterForEClassifiersBoolean;

		this.patternFilter.setFilterForEClassifiersBoolean(filterForEClassifiersBoolean);
	}

	public void setFilterForAttributesBoolean(final boolean filterForAttributesBoolean) {
		this.filterForAttributesBoolean = filterForAttributesBoolean;

		this.patternFilter.setFilterForAttributesBoolean(filterForAttributesBoolean);
	}

}
