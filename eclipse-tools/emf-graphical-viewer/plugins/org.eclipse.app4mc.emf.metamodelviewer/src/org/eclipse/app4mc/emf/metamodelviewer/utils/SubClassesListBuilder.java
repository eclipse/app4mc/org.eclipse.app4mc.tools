/**
 ********************************************************************************
 * Copyright (c) 2017-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

public class SubClassesListBuilder {

	private final EClass eClass;

	public SubClassesListBuilder(final EClass eClass) {
		this.eClass = eClass;
	}

	private boolean isCalculated;

	final Set<EClass> allSubClasses = new HashSet<>();

	public Set<EClass> getAllSubClasses() {

		if (!this.isCalculated) {
			final List<EClass> eAllSuperTypes = getSubClasses(this.eClass);

			this.allSubClasses.addAll(eAllSuperTypes);

			for (final EClass subClass : eAllSuperTypes) {
				final SubClassesListBuilder classList = new SubClassesListBuilder(subClass);

				this.allSubClasses.addAll(classList.getAllSubClasses());

			}

		}

		return this.allSubClasses;

	}

	private List<EClass> getSubClasses(final EClass selectedObjClass) {

		final List<EClass> subClassList = new ArrayList<>();

		final ResourceSet resourceSet = selectedObjClass.eResource().getResourceSet();

		if (resourceSet != null) {

			// With this approach check is made to find the Sub-Classes for the supplied EClass in all the packages
			// belonging to the resource set

			final EList<Resource> resources = resourceSet.getResources();

			for (final Resource resource : resources) {

				final EList<EObject> contents = resource.getContents();

				if (!contents.isEmpty()) {
					for (EObject eObj : contents) {
						if (eObj instanceof EPackage) {
							subClassList.addAll(getSubClasses(selectedObjClass, (EPackage) eObj));
						}
					}
				}
			}

		} else {

			final EPackage ePackage = selectedObjClass.getEPackage();

			if (ePackage != null) {
				EObject rootEPackage = org.eclipse.emf.ecore.util.EcoreUtil.getRootContainer(selectedObjClass);

				if (rootEPackage instanceof EPackage) {
					return getSubClasses(selectedObjClass, (EPackage) rootEPackage);
				}
			}

			return getSubClasses(selectedObjClass, ePackage);
		}

		return subClassList;
	}

	private List<EClass> getSubClasses(final EClass selectedObjClass, final EPackage ePackage) {

		final List<EClass> subClassList = new ArrayList<>();

		EList<EPackage> eSubpackages = ePackage.getESubpackages();

		for (EPackage eSubPackage : eSubpackages) {
			subClassList.addAll(getSubClasses(selectedObjClass, eSubPackage));
		}

		final EList<EClassifier> eClassifiers = ePackage.getEClassifiers();

		for (final EClassifier eClassifier : eClassifiers) {

			if (eClassifier instanceof EClass
					&& selectedObjClass != eClassifier
					&& selectedObjClass.isSuperTypeOf((EClass) eClassifier)) {
				subClassList.add((EClass) eClassifier);
			}
		}
		return subClassList;
	}

}
