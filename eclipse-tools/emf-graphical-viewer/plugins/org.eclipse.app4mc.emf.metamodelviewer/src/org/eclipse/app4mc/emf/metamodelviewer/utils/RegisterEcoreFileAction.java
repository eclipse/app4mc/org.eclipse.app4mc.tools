/**
 ********************************************************************************
 * Copyright (c) 2017-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

public class RegisterEcoreFileAction {

	// Suppress default constructor
	private RegisterEcoreFileAction() {
		throw new IllegalStateException("Utility class");
	}

	public static List<String> loadEcoreFiles(List<String> ecoreFiles) throws IOException {

		ResourceSet metaDataResourceSet = new ResourceSetImpl();

		if (metaDataResourceSet instanceof ResourceSetImpl) {
			((ResourceSetImpl) metaDataResourceSet).setURIResourceMap(new HashMap<>());
		}
		Map<String, Object> extensionToFactoryMap = metaDataResourceSet.getResourceFactoryRegistry()
				.getExtensionToFactoryMap();

		extensionToFactoryMap.put("ecore", new EcoreResourceFactoryImpl());

		// register Ecore model in resourceSet registry
		Registry packageRegistry = metaDataResourceSet.getPackageRegistry();

		packageRegistry.put("http://www.eclipse.org/emf/2002/Ecore", EcorePackage.eINSTANCE);

		for (String ecorePath : ecoreFiles) {

			EPackage loadEPackage = loadEPackage(ecorePath, metaDataResourceSet);

			if (packageRegistry.containsKey(loadEPackage.getNsURI())) {
				Object object = packageRegistry.get(loadEPackage.getNsURI());

				if (object instanceof EPackage) {
					((EPackage) object).getESubpackages().add(loadEPackage);
				}
			} else {
				packageRegistry.put(loadEPackage.getNsURI(), loadEPackage);
			}

		}

		EcoreUtil.resolveAll(metaDataResourceSet);

		/*-for sorting */

		List<String> keyList = new ArrayList<>(EPackage.Registry.INSTANCE.keySet());
		Collections.sort(keyList);

		keyList.addAll(0, metaDataResourceSet.getPackageRegistry().keySet());

		EPackage.Registry.INSTANCE.putAll(metaDataResourceSet.getPackageRegistry());

		return keyList;
	}

	private static EPackage loadEPackage(String path, ResourceSet metaResourceSet) throws IOException {

		path = getCanonicalPathFullPath(path);
		Resource metaResource = metaResourceSet.getResource(URI.createFileURI(path), true);
		metaResource.load(Collections.emptyMap());

		EObject eObject = metaResource.getContents().get(0);
		return (EPackage) eObject;

	}

	private static String getCanonicalPathFullPath(String path) throws IOException {

		File file = new File(path);
		return file.getCanonicalPath();
	}

}
