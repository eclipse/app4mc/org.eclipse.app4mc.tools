/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.views.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.app4mc.emf.metamodelviewer.utils.SubClassesListBuilder;
import org.eclipse.app4mc.emf.metamodelviewer.utils.SuperClassesListBuilder;
import org.eclipse.app4mc.emf.metamodelviewer.utils.TypeElement;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.jface.viewers.ITreeContentProvider;

import wrapper.ISphinxWrapper;

public class ViewContentProvider implements ITreeContentProvider {
	
	private ISphinxWrapper iSphinxWrapper;
	public ViewContentProvider(ISphinxWrapper iSphinxWrapper) {
		this.iSphinxWrapper=iSphinxWrapper;
	}

	@Override
	public Object[] getElements(final Object parent) {
		if (iSphinxWrapper!=null && iSphinxWrapper.isInstanceOfMetaModelDescriptor(parent)) {

			final Collection<EPackage> ePackages = iSphinxWrapper.getListOfEpackagesFromMetaModelDescriptor(parent);

			return ePackages.toArray();
		} else if (parent instanceof EPackage) {

			final EList<EPackage> eSubpackages = ((EPackage) parent).getESubpackages();
			final EList<EClassifier> eClassifiers = ((EPackage) parent).getEClassifiers();

			final ArrayList<EPackage> arrayList1 = new ArrayList<>();

			arrayList1.addAll(eSubpackages);

			Collections.sort(arrayList1, this.comparator);

			final ArrayList<EClassifier> arrayList2 = new ArrayList<>();

			arrayList2.addAll(eClassifiers);

			Collections.sort(arrayList2, this.comparator);

			final List<ENamedElement> combinedList = new ArrayList<>();

			combinedList.addAll(arrayList1);
			combinedList.addAll(arrayList2);

			return combinedList.toArray();

		} else if (parent instanceof Entry) {
			return new Object[] { ((Entry<?, ?>) parent).getValue() };
		}

		return getChildren(parent);
	}

	@Override
	public Object getParent(final Object child) {

		if (child instanceof EPackage) {
			return ((EPackage) child).eContainer();
		} else if (child instanceof EClass) {
			return ((EClass) child).eContainer();
		} else if (child instanceof EStructuralFeature) {
			return ((EStructuralFeature) child).eContainer();
		}

		return null;
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object[] getChildren(final Object parent) {
		if (parent instanceof EPackage) {

			final EList<EPackage> eSubpackages = ((EPackage) parent).getESubpackages();
			final EList<EClassifier> eClassifiers = ((EPackage) parent).getEClassifiers();

			final List<EPackage> list1 = new ArrayList<>();
			list1.addAll(eSubpackages);
			Collections.sort(list1, this.comparator);

			final List<EClassifier> list2 = new ArrayList<>();
			list2.addAll(eClassifiers);
			Collections.sort(list2, this.comparator);

			final List<ENamedElement> combinedList = new ArrayList<>();

			combinedList.addAll(list1);
			combinedList.addAll(list2);

			return combinedList.toArray();

		} else if (parent instanceof EClass) {

			final EList<EStructuralFeature> eAllStructuralFeatures = ((EClass) parent).getEAllStructuralFeatures();

			final List list = new ArrayList<>();
			list.addAll(eAllStructuralFeatures);
			Collections.sort(list, this.comparator);

			int index = 0;

			final SuperClassesListBuilder superClassList = new SuperClassesListBuilder((EClass) parent);
			if (!superClassList.getAllSuperClasses().isEmpty()) {
				list.add(index++, superClassList);
			}

			final SubClassesListBuilder SubClassesListBuilder = new SubClassesListBuilder((EClass) parent);
			if (!SubClassesListBuilder.getAllSubClasses().isEmpty()) {
				list.add(index++, SubClassesListBuilder);
			}

			return list.toArray();
		} else if (parent instanceof EDataType) {

			if (parent instanceof EEnum) {

				final EList<EEnumLiteral> eLiterals = ((EEnum) parent).getELiterals();

				final List<ENamedElement> list = new ArrayList<>();

				list.addAll(eLiterals);

				Collections.sort(list, this.comparator);

				return list.toArray();
			}

			final EList<ETypeParameter> eTypeParameters = ((EDataType) parent).getETypeParameters();

			final List<ENamedElement> list = new ArrayList<>();

			list.addAll(eTypeParameters);

			Collections.sort(list, this.comparator);

			return list.toArray();
		} else if (parent instanceof EEnum) {

			final EList<EEnumLiteral> eLiterals = ((EEnum) parent).getELiterals();

			final List<ENamedElement> list = new ArrayList<>();

			list.addAll(eLiterals);

			Collections.sort(list, this.comparator);

			return list.toArray();
		} else if (parent instanceof SuperClassesListBuilder) {
			final Set<EClass> allSuperClasses = ((SuperClassesListBuilder) parent).getAllSuperClasses();
			final ArrayList<ENamedElement> arrayList = new ArrayList<>(allSuperClasses);
			Collections.sort(arrayList, this.comparator);
			return arrayList.toArray();
		} else if (parent instanceof SubClassesListBuilder) {
			final Set<EClass> allSubClasses = ((SubClassesListBuilder) parent).getAllSubClasses();
			final ArrayList<ENamedElement> arrayList = new ArrayList<>(allSubClasses);
			Collections.sort(arrayList, this.comparator);
			return arrayList.toArray();
		} else if (parent instanceof EStructuralFeature) {
			final EClassifier eType = ((EStructuralFeature) parent).getEType();
			return new Object[] { new TypeElement(eType) };
		} else if (parent instanceof TypeElement) {
			final EClassifier eType = ((TypeElement) parent).getClassifier();
			return new Object[] { eType };
		}
		return new Object[0];
	}

	@Override
	public boolean hasChildren(final Object parent) {
		if (parent instanceof EPackage || parent instanceof EClass || parent instanceof EStructuralFeature) {
			return true;
		} 

		// order is important: EEnum is a sub interface of EDataType
		if (parent instanceof EEnum) {
			return !((EEnum) parent).getELiterals().isEmpty();
		}
		
		if (parent instanceof EDataType) {
			return !((EDataType) parent).getETypeParameters().isEmpty();
		}

		if (parent instanceof SuperClassesListBuilder) {
			return !((SuperClassesListBuilder) parent).getAllSuperClasses().isEmpty();
		}

		if (parent instanceof SubClassesListBuilder) {
			return !((SubClassesListBuilder) parent).getAllSubClasses().isEmpty();
		}

		if (parent instanceof EEnumLiteral) {
			return false;
		}

		if (parent instanceof TypeElement) {
			return true;
		}
		
		return true;
	}

	final Comparator<ENamedElement> comparator = ( (arg0, arg1) -> arg0.getName().compareTo(arg1.getName()) );

}
