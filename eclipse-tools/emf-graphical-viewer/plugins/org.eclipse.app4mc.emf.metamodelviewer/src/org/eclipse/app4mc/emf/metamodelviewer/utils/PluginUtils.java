/**
 ********************************************************************************
 * Copyright (c) 2017-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.utils;

import java.io.IOException;
import java.net.URL;

import org.eclipse.app4mc.emf.metamodelviewer.base.Activator;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;

import wrapper.ISphinxWrapper;

public class PluginUtils {

	// Suppress default constructor
	private PluginUtils() {
		throw new IllegalStateException("Utility class");
	}

	public static ImageDescriptor getImageDescriptor(final String location) {

		final URL entry = Activator.getDefault().getBundle().getEntry(location);

		if (entry != null) {

			URL fileURL;
			try {
				fileURL = FileLocator.toFileURL(entry);
				if (fileURL != null) {
					return ImageDescriptor.createFromURL(fileURL);
				}
			} catch (final IOException e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	public static ISphinxWrapper getObjectOfSphinxWrapper() {

		final IExtensionRegistry registry = Platform.getExtensionRegistry();

		final IExtensionPoint extensionPoint = registry
				.getExtensionPoint("org.eclipse.app4mc.emf.metamodelviewer.sphinx.wrapper");

		final IConfigurationElement[] extensions = extensionPoint.getConfigurationElements();

		if (extensions.length > 0) {

			IConfigurationElement iConfigurationElement = extensions[0];

			ISphinxWrapper module = null;
			try {
				module = (ISphinxWrapper) iConfigurationElement.createExecutableExtension("class");
			} catch (CoreException e) {
				// do nothing
			}
			return module;
		}

		return null;
	}

}
