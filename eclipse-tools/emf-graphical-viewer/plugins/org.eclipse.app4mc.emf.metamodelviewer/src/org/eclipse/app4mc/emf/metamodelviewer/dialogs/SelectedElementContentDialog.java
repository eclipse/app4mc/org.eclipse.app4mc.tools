/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.dialogs;

import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.app4mc.emf.metamodelviewer.base.Activator;
import org.eclipse.app4mc.emf.metamodelviewer.preferences.MetaModelViewerPreferenceConstants;
import org.eclipse.app4mc.emf.metamodelviewer.utils.PluginUtils;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPartSite;

public class SelectedElementContentDialog extends org.eclipse.jface.dialogs.Dialog {

	EObject eObject;

	private TreeViewer tv;

	private final IWorkbenchPartSite iWorkbenchPartSite;

	public SelectedElementContentDialog(final Shell parentShell, final IWorkbenchPartSite iWorkbenchPartSite,
			final EObject eObject) {
		super(parentShell);
		this.eObject = eObject;
		this.iWorkbenchPartSite = iWorkbenchPartSite;
	}

	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite container = (Composite) super.createDialogArea(parent);

		//
		// final ElementSearchPatternFilter patternFilter = new ElementSearchPatternFilter(true, true);
		//
		// final FilteredTree filteredTree = new FilteredTree(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL |
		// SWT.BORDER,
		// patternFilter, true);
		//
		// filteredTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 0));
		//
		//
		// filteredTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 0));
		//
		// this.tv = filteredTree.getViewer();

		this.tv = new TreeViewer(container);
		this.tv.getTree().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		this.tv.setContentProvider(
				new org.eclipse.app4mc.emf.metamodelviewer.views.providers.ViewContentProvider(PluginUtils.getObjectOfSphinxWrapper()));
		this.tv.setLabelProvider(new org.eclipse.app4mc.emf.metamodelviewer.views.providers.ViewLabelProvider());

		final Entry<String, EObject> entry = new AbstractMap.SimpleEntry<>("Selected Element", this.eObject);

		this.tv.setInput(entry);

		hookContextMenu();
		hookDoubleClickAction();
		return container;
	}

 
	
	private void hookContextMenu() {
		final MenuManager menuMgr = new MenuManager("#PopupMenu") {
			@Override
			public IContributionItem[] getItems() {

				IContributionItem[] menuItems = super.getItems();

				final List<IContributionItem> filteredContributionItems = new LinkedList<>();

				for (final IContributionItem contributionItem : menuItems) {
					if (contributionItem != null && (contributionItem.getId() != null && (!(contributionItem.getId()
							.startsWith("org.eclipse.emf.ecore.editor.CreateDynamicInstance"))))) {
						filteredContributionItems.add(contributionItem);
					}
				}
				menuItems = new IContributionItem[filteredContributionItems.size()];
				return filteredContributionItems.toArray(menuItems);

			}
		};
		menuMgr.setRemoveAllWhenShown(true);
		final Menu menu = menuMgr.createContextMenu(this.tv.getControl());
		this.tv.getControl().setMenu(menu);
		getIWorkbenchPartSite().registerContextMenu(menuMgr, this.tv);
	}

	private void hookDoubleClickAction() {
		this.tv.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(final DoubleClickEvent event) {
				// MetaModelExplorerView.this.doubleClickAction.run();

				final ISelection selection = event.getSelection();

				if (selection instanceof TreeSelection) {
					final Object firstElement = ((TreeSelection) selection).getFirstElement();
					if (firstElement instanceof EObject) {
						Shell parentShell = null;

						String displayDialogStyle = (Activator.getDefault().getPreferenceStore()
								.getString(MetaModelViewerPreferenceConstants.P_DIALOG_DISPLAY));

						if (displayDialogStyle != null
								&& displayDialogStyle.equals(MetaModelViewerPreferenceConstants.V_ON_TOP)) {
							parentShell = new Shell(SWT.ON_TOP);
						} else {
							parentShell = new Shell();
						}

						// final String name = ((firstElement instanceof EClass) ? ((EClass)
						// firstElement).getName() :
						// "");
						//
						// parentShell.setText("Details of : " + ((EClass)
						// firstElement).eClass().getName() + " " +
						// name);
						//
						//
						// final TaskBar systemTaskBar = parentShell.getDisplay().getSystemTaskBar();
						// final TaskItem taskItem = systemTaskBar.getItem(parentShell);
						//
						// taskItem.setData(parentShell);
						// taskItem.setOverlayText(name);
						// taskItem.setText(name);

						new SelectedElementContentDialog(parentShell, getIWorkbenchPartSite(), (EObject) firstElement)
								.open();
					}
				}
			}
		});
	}

	@Override
	protected void configureShell(final Shell newShell) {
		super.configureShell(newShell);

		final String name = ((this.eObject instanceof EClass) ? ((EClass) this.eObject).getName() : "");
		newShell.setText(name);

		//
		// final TaskBar systemTaskBar = newShell.getDisplay().getSystemTaskBar();
		// final TaskItem taskItem = systemTaskBar.getItem(newShell);
		//
		// taskItem.setData(newShell);
		// taskItem.setOverlayText(name);
		// taskItem.setText(name);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}

	@Override
	protected void setShellStyle(final int newShellStyle) {
		super.setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
		setBlockOnOpen(false);
	}

	public IWorkbenchPartSite getIWorkbenchPartSite() {
		return this.iWorkbenchPartSite;
	}

}