/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class MetaModelViewerPreferenceConstants {

	// Suppress default constructor
	private MetaModelViewerPreferenceConstants() {
		throw new IllegalStateException("Utility class");
	}

	public static final String P_DIALOG_DISPLAY = "P_DIALOG_DISPLAY";
	public static final String V_ON_TOP = "On Top";
	public static final String V_DEFAUT = "Default";
}
