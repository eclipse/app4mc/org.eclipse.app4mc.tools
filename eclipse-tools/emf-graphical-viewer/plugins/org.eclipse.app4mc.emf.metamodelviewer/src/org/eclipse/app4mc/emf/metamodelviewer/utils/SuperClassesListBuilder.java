/**
 ********************************************************************************
 * Copyright (c) 2017-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.utils;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

public class SuperClassesListBuilder {

	private final EClass eClass;

	private boolean isCalculated;

	final Set<EClass> allSuperClasses = new HashSet<>();

	public SuperClassesListBuilder(final EClass eClass) {
		this.eClass = eClass;

	}

	public Set<EClass> getAllSuperClasses() {

		if (!this.isCalculated) {

			final EList<EClass> eAllSuperTypes = this.eClass.getEAllSuperTypes();
			this.allSuperClasses.addAll(eAllSuperTypes);

			for (final EClass subClass : eAllSuperTypes) {
				final SuperClassesListBuilder classList = new SuperClassesListBuilder(subClass);

				this.allSuperClasses.addAll(classList.getAllSuperClasses());
			}
		}

		return this.allSuperClasses;
	}

}
