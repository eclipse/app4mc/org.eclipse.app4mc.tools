/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.preferences;

import org.eclipse.app4mc.emf.metamodelviewer.base.Activator;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class MetaModelViewerPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	public MetaModelViewerPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Preference page to specify the properties/behaviour of MetaModel viewer");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks needed to manipulate various
	 * types of preferences. Each field editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		final String[][] options = {
				{ MetaModelViewerPreferenceConstants.V_DEFAUT, MetaModelViewerPreferenceConstants.V_DEFAUT },
				{ MetaModelViewerPreferenceConstants.V_ON_TOP, MetaModelViewerPreferenceConstants.V_ON_TOP } };

		addField(new ComboFieldEditor(MetaModelViewerPreferenceConstants.P_DIALOG_DISPLAY,
				"&Configure the way child dialogs should be displayed", options, getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(final IWorkbench workbench) {
		// no specific initialization required
	}

}