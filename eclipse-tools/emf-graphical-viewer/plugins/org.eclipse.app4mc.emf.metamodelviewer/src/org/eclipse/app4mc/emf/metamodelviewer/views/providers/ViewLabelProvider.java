/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.views.providers;

import java.io.IOException;
import java.net.URL;

import org.eclipse.app4mc.emf.metamodelviewer.base.Activator;
import org.eclipse.app4mc.emf.metamodelviewer.utils.SubClassesListBuilder;
import org.eclipse.app4mc.emf.metamodelviewer.utils.SuperClassesListBuilder;
import org.eclipse.app4mc.emf.metamodelviewer.utils.TypeElement;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class ViewLabelProvider extends LabelProvider {

	private final Image classImage;
	private final Image enumImage;
	private final Image containmentImage;
	private final Image superClassListImage;
	private final Image subClassListImage;
	private final Image attributeImage;
	private final Image attribute_referenceImage;
	private final Image abstractClassImage;
	private final Image interfaceImage;
	private final Image eTypeImage;

	public ViewLabelProvider() {

		this.classImage = getImage("icons/class_obj.png");
		this.enumImage = getImage("icons/enum_obj.png");
		this.containmentImage = getImage("icons/attribute_containment.png");
		this.superClassListImage = getImage("icons/super_co.gif");
		this.subClassListImage = getImage("icons/downward_co.gif");
		this.attributeImage = getImage("icons/attribute.png");
		this.attribute_referenceImage = getImage("icons/attribute_reference.png");
		this.abstractClassImage = getImage("icons/abstract_class_obj.png");
		this.interfaceImage = getImage("icons/interface_obj.png");
		this.eTypeImage = getImage("icons/type_obj.gif");
	}

	private Image getImage(final String location) {

		final URL entry = Activator.getDefault().getBundle().getEntry(location);

		if (entry != null) {

			URL fileURL;
			try {
				fileURL = FileLocator.toFileURL(entry);
				if (fileURL != null) {
					return new Image(Display.getDefault(), new ImageData(fileURL.getFile()));
				}
			} catch (final IOException e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	@Override
	public String getText(final Object child) {

		if (child instanceof EPackage) {
			return ((EPackage) child).getName();
		} else if (child instanceof EClass) {
			return ((EClass) child).getName();
		} else if (child instanceof EDataType) {
			return ((EDataType) child).getName();
		} else if (child instanceof EEnum) {
			return "Enum : " + ((EEnum) child).getName();
		} else if (child instanceof EEnumLiteral) {
			return ((EEnumLiteral) child).getName();
		} else if (child instanceof EStructuralFeature) {
			final EStructuralFeature eStFeature = (EStructuralFeature) child;

			final int lowerBound = eStFeature.getLowerBound();
			final int upperBound = eStFeature.getUpperBound();

			final String lowerBoundString = lowerBound == -1 ? "*" : lowerBound + "";

			final String upperBoundString = upperBound == -1 ? "*" : upperBound + "";

			final String multiplicity = "[" + lowerBoundString + " " + upperBoundString + "]";

			return eStFeature.getName() + "  " + multiplicity + " " + eStFeature.getEType().getName();
		} else if (child instanceof SuperClassesListBuilder) {
			return "All Super Classes (flat)";
		} else if (child instanceof SubClassesListBuilder) {
			return "All Sub Classes (flat)";
		} else if (child instanceof TypeElement) {
			return "<--EType-->";
		}
		return child.toString();
	}

	@Override
	public Image getImage(final Object obj) {
		String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
		if (obj instanceof EPackage) {
			imageKey = ISharedImages.IMG_OBJ_FOLDER;
		} else if (obj instanceof EStructuralFeature && obj instanceof EReference) {

			if (((EReference) obj).isContainment()) {
				return this.containmentImage;
			}

			return this.attribute_referenceImage;
		} else if (obj instanceof EAttribute) {
			return this.attributeImage;

		} else if (obj instanceof EDataType) {
			imageKey = ISharedImages.IMG_OBJ_ELEMENT;

			if (obj instanceof EEnum) {
				return this.enumImage;
			}

		} else if (obj instanceof EClass) {
			if (((EClass) obj).isAbstract() && ((EClass) obj).isInterface()) {
				return this.interfaceImage;
			}
			if (((EClass) obj).isAbstract()) {
				return this.abstractClassImage;
			}

			return this.classImage;
		} else if (obj instanceof EEnum) {
			return this.enumImage;
		} else if (obj instanceof SuperClassesListBuilder) {
			return this.superClassListImage;
		} else if (obj instanceof SubClassesListBuilder) {

			return this.subClassListImage;
		} else if (obj instanceof TypeElement) {

			return this.eTypeImage;
		}
		return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
	}
}