/**
 ********************************************************************************
 * Copyright (c) 2017-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.metamodelviewer.utils;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.dialogs.PatternFilter;

public class ElementSearchPatternFilter extends PatternFilter {

	private boolean filterForEClassifiersBoolean;

	private boolean filterForAttributesBoolean;

	public ElementSearchPatternFilter(final boolean filterForEClassifiersBoolean,
			final boolean filterForAttributesBoolean) {

		this.filterForEClassifiersBoolean = filterForEClassifiersBoolean;
		this.filterForAttributesBoolean = filterForAttributesBoolean;
	}

	@Override
	protected boolean isParentMatch(final Viewer viewer, final Object element) {

		if (element instanceof SuperClassesListBuilder
				|| element instanceof SubClassesListBuilder
				|| element instanceof EStructuralFeature
				|| element instanceof TypeElement) {
			return false;
		}

		return super.isParentMatch(viewer, element);
	}

	@Override
	protected boolean isLeafMatch(final Viewer viewer, final Object element) {

		if ((element instanceof EClassifier)) {
			if (!isFilterForEClassifiersBoolean()) {
				return false;
			}
			return super.isLeafMatch(viewer, element);

		} else if ((element instanceof EStructuralFeature)) {
			if (!isFilterForAttributesBoolean()) {
				return false;
			}
			return super.isLeafMatch(viewer, element);

		} else if (element instanceof SuperClassesListBuilder
				|| element instanceof SubClassesListBuilder
				|| element instanceof TypeElement) {
			return false;
		}
		return super.isLeafMatch(viewer, element);
	}

	public boolean isFilterForEClassifiersBoolean() {
		return this.filterForEClassifiersBoolean;
	}

	public boolean isFilterForAttributesBoolean() {
		return this.filterForAttributesBoolean;
	}

	public void setFilterForEClassifiersBoolean(final boolean filterForEClassifiersBoolean) {
		this.filterForEClassifiersBoolean = filterForEClassifiersBoolean;
	}

	public void setFilterForAttributesBoolean(final boolean filterForAttributesBoolean) {
		this.filterForAttributesBoolean = filterForAttributesBoolean;
	}
	
}
