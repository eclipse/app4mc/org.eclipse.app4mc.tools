/**
 ********************************************************************************
 * Copyright (c) 2017-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package wrapper;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EPackage;

public interface ISphinxWrapper {

	public Object getMetaModelDescriptor(String id);

	public boolean isInstanceOfMetaModelDescriptor(Object descriptor);

	public List<String> getListOfMetaModelDescriptorIDs();

	public Collection<EPackage> getListOfEpackagesFromMetaModelDescriptor(Object metaModelDescriptor);

}
