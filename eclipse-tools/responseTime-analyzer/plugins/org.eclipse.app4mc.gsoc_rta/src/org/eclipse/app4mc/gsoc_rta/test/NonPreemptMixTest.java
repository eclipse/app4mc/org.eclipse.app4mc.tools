/*******************************************************************************
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     FH Dortmund - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.gsoc_rta.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.math.BigInteger;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType;
import org.eclipse.app4mc.gsoc_rta.NPandPRTA;
import org.eclipse.emf.common.util.EList;
import org.junit.BeforeClass;
import org.junit.Test;

public class NonPreemptMixTest {

	private static Amalthea amalthea;
	int[] iap = { 5, 2, 5, 3, 3, 1, 3, 5, 1, 0, 0, 6, 4, 6 };
	@BeforeClass
	public static void init() {
		org.apache.log4j.BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.DEBUG);
	}

	@Test
	public void testPureExecutionForTask()
	{
		final Logger log = Logger.getLogger(NonPreemptMixTest.class);
		final Amalthea inputModel = AmaltheaLoader.loadFromFile(new File("model-input/ChallengeModelNP.amxmi"));
		NPandPRTA nprta = new NPandPRTA(inputModel, iap);
		EList<Task> listTask = inputModel.getSwModel().getTasks();
		log.info("Start testing pure execution time");
		for (int i = 0; i < listTask.size(); i++) {
			Task thisTask = listTask.get(i);
			BigInteger timeValue = nprta.getPureExecutionTime(thisTask, iap, TimeType.WCET).getValue();
			if (i == 0) {
				assertEquals(BigInteger.valueOf(50000000000l), timeValue);
			}

			else if (i == 1) {
				assertEquals(BigInteger.valueOf(14753760000l), timeValue);
			}
			else if (i == 2) {
				assertEquals(BigInteger.valueOf(1861275000l), timeValue);
			}
			else if (i == 3) {
				assertEquals(BigInteger.valueOf(600000000l), timeValue);
			}
			else if (i == 4) {
				assertEquals(BigInteger.valueOf(4762510000l), timeValue);
			}
			else if (i == 5) {
				assertEquals(BigInteger.valueOf(12597052500l), timeValue);
			}
			else if (i == 6) {
				assertEquals(BigInteger.valueOf(9785855000l), timeValue);
			}
			else if (i == 7) {
				assertEquals(BigInteger.valueOf(18579392500l), timeValue);
			}
			else if (i == 8) {
				assertEquals(BigInteger.valueOf(8125495500l), timeValue);
			}
			else if (i == 9) {
				assertEquals(BigInteger.valueOf(4775267000l), timeValue);
			}
			else if (i == 10) {
				assertEquals(BigInteger.valueOf(28317754000l), timeValue);
			}
			else if (i == 11) {
				assertEquals(BigInteger.valueOf(124156673331l), timeValue);
			}
			else if (i == 12) {
				assertEquals(BigInteger.valueOf(52294720000l), timeValue);
			}
			else if (i == 13) {
				assertEquals(BigInteger.valueOf(116286459997l), timeValue);
			}
		}
	}
	
	@Test
	public void testNonPreempMixForTask()
	{
		final Amalthea inputModel = AmaltheaLoader.loadFromFile(new File("model-input/ChallengeModelNP.amxmi"));
		final Logger log = Logger.getLogger(NonPreemptMixTest.class);
		NPandPRTA nprta = new NPandPRTA(inputModel, iap);
		EList<Task> listTask = inputModel.getSwModel().getTasks();
		log.info("Start testing non-preemp mix");
		for (int i = 0; i < listTask.size(); i++) {
			Task thisTask = listTask.get(i);
			BigInteger timeValue = nprta.getNonPreemptaskRTA(thisTask, iap, TimeType.WCET).getValue();
			if (i == 0) {
				assertEquals(BigInteger.valueOf(79780400000l), timeValue);
			}

			else if (i == 1) {
				assertEquals(BigInteger.valueOf(14753760000l), timeValue);
			}
			else if (i == 2) {
				assertEquals(BigInteger.valueOf(0), timeValue);
			}
			else if (i == 3) {
				assertEquals(BigInteger.valueOf(5362510000l), timeValue);
			}
			
			else if (i == 4) {
				assertEquals(BigInteger.valueOf(5362510000l), timeValue);
			}
			else if (i == 5) {
				assertEquals(BigInteger.valueOf(12597052500l), timeValue);
			}
			else if (i == 6) {
				assertEquals(BigInteger.valueOf(21110875000l), timeValue);
			}
			else if (i == 7) {
				assertEquals(BigInteger.valueOf(189307842500l), timeValue);
			}
			else if (i == 8) {
				assertEquals(BigInteger.valueOf(58513705500l), timeValue);
			}
			else if (i == 9) {
				assertEquals(BigInteger.valueOf(61410775000l), timeValue);
			}
			else if (i == 10) {
				assertEquals(BigInteger.valueOf(28317754000l), timeValue);
			}
			else if (i == 11) {
				assertEquals(BigInteger.valueOf(356729593325l), timeValue);
			}
			else if (i == 12) {
				assertEquals(BigInteger.valueOf(52294720000l), timeValue);
			}
			else if (i == 13) {
				assertEquals(BigInteger.valueOf(116286459997l), timeValue);
			}
		}
	}
	
	@Test
	public void testClassicRTAForTask()
	{
		final Amalthea inputModel = AmaltheaLoader.loadFromFile(new File("model-input/ChallengeModelNP.amxmi"));
		final Logger log = Logger.getLogger(NonPreemptMixTest.class);
		NPandPRTA nprta = new NPandPRTA(inputModel, iap);
		EList<Task> listTask = inputModel.getSwModel().getTasks();
		log.info("Start testing classic rta");
		for (int i = 0; i < listTask.size(); i++) {
			Task thisTask = listTask.get(i);
			BigInteger timeValue = nprta.getClassicRTA(thisTask, iap, TimeType.WCET).getValue();
			if (i == 0) {
				assertEquals(BigInteger.valueOf(79780400000l), timeValue);
			}

			else if (i == 1) {
				assertEquals(BigInteger.valueOf(14753760000l), timeValue);
			}
			else if (i == 2) {
				assertEquals(BigInteger.valueOf(1861275000l), timeValue);
			}
			else if (i == 3) {
				assertEquals(BigInteger.valueOf(600000000l), timeValue);
			}
			else if (i == 4) {
				assertEquals(BigInteger.valueOf(5362510000l), timeValue);
			}
			else if (i == 5) {
				assertEquals(BigInteger.valueOf(12597052500l), timeValue);
			}
			else if (i == 6) {
				assertEquals(BigInteger.valueOf(21110875000l), timeValue);
			}
			else if (i == 7) {
				assertEquals(BigInteger.valueOf(189307842500l), timeValue);
			}
			else if (i == 8) {
				assertEquals(BigInteger.valueOf(58513705500l), timeValue);
			}
			else if (i == 9) {
				assertEquals(BigInteger.valueOf(61410775000l), timeValue);
			}
			else if (i == 10) {
				assertEquals(BigInteger.valueOf(28317754000l), timeValue);
			}
			else if (i == 11) {
				assertEquals(BigInteger.valueOf(356729593325l), timeValue);
			}
			else if (i == 12) {
				assertEquals(BigInteger.valueOf(52294720000l), timeValue);
			}
			else if (i == 13) {
				assertEquals(BigInteger.valueOf(116286459997l), timeValue);
			}
		}
	}
	
	


}
