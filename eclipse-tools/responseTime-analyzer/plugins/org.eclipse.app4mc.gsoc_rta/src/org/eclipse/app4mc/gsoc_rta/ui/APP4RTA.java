/*******************************************************************************
 * Copyright (c) 2019 Dortmund University of Applied Sciences and Arts.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors: Junhyung Ki - initial API and implementation
 * 
 *******************************************************************************/
package org.eclipse.app4mc.gsoc_rta.ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.stream.Collectors;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaServices;
import org.eclipse.app4mc.amalthea.model.EventChain;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.PuType;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType;
import org.eclipse.app4mc.gsoc_rta.*;
import org.eclipse.app4mc.gsoc_rta.E2ELatency.SinglecoreLatCase;
import org.eclipse.emf.common.util.EList;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JSeparator;

/**
 * Date: May 04, 2020
 * @version 2.0
 *					The purpose of this class is to provide users a better visual and increase usability of Response Time Analysis.
 */
@SuppressWarnings("serial")
public class APP4RTA extends JFrame {
	private static JComboBox<String> ecComboBox = new JComboBox<String>();
	
	private CPURta cpurta = new CPURta(false, AmaltheaLoader.loadFromFile(new File(SharedConsts.SINGLE_CORE_MODEL)), new int[] { 4, 1, 1, 3, 4, 0, 1, 3, 3, 0, 6, 2, 5, 6 });
	private E2ELatency e2e = new E2ELatency();
	private Amalthea currentModel = null;
	private java.util.List<ProcessingUnit> currentPUList = null;
	private int[] currentIA = null;
	private EList<Task> allTaskList = null;
	private EList<EventChain> currentECList = null;
	private EventChain currentEC = null;
	private java.util.List<Task> currentTaskChain = null;
	private File currentFile = null;
	
	private HashMap<Integer, java.util.List<Task>> puTaskListHM;
	private JFrame frame;
	private int selectedECIndex = -1;
	private boolean schedulability_flag = false;
	JFileChooser fileChooser;
	JTextField selectedModelLbl;
	
	private JLabel jl0, jl1, jl2, jl3, jl4, jl5, jl6, jl7, jl8, jl9, jl10, 
		jl11, jl12, jl13, jl14, jl15, jl16, jl17, jl18, jl19;
	private JTextField tf0, tf1, tf2, tf3, tf4, tf5, tf6, tf7, tf8, tf9, 
		tf10, tf11, tf12, tf13, tf14, tf15, tf16, tf17, tf18, tf19;
	private JLabel[] jLabelArray = {
			jl0, jl1, jl2, jl3, jl4, jl5, jl6, jl7, jl8, jl9, jl10, 
			jl11, jl12, jl13, jl14, jl15, jl16, jl17, jl18, jl19
	};
	private JTextField[] jTextFieldArray= {
			tf0, tf1, tf2, tf3, tf4, tf5, tf6, tf7, tf8, tf9, 
			tf10, tf11, tf12, tf13, tf14, tf15, tf16, tf17, tf18, tf19
	};
	
	private JLabel jl_0, jl_1, jl_2, jl_3, jl_4, jl_5, jl_6, jl_7;
	private JLabel jl_a, jl_b, jl_c, jl_d, jl_e, jl_f, jl_g, jl_h;
	private List l0, l1, l2, l3, l4, l5, l6, l7;
	private List rtL0, rtL1, rtL2, rtL3, rtL4, rtL5, rtL6, rtL7;
	private JLabel[] puLabelArray = {
			jl_0, jl_1, jl_2, jl_3, jl_4, jl_5, jl_6, jl_7
	};
	private JLabel[] rtListLabelArray = {
			jl_a, jl_b, jl_c, jl_d, jl_e, jl_f, jl_g, jl_h
	};
	private List[] puListArray = {
			l0, l1, l2, l3, l4, l5, l6, l7
	};
	private List[] rtListArray = {
			rtL0, rtL1, rtL2, rtL3, rtL4, rtL5, rtL6, rtL7
	};
	
	private JLabel ecL0, ecL1, ecL2, ecL3, ecL4, ecL5, ecL6, 
		ecL7, ecL8, ecL9, ecL10, ecL11, ecL12, ecL13;
	private JTextField jtf0, jtf1, jtf2, jtf3, jtf4, jtf5, jtf6,
		jtf7, jtf8, jtf9, jtf10, jtf11, jtf12, jtf13;
	private JLabel[] ecLabelArray = {
			ecL0, ecL1, ecL2, ecL3, ecL4, ecL5, ecL6,
			ecL7, ecL8, ecL9, ecL10, ecL11, ecL12, ecL13
	};
	private JTextField[] ecJTPArray = {
			jtf0, jtf1, jtf2, jtf3, jtf4, jtf5, jtf6,
			jtf7, jtf8, jtf9, jtf10, jtf11, jtf12, jtf13
	};

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				org.apache.log4j.BasicConfigurator.configure();
				Logger.getRootLogger().setLevel(Level.ERROR);
				final APP4RTA window = new APP4RTA();
				window.frame.setVisible(true);
				window.frame.setTitle("APP4RTA");
			}
		});
	}

	public APP4RTA() {
		initialize();
	}
	
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Sitka Small", Font.PLAIN, 13));
		frame.setBounds(0, 31, 1200, 1027);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblSelectTheModel = new JLabel("Amalthea Model");
		lblSelectTheModel.setFont(new Font("Stencil BT", Font.PLAIN, 13));
		lblSelectTheModel.setBounds(10, 13, 120, 22);
		frame.getContentPane().add(lblSelectTheModel);
		
		JLabel lblTaskName = new JLabel("Task Name");
		lblTaskName.setForeground(new Color(139, 0, 0));
		lblTaskName.setFont(new Font("Sitka Heading", Font.BOLD, 12));
		lblTaskName.setHorizontalAlignment(SwingConstants.CENTER);
		lblTaskName.setBounds(15, 43, 70, 16);
		frame.getContentPane().add(lblTaskName);
		
		JLabel lblPuNum = new JLabel("PU Num");
		lblPuNum.setForeground(new Color(0, 0, 255));
		lblPuNum.setHorizontalAlignment(SwingConstants.CENTER);
		lblPuNum.setFont(new Font("Sitka Heading", Font.BOLD, 12));
		lblPuNum.setBounds(92, 43, 70, 16);
		frame.getContentPane().add(lblPuNum);
		
		JButton btnDefualtIa = new JButton("Default IA");
		btnDefualtIa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cpurta.getModel() == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: A model is not loaded.");
					return ;
				} else if (cpurta.getTRT() == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: TRT HashMap is not loaded.");
					return ;
				} else if (cpurta.getPUl().size() == 0) {
					JOptionPane.showMessageDialog(frame, "ERROR: Processing Unit List is not loaded.");
					return ;
				}
				/* This is only for 'ChallengeModel_TCs.amxmi' */
				/*
				final int[] defaultIA = SharedConsts.defaultIAMapping;
				if (cpurta.getModel().getSwModel().getTasks().size() == defaultIA.length) {
					for (int i = 0; i < defaultIA.length; i++) {
						jTextFieldArray[i].setText("" + defaultIA[i]);
					}
					return ;
				}
				String str = "";
				for (int i = 0; i < currentPUList.size(); i++) {
					if (i == currentPUList.size() - 1) {
						str = str + i; 
						break;
					}
					str = str + i + ", ";
				}
				JOptionPane.showMessageDialog(frame, "ERROR: The length of the default integer array does not match with\n"
						+ "the number of tasks in the target model.\n\n" + "Please assign the number(s) manually.\n" 
						+ "Available PU number(s): " + str); 
				*/
				if (currentPUList.size() < 1) {
					JOptionPane.showMessageDialog(frame, "ERROR: There is no processing unit in this model.\n"
							+ "Please choose another model."); 
				} else if (currentPUList.size() == 1) {
					final int numOfTasks = cpurta.getModel().getSwModel().getTasks().size();
					for (int i = 0; i < numOfTasks; i++) {
						jTextFieldArray[i].setText("0");
					}
					return ;
				} else {
					final int numOfTasks = cpurta.getModel().getSwModel().getTasks().size();
					final int numOfPUs = currentPUList.size();
					int x = 0;
					for (int i = 0; i < numOfTasks; i++) {
						if (x < numOfPUs) {
							jTextFieldArray[i].setText("" + x);
						} else {
							x = 0;
							jTextFieldArray[i].setText("" + x);
						}
						x++;
					}
				}
			}
		});
		
		JButton btnEnterIA = new JButton("Enter IA");
		btnEnterIA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentIA = new int[cpurta.getModel().getSwModel().getTasks().size()];
				for (int i = 0; i < allTaskList.size(); i++) {
					if (jTextFieldArray[i].getText().length() == 0) {
						JOptionPane.showMessageDialog(frame, "ERROR: Fill the rest of the array!");
						return ;
					} else if (jTextFieldArray[i].getText().charAt(0) < 48 || jTextFieldArray[i].getText().charAt(0) > 54) {
						JOptionPane.showMessageDialog(frame, "ERROR: Wrong array value!");
						return ;
					} else if (i >= 0 && i <= 9 && Integer.parseInt(jTextFieldArray[i].getText()) == 6) {
						JOptionPane.showMessageDialog(frame, "ERROR: CPU Tasks should not be mapped to GPU! \n\n"
								+ "Please assign different IA value for " + "(" + allTaskList.get(i).getName() + ")");
						return ;
					} else if (i == jTextFieldArray.length-1 && Integer.parseInt(jTextFieldArray[i].getText()) != 6) {
						JOptionPane.showMessageDialog(frame, "ERROR: 'Detection' task can not be mapped to CPU!");
						return ;
					}
					currentIA[i] = Integer.parseInt(jTextFieldArray[i].getText());
				}
				cpurta.setIA(currentIA);
				cpurta.setContention(currentIA, currentModel);
				puTaskListHM = cpurta.getPUTaskListHashMap(currentModel);
				for (int i = 0; i < puTaskListHM.size(); i++) {
					if (puListArray[i].getItemCount() == 0) {
						for (int j = 0; j < puTaskListHM.get(i).size(); j++) {	
							puListArray[i].add((puTaskListHM.get(i).get(j)).getName());
						}
					}
				}
			}
		});
		
		JRadioButton rdbtnSynchronous = new JRadioButton("Synchronous");
		rdbtnSynchronous.setForeground(new Color(139, 0, 0));
		rdbtnSynchronous.setFont(new Font("Sitka Heading", Font.BOLD, 13));
		
		JRadioButton rdbtnAsynchronous = new JRadioButton("Asynchronous");
		rdbtnAsynchronous.setForeground(new Color(0, 0, 255));
		rdbtnAsynchronous.setFont(new Font("Sitka Heading", Font.BOLD, 13));
		
		ButtonGroup offloadingMode = new ButtonGroup();
		offloadingMode.add(rdbtnSynchronous);
		offloadingMode.add(rdbtnAsynchronous);
		
		JRadioButton rdbtnWorstCase = new JRadioButton("Worst-Case");
		rdbtnWorstCase.setFont(new Font("Sitka Heading", Font.BOLD, 13));
		rdbtnWorstCase.setForeground(Color.DARK_GRAY);
		
		JRadioButton rdbtnAverageCase = new JRadioButton("Average-Case");
		rdbtnAverageCase.setFont(new Font("Sitka Heading", Font.BOLD, 13));
		rdbtnAverageCase.setForeground(Color.DARK_GRAY);
		
		JRadioButton rdbtnBestCase = new JRadioButton("Best-Case");
		rdbtnBestCase.setFont(new Font("Sitka Heading", Font.BOLD, 13));
		rdbtnBestCase.setForeground(Color.DARK_GRAY);

		ButtonGroup executionMode = new ButtonGroup();
		executionMode.add(rdbtnWorstCase);
		executionMode.add(rdbtnAverageCase);
		executionMode.add(rdbtnBestCase);
		
		JLabel lblSchedulability = new JLabel("Schedulability");
		JTextField schedulability = new JTextField();
		schedulability.setHorizontalAlignment(SwingConstants.RIGHT);
		schedulability.setEditable(false);
		
		JLabel lblMemAccCost = new JLabel("Cumulated Memory-Access Cost");
		JTextField memAccCost = new JTextField();
		memAccCost.setHorizontalAlignment(SwingConstants.RIGHT);
		memAccCost.setEditable(false);
		
		JLabel lblCumulContention = new JLabel("Cumulated Contention");
		JTextField cumulContention = new JTextField();
		cumulContention.setHorizontalAlignment(SwingConstants.RIGHT);
		cumulContention.setEditable(false);
		
		JLabel lblComputation = new JLabel("Computation");
		JTextField computation = new JTextField();
		computation.setHorizontalAlignment(SwingConstants.RIGHT);
		computation.setEditable(false);
		
		JLabel lblResponseTimeSum = new JLabel("Response Time Sum");
		JTextField rtSum = new JTextField();
		rtSum.setHorizontalAlignment(SwingConstants.RIGHT);
		rtSum.setEditable(false);
		
		JButton btnCalculate = new JButton("Calculate");
		btnCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (int i = 0; i < allTaskList.size(); i++) {
					if (jTextFieldArray[i].getText().length() == 0) {
						JOptionPane.showMessageDialog(frame, "ERROR: Fill the array first!");
						return ;
					}
				}				
				if (rdbtnSynchronous.isSelected()) {
					SharedConsts.synchronousOffloading = true;
				} else if (rdbtnAsynchronous.isSelected()) {
					SharedConsts.synchronousOffloading = false;
				} else {
					JOptionPane.showMessageDialog(frame, "ERROR: You should choose an offloading mode! (Sync / Async)");
					return ;
				}
				if (rdbtnWorstCase.isSelected()) {
					SharedConsts.timeType = TimeType.WCET;
				} else if (rdbtnAverageCase.isSelected()) {
					SharedConsts.timeType = TimeType.ACET;
				} else if (rdbtnBestCase.isSelected()) {
					SharedConsts.timeType = TimeType.BCET;
				} else {
					JOptionPane.showMessageDialog(frame, "ERROR: You should choose an execution case! (Worst Case / Average Case / Best Case)");
					return ;
				}
				if (currentIA == null || puTaskListHM.size() == 0) {
					JOptionPane.showMessageDialog(frame, "ERROR: Click the 'Enter IA' button.");
					return ;
				}
				schedulability_flag = true;
				Time pRtSum = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
				for (int i = 0; i < puTaskListHM.size(); i++) {
					final ProcessingUnit pu = cpurta.getPUl().get(i);
					final java.util.List<Task> thisPUTaskList = puTaskListHM.get(i);
					Time thisRT = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
					if (rtListArray[i].getItemCount() == 0) {
						if (pu.getDefinition().getPuType().equals(PuType.CPU)) {
							for (Task t : thisPUTaskList) {
								thisRT = cpurta.preciseTestCPURT(t, thisPUTaskList, SharedConsts.timeType, pu);
								final Time thisPeriod = CommonUtils.getStimInTime(t);
								final BigInteger period = AmaltheaServices.convertToPicoSeconds(thisPeriod);
								if (AmaltheaServices.convertToPicoSeconds(thisRT).compareTo(period) > 0) {
									rtListArray[i].add("Not Scheduleable! => MAX Value");
									schedulability_flag = false;
								} else if (AmaltheaServices.convertToPicoSeconds(thisRT).equals(BigInteger.ZERO)) {
									rtListArray[i].add("0 ps (GPU Task on CPU)");
								} else {
									rtListArray[i].add(thisRT.toString());
								}
								pRtSum = pRtSum.add(thisRT);
							}
						} else {
							for (Task t : thisPUTaskList) {
								thisRT = cpurta.getTRT().get(t);
								rtListArray[i].add(thisRT.toString());
								pRtSum = pRtSum.add(thisRT);
							}
						}
					}
				}
				if (schedulability_flag) {
					schedulability.setText("Scheduleable! :)");
					memAccCost.setText("" + cpurta.getCumulatedMemAccCosts());
					cumulContention.setText("" + cpurta.getCumulatedContention());
					computation.setText("" + (pRtSum.subtract(cpurta.getCumulatedMemAccCosts())).subtract(cpurta.getCumulatedContention()));
					rtSum.setText(pRtSum.toString());
				} else {
					schedulability.setText("Non Scheduleable! :(");
					memAccCost.setText("Not Available.");
					cumulContention.setText("Not Available.");
					computation.setText("Not Available.");
					rtSum.setText("Not Available.");
					JOptionPane.showMessageDialog(frame, "ERROR: This mapping is not schedulable.\n"
							+ "Please change the mapping.");
				}
			}
		});
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentIA = null;
				cpurta.setIA(currentIA);
				cpurta.setContention(currentIA, currentModel);
				/* latency time initializing */
				cpurta.initCumulatedMemAccCosts();
				cpurta.initCumulatedContention();
				for (int i = 0; i < allTaskList.size(); i++) {
					jTextFieldArray[i].setText("");
				}
				for (int i = 0; i < currentPUList.size(); i++) {
					puListArray[i].removeAll();
				}
				for (int i = 0; i < currentPUList.size(); i++) {
					rtListArray[i].removeAll();
				}
				offloadingMode.setSelected(null, false);
				executionMode.setSelected(null, false);
				schedulability.setText(null);
				memAccCost.setText(null);
				cumulContention.setText(null);
				computation.setText(null);
				rtSum.setText(null);
				currentIA = null;
				puTaskListHM = null;
				frame.revalidate();
				frame.repaint();
			}
		});

		selectedModelLbl = new JTextField();
		selectedModelLbl.setEditable(false);
		selectedModelLbl.setBounds(142, 13, 206, 22);
		frame.getContentPane().add(selectedModelLbl);
		selectedModelLbl.setColumns(10);
		
		JButton amaltheaSchBtn = new JButton("Search Amalthea");
		amaltheaSchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final String userDirLocation = System.getProperty("user.dir");
				File userDir = new File(userDirLocation);
				fileChooser = new JFileChooser(userDir);
				fileChooser.setAcceptAllFileFilterUsed(false);
		        FileNameExtensionFilter filter = new FileNameExtensionFilter("Amalthea models", "amxmi");
		        fileChooser.addChoosableFileFilter(filter);
		        fileChooser.setDialogTitle("Open File");
		        int result = fileChooser.showOpenDialog(null);
		        if (result == JFileChooser.APPROVE_OPTION) {
		        	final Amalthea loadedModel = AmaltheaLoader.loadFromFile(fileChooser.getSelectedFile());
		        	if (loadedModel.getVersion().compareTo("0.9.8") != 0) {
		        		JOptionPane.showMessageDialog(frame, "Error: This model should be migrated to the version 0.9.8");
		        		return ;
		        	}
		        	/* Something was selected before */
		        	if (currentFile != null) {
		        		/* The selected file is equal to the previous file => Do nothing */
		        		if (fileChooser.getSelectedFile().equals(currentFile)) {
		        			JOptionPane.showMessageDialog(frame, "Same Model");
		        			return ;
		        		}
		        		/* If the selected file is different to the previous one => Remove and Do Normal. */
		        		else {
		        			schedulability.setText(null);
		    				memAccCost.setText(null);
		    				cumulContention.setText(null);
		    				computation.setText(null);
		    				rtSum.setText(null);
		        			for (int i = 0; i < allTaskList.size(); i++) {
								frame.remove(jLabelArray[i]);
								frame.remove(jTextFieldArray[i]);
							}
							frame.remove(btnDefualtIa);
							frame.remove(btnEnterIA);
							frame.remove(rdbtnSynchronous);
							frame.remove(rdbtnAsynchronous);
							frame.remove(rdbtnWorstCase);
							frame.remove(rdbtnAverageCase);
							frame.remove(rdbtnBestCase);
							frame.remove(btnCalculate);
							frame.remove(btnReset);
							frame.remove(lblSchedulability);
							frame.remove(schedulability);
							frame.remove(lblMemAccCost);
							frame.remove(memAccCost);
							frame.remove(lblCumulContention);
							frame.remove(cumulContention);
							frame.remove(lblComputation);
							frame.remove(computation);
							frame.remove(lblResponseTimeSum);
							frame.remove(rtSum);
							for (int i = 0; i < currentPUList.size(); i++) {
								frame.remove(puLabelArray[i]);
								frame.remove(rtListLabelArray[i]);
								frame.remove(puListArray[i]);
								frame.remove(rtListArray[i]);
							}
							frame.remove(ecComboBox);
		        		}
		        	}
		        	/* Something was not selected before => Do normal */
		        	currentFile = fileChooser.getSelectedFile();
		        	selectedModelLbl.setText(currentFile.getName());
		        	currentModel = loadedModel;
					currentPUList = CommonUtils.getPUs(currentModel);
					if (currentPUList.get(0).getAccessElements().size() == 0) {
						JOptionPane.showMessageDialog(frame, "The selected model's processing unit(s) does(do) not have 'read & write Latency' values.\n\n"
								+ "Therefore, the default value of 30 will be assigned for them.\n"
								+ "This will affect the response time results.");
					}
					cpurta.setModel(currentModel);
					cpurta.setTRT(cpurta.getDefaultTRT(currentModel));
					cpurta.setPUl(currentPUList);
					allTaskList = currentModel.getSwModel().getTasks();
					if (allTaskList.size() > 20) {
						JOptionPane.showMessageDialog(frame, "ERROR: The selected model has " + allTaskList.size() + " Tasks. (Limit: 20)\n" 
								+ "Please choose a different model.");
						return ;
					} else if (currentPUList.size() > 8) {
						JOptionPane.showMessageDialog(frame, "ERROR: The selected model has " + currentPUList.size() + " Processing Units. (Limit: 8)\n"
								+ "Please choose a different model.");
						return ;
					}
					/* Task Indices */
					for (int i = 0; i < allTaskList.size(); i++) {
						jLabelArray[i] = new JLabel(allTaskList.get(i).getName());
						jLabelArray[i].setBounds(12, (60+(i*24)), 97, 16);
						frame.getContentPane().add(jLabelArray[i]);
						jTextFieldArray[i] = new JTextField();
						jTextFieldArray[i].setColumns(10);
						jTextFieldArray[i].setBounds(118, (60+(i*24)), 22, 22);
						frame.getContentPane().add(jTextFieldArray[i]);
					}
					/* DefaultIA Button */
					btnDefualtIa.setBounds(155, 60, 97, 25);
					frame.getContentPane().add(btnDefualtIa);
					/* EnterIA Button */
					btnEnterIA.setBounds(155, 90, 97, 25);
					frame.getContentPane().add(btnEnterIA);
					/* Sync Radio Button */
					rdbtnSynchronous.setBounds(152, 125, 126, 25);
					frame.getContentPane().add(rdbtnSynchronous);
					/* Async Radio Button */
					rdbtnAsynchronous.setBounds(152, 145, 126, 25);
					frame.getContentPane().add(rdbtnAsynchronous);
					/* WorstCase Radio Button */
					rdbtnWorstCase.setBounds(152, 175, 107, 25);
					frame.getContentPane().add(rdbtnWorstCase);
					/* AverageCase Radio Button */
					rdbtnAverageCase.setBounds(152, 195, 107, 25);
					frame.getContentPane().add(rdbtnAverageCase);
					/* BestCase Radio Button */
					rdbtnBestCase.setBounds(152, 215, 107, 25);
					frame.getContentPane().add(rdbtnBestCase);
					/* Calculate Button */
					btnCalculate.setBounds(155, 252, 97, 25);
					frame.getContentPane().add(btnCalculate);
					/* Reset Button */
					btnReset.setBounds(155, 282, 97, 25);
					frame.getContentPane().add(btnReset);
					/* Schedulability Label & TextField */
					lblSchedulability.setBounds(157, 323, 200, 16);
					frame.getContentPane().add(lblSchedulability);
					schedulability.setBounds(155, 343, 200, 22);
					frame.getContentPane().add(schedulability);
					/* MemAccCost Label & TextField */
					lblMemAccCost.setBounds(157, 368, 200, 16);
					frame.getContentPane().add(lblMemAccCost);
					memAccCost.setBounds(155, 388, 200, 22);
					frame.getContentPane().add(memAccCost);
					/* CumulContention Label & TextField */
					lblCumulContention.setBounds(157, 413, 200, 16);
					frame.getContentPane().add(lblCumulContention);
					cumulContention.setBounds(155, 433, 200, 22);
					frame.getContentPane().add(cumulContention);
					/* Computation */
					lblComputation.setBounds(157, 458, 200, 16);
					frame.getContentPane().add(lblComputation);
					computation.setBounds(155, 478, 200, 22);
					frame.getContentPane().add(computation);
					/* ResponseTime Sum Label & TextField */
					lblResponseTimeSum.setBounds(157, 503, 200, 16);
					frame.getContentPane().add(lblResponseTimeSum);
					rtSum.setBounds(155, 523, 200, 22);
					frame.getContentPane().add(rtSum);
					/* PU & RT Lists */
					for (int i = 0; i < currentPUList.size(); i++) {
						puLabelArray[i] = new JLabel(i +": " + currentPUList.get(i).getDefinition().getName());
						rtListLabelArray[i] = new JLabel("Response Time");
						puListArray[i] = new List();
						rtListArray[i] = new List();
						if (i >= 0 && i < 4) {
							puLabelArray[i].setBounds((387+(i*200)), 50, 87, 22);
							rtListLabelArray[i].setBounds((474+(i*200)), 50, 88, 22);
							puListArray[i].setBounds((385+(i*200)), 75, 87, 145);
							rtListArray[i].setBounds((475+(i*200)), 75, 87, 145);
						} else if (i >= 4) {
							puLabelArray[i].setBounds((387+((i-4)*200)), 225, 87, 22);
							rtListLabelArray[i].setBounds((474+((i-4)*200)), 225, 88, 22);
							puListArray[i].setBounds((385+((i-4)*200)), 250, 87, 145);
							rtListArray[i].setBounds((475+((i-4)*200)), 250, 87, 145);
						}
						frame.getContentPane().add(puLabelArray[i]);
						frame.getContentPane().add(rtListLabelArray[i]);
						frame.getContentPane().add(puListArray[i]);
						frame.getContentPane().add(rtListArray[i]);
					}
					/* E2ELatency Part */
					currentECList = currentModel.getConstraintsModel().getEventChains();
					if (currentECList.size() != 0) {
						final String[] ecComboArray = new String[currentECList.size()];
						for (int i = 0; i < currentECList.size(); i++) {
							ecComboArray[i] = currentECList.get(i).getName();
						}
						ecComboBox = new JComboBox<String>(ecComboArray);
						ecComboBox.setBounds(150, 570, 206, 22);
						frame.getContentPane().add(ecComboBox);
						ecComboBox.setSelectedItem(null);
					} else {
						JOptionPane.showMessageDialog(frame, "WARNING: The selected model has no Event-Chain.\n"
								+ "End-to-End Event-Chain Latency Calculation is impossible with this model.");
						// TODO: Something needs to be done here to prevent E2ELatency calculation.
					}
					frame.revalidate();
					frame.repaint();
		        }
			}
		});
		amaltheaSchBtn.setBounds(348, 12, 133, 24);
		frame.getContentPane().add(amaltheaSchBtn);

		JLabel lblEventChainModel = new JLabel("EVENT CHAIN MODEL");
		lblEventChainModel.setFont(new Font("Stencil BT", Font.PLAIN, 13));
		lblEventChainModel.setBounds(10, 570, 136, 22);
		frame.getContentPane().add(lblEventChainModel);
		
		JLabel lblRI = new JLabel("Reaction (Implicit)");
		lblRI.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 13));
		lblRI.setBounds(105, 605, 293, 16);
		frame.getContentPane().add(lblRI);
		
		JLabel lblBCRI = new JLabel("Best-Case");
		lblBCRI.setHorizontalAlignment(SwingConstants.CENTER);
		lblBCRI.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblBCRI.setBounds(10, 627, 90, 16);
		frame.getContentPane().add(lblBCRI);
		
		JTextField txtField0 = new JTextField();
		txtField0.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField0.setBounds(103, 625, 200, 22);
		txtField0.setEditable(false);
		frame.getContentPane().add(txtField0);
		
		JLabel lblWCRI = new JLabel("Worst-Case");
		lblWCRI.setHorizontalAlignment(SwingConstants.CENTER);
		lblWCRI.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblWCRI.setBounds(10, 656, 90, 16);
		frame.getContentPane().add(lblWCRI);
		
		JTextField txtField1 = new JTextField();
		txtField1.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField1.setBounds(103, 654, 200, 22);
		txtField1.setEditable(false);
		frame.getContentPane().add(txtField1);
		
		JLabel lblIRI = new JLabel("Initial Reaction (Implicit)");
		lblIRI.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 13));
		lblIRI.setBounds(105, 688, 193, 16);
		frame.getContentPane().add(lblIRI);
		
		JLabel lblBCIRI = new JLabel("Best-Case");
		lblBCIRI.setHorizontalAlignment(SwingConstants.CENTER);
		lblBCIRI.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblBCIRI.setBounds(10, 710, 90, 16);
		frame.getContentPane().add(lblBCIRI);
		
		JTextField txtField2 = new JTextField();
		txtField2.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField2.setBounds(103, 708, 200, 22);
		txtField2.setEditable(false);
		frame.getContentPane().add(txtField2);
		
		JLabel lblWCIRI = new JLabel("Worst-Case");
		lblWCIRI.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblWCIRI.setHorizontalAlignment(SwingConstants.CENTER);
		lblWCIRI.setBounds(10, 739, 90, 16);
		frame.getContentPane().add(lblWCIRI);
		
		JTextField txtField3 = new JTextField();
		txtField3.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField3.setBounds(103, 737, 200, 22);
		txtField3.setEditable(false);
		frame.getContentPane().add(txtField3);
		
		JLabel lblRL = new JLabel("Reaction (LET)");
		lblRL.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 13));
		lblRL.setBounds(415, 605, 222, 16);
		frame.getContentPane().add(lblRL);
		
		JLabel lblBCRL = new JLabel("Best-Case");
		lblBCRL.setHorizontalAlignment(SwingConstants.CENTER);
		lblBCRL.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblBCRL.setBounds(320, 627, 90, 16);
		frame.getContentPane().add(lblBCRL);
		
		JTextField txtField4 = new JTextField();
		txtField4.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField4.setBounds(413, 625, 200, 22);
		txtField4.setEditable(false);
		frame.getContentPane().add(txtField4);
		
		JLabel lblWCRL = new JLabel("Worst-Case");
		lblWCRL.setHorizontalAlignment(SwingConstants.CENTER);
		lblWCRL.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblWCRL.setBounds(320, 656, 90, 16);
		frame.getContentPane().add(lblWCRL);
		
		JTextField txtField5 = new JTextField();
		txtField5.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField5.setBounds(413, 654, 200, 22);
		txtField5.setEditable(false);
		frame.getContentPane().add(txtField5);
		
		JLabel lblIRL = new JLabel("Initial Reaction (LET)");
		lblIRL.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 13));
		lblIRL.setBounds(415, 688, 232, 16);
		frame.getContentPane().add(lblIRL);
		
		JLabel lblBCIRL = new JLabel("Best-Case");
		lblBCIRL.setHorizontalAlignment(SwingConstants.CENTER);
		lblBCIRL.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblBCIRL.setBounds(320, 710, 90, 16);
		frame.getContentPane().add(lblBCIRL);
		
		JTextField txtField6 = new JTextField();
		txtField6.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField6.setBounds(413, 708, 200, 22);
		txtField6.setEditable(false);
		frame.getContentPane().add(txtField6);
		
		JLabel lblWCIRL = new JLabel("Worst-Case");
		lblWCIRL.setHorizontalAlignment(SwingConstants.CENTER);
		lblWCIRL.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblWCIRL.setBounds(320, 739, 90, 16);
		frame.getContentPane().add(lblWCIRL);
		
		JTextField txtField7 = new JTextField();
		txtField7.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField7.setBounds(413, 737, 200, 22);
		txtField7.setEditable(false);
		frame.getContentPane().add(txtField7);
		
		JLabel lblAI = new JLabel("Age (Implicit)");
		lblAI.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 13));
		lblAI.setBounds(105, 771, 293, 16);
		frame.getContentPane().add(lblAI);
		
		JLabel lblBCAI = new JLabel("Best-Case");
		lblBCAI.setHorizontalAlignment(SwingConstants.CENTER);
		lblBCAI.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblBCAI.setBounds(10, 793, 90, 16);
		frame.getContentPane().add(lblBCAI);
		
		JTextField txtField8 = new JTextField();
		txtField8.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField8.setBounds(103, 791, 200, 22);
		txtField8.setEditable(false);
		frame.getContentPane().add(txtField8);
		
		JLabel lblWCAI = new JLabel("Worst-Case");
		lblWCAI.setHorizontalAlignment(SwingConstants.CENTER);
		lblWCAI.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblWCAI.setBounds(10, 822, 90, 16);
		frame.getContentPane().add(lblWCAI);
		
		JTextField txtField9 = new JTextField();
		txtField9.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField9.setBounds(103, 820, 200, 22);
		txtField9.setEditable(false);
		frame.getContentPane().add(txtField9);
		
		JLabel lblAL = new JLabel("Age (LET)");
		lblAL.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 13));
		lblAL.setBounds(415, 771, 232, 16);
		frame.getContentPane().add(lblAL);
		
		JLabel lblBCAL = new JLabel("Best-Case");
		lblBCAL.setHorizontalAlignment(SwingConstants.CENTER);
		lblBCAL.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblBCAL.setBounds(320, 793, 90, 16);
		frame.getContentPane().add(lblBCAL);
		
		JTextField txtField10 = new JTextField();
		txtField10.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField10.setBounds(413, 791, 200, 22);
		txtField10.setEditable(false);
		frame.getContentPane().add(txtField10);
		
		JLabel lblWCAL = new JLabel("Worst-Case");
		lblWCAL.setHorizontalAlignment(SwingConstants.CENTER);
		lblWCAL.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblWCAL.setBounds(320, 822, 90, 16);
		frame.getContentPane().add(lblWCAL);
		
		JTextField txtField11 = new JTextField();
		txtField11.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField11.setBounds(413, 820, 200, 22);
		txtField11.setEditable(false);
		frame.getContentPane().add(txtField11);
		
		JLabel lblSIR = new JLabel("Single-core Initial Reaction");
		lblSIR.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 13));
		lblSIR.setBounds(105, 865, 293, 16);
		frame.getContentPane().add(lblSIR);
		
		JLabel lblCRSIR = new JLabel("Critical-Case");
		lblCRSIR.setHorizontalAlignment(SwingConstants.CENTER);
		lblCRSIR.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblCRSIR.setBounds(10, 887, 90, 16);
		frame.getContentPane().add(lblCRSIR);
		
		JTextField txtField12 = new JTextField();
		txtField12.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField12.setBounds(103, 885, 200, 22);
		txtField12.setEditable(false);
		frame.getContentPane().add(txtField12);
		
		JLabel lblBCSIR = new JLabel("Best-Case");
		lblBCSIR.setHorizontalAlignment(SwingConstants.CENTER);
		lblBCSIR.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblBCSIR.setBounds(10, 916, 90, 16);
		frame.getContentPane().add(lblBCSIR);
		
		JTextField txtField13 = new JTextField();
		txtField13.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField13.setBounds(103, 914, 200, 22);
		txtField13.setEditable(false);
		frame.getContentPane().add(txtField13);
		
		JLabel lblWCSIR = new JLabel("Worst-Case");
		lblWCSIR.setHorizontalAlignment(SwingConstants.CENTER);
		lblWCSIR.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblWCSIR.setBounds(10, 945, 90, 16);
		frame.getContentPane().add(lblWCSIR);
		
		JTextField txtField14 = new JTextField();
		txtField14.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField14.setBounds(103, 943, 200, 22);
		txtField14.setEditable(false);
		frame.getContentPane().add(txtField14);

		JLabel lblSWCR = new JLabel("Single-core Worst-case Reaction");
		lblSWCR.setFont(new Font("Sitka Text", Font.BOLD | Font.ITALIC, 13));
		lblSWCR.setBounds(415, 865, 293, 16);
		frame.getContentPane().add(lblSWCR);
		
		JLabel lblCRSWCR = new JLabel("Critical-Case");
		lblCRSWCR.setHorizontalAlignment(SwingConstants.CENTER);
		lblCRSWCR.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblCRSWCR.setBounds(320, 887, 90, 16);
		frame.getContentPane().add(lblCRSWCR);
		
		JTextField txtField15 = new JTextField();
		txtField15.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField15.setBounds(413, 885, 200, 22);
		txtField15.setEditable(false);
		frame.getContentPane().add(txtField15);
		
		JLabel lblBCSWCR = new JLabel("Best-Case");
		lblBCSWCR.setHorizontalAlignment(SwingConstants.CENTER);
		lblBCSWCR.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblBCSWCR.setBounds(320, 916, 90, 16);
		frame.getContentPane().add(lblBCSWCR);
		
		JTextField txtField16 = new JTextField();
		txtField16.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField16.setBounds(413, 914, 200, 22);
		txtField16.setEditable(false);
		frame.getContentPane().add(txtField16);
		
		JLabel lblWCSWCR = new JLabel("Worst-Case");
		lblWCSWCR.setHorizontalAlignment(SwingConstants.CENTER);
		lblWCSWCR.setFont(new Font("Sitka Small", Font.PLAIN, 13));
		lblWCSWCR.setBounds(320, 945, 90, 16);
		frame.getContentPane().add(lblWCSWCR);
		
		JTextField txtField17 = new JTextField();
		txtField17.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField17.setBounds(413, 943, 200, 22);
		txtField17.setEditable(false);
		frame.getContentPane().add(txtField17);
		
		JButton ecSelect = new JButton("Calculate");
		ecSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (currentModel == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: Select an AMALTHEA model first.");
					return ;
				} else if (currentECList == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: Event-Chain List is empty somehow.");
					return ;
				} else if (currentECList.size() == 0) {
					JOptionPane.showMessageDialog(frame, "ERROR: There is no Event-Chain in this model.");
					return ;
				} else if (ecComboBox.getSelectedItem() == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: Select an Event-Chain.");
					return ;
				} else if (puTaskListHM == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: IA(Integer Array) has not been assigned.");
					return ;
				} else if (offloadingMode.getSelection() == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: Choose an offloading mode. (Sync / Async)");
					return ;
				} else if (schedulability_flag == false) {
					String offMode = null;
					if (SharedConsts.synchronousOffloading == true) {
						offMode = "Sync";
					} else if (SharedConsts.synchronousOffloading == false) {
						offMode = "Aync";
					}
					String tType = null;
					if (SharedConsts.timeType.equals(TimeType.WCET)) {
						tType = "WC";
					} else if (SharedConsts.timeType.equals(TimeType.ACET)) {
						tType = "AC";
					} else if (SharedConsts.timeType.equals(TimeType.BCET)) {
						tType = "BC";
					}
					String iaFeedback = "";
					for (int i = 0; i < currentIA.length; i++) {
						if (i == currentIA.length - 1) {
							iaFeedback = iaFeedback + currentIA[i];
						} else {
							iaFeedback = iaFeedback + currentIA[i] + ", ";
						}
					}
					JOptionPane.showMessageDialog(frame, "ERROR: The entered IA value is not schedulable with the current configuration.\n\n"
							+ "Current Integer Arrary: { " + iaFeedback + " }\n\n"
							+ "Current Configuration: Offloading Mode(" + offMode + "), ExecutionCase(" + tType + ")");
					return ;
				}
				
				/* If cpurta has only one core > single core mode, else > multicore mode (01/05/2020) */
				final int thisIndex = ecComboBox.getSelectedIndex();
				/* Something was selected before */
				if (selectedECIndex != -1) {
					/* If the selected Event-Chain is the same as before => Do Nothing */
					if (selectedECIndex == thisIndex) {
						return ;
					}
					/* If the selected Event-Chain is different to the previous one => Remove and Do Normal. */
					if (selectedECIndex != thisIndex) {
						for (int i = 0; i < currentTaskChain.size(); i++) {
							frame.remove(ecLabelArray[i]);
							frame.remove(ecJTPArray[i]);
						}
						selectedECIndex = thisIndex;
					}
				}
				selectedECIndex = thisIndex;
				currentEC = currentECList.get(thisIndex);
				currentTaskChain = e2e.getECTaskList(currentEC);
				if (currentTaskChain.size() == 0 ) {
					JOptionPane.showMessageDialog(frame, "ERROR: The selected Event-Chain has no chain-element.");
					txtField0.setText("Not Available.");
					txtField1.setText("Not Available.");
					txtField2.setText("Not Available.");
					txtField3.setText("Not Available.");
					txtField4.setText("Not Available.");
					txtField5.setText("Not Available.");
					txtField6.setText("Not Available.");
					txtField7.setText("Not Available.");
					return ;
				}
				else if (currentTaskChain.size() > 14) {
					JOptionPane.showMessageDialog(frame, "ERROR: The selected Event-Chain has more than " + currentTaskChain.size() + " elements. (Limit: 14)\n");
					txtField0.setText("Not Available.");
					txtField1.setText("Not Available.");
					txtField2.setText("Not Available.");
					txtField3.setText("Not Available.");
					txtField4.setText("Not Available.");
					txtField5.setText("Not Available.");
					txtField6.setText("Not Available.");
					txtField7.setText("Not Available.");
					return ;
				}
				for (int i = 0; i < currentTaskChain.size(); i++) {
					final Task task = currentTaskChain.get(i);
					for (int j = 0; j < puTaskListHM.size(); j++) {
						java.util.List<Task> puTaskList = puTaskListHM.get(j);
						if (puTaskList.stream().filter(s -> s.equals(task)).collect(Collectors.toList()).size() != 0) {
							final ProcessingUnit pu = currentPUList.get(j);
							ecLabelArray[i] = new JLabel((i+1) + ": " + pu.getName() + " (" + pu.getDefinition().getName() + ")");
							ecJTPArray[i] = new JTextField();
							ecJTPArray[i].setText(task.getName());
							ecJTPArray[i].setEditable(false);
							if (i < 7) {
								ecLabelArray[i].setBounds(750, (604+(50*i)), 187, 16);
								ecJTPArray[i].setBounds(750, (624+(50*i)), 187, 22);
							} else {
								ecLabelArray[i].setBounds(950, (604+(50*(i-7))), 187, 16);
								ecJTPArray[i].setBounds(950, (624+(50*(i-7))), 187, 22);
							}
							frame.getContentPane().add(ecLabelArray[i]);
							frame.getContentPane().add(ecJTPArray[i]);
							break;
						}
					}
				}
				
				/* Calculation Part */
				if (currentModel == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: Select an AMALTHEA model first.");
					return ;
				} else if (ecComboBox.getSelectedItem() == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: Select an Event-Chain.");
					return ;
				} else if (currentIA == null) {
					JOptionPane.showMessageDialog(frame, "ERROR: IA(Integer Array) has not been entered.");
					return ;
				}
				
				/* To find out how many processing units are used by tasks */
				ProcessingUnit pu = null;
				int puCount = 0;
				for (int i = 0; i < puTaskListHM.size(); i++) {
					final java.util.List<Task> tasks = puTaskListHM.get(i);
					if (tasks.size() > 0) {
						pu = currentPUList.get(i);
						puCount++;
					}
				}
				
				if (puCount > 1) {
					final Time bcRctIm = e2e.getBCReactionImplicit(currentEC, cpurta);
					final Time wcRctIm = e2e.getWCReactionImplicit(currentEC, cpurta);
					final Time bcInitRctIm = e2e.getBCInitialRctImplicit(currentEC, cpurta);
					final Time wcInitRctIm = e2e.getWCInitialRctImplicit(currentEC, cpurta);
					final Time bcRctLET = e2e.getBCReactionLET(currentEC, cpurta);
					final Time wcRctLET = e2e.getWCReactionLET(currentEC, cpurta);
					final Time bcInitRctLET = e2e.getBCInitialRctLET(currentEC, cpurta);
					final Time wcInitRctLET = e2e.getWCInitialRctLET(currentEC, cpurta);
					final Time bcAgeIm = e2e.getBCAgeImplicit(currentEC, cpurta);
					final Time wcAgeIm = e2e.getWCAgeImplicit(currentEC, cpurta);
					final Time bcAgeLET = e2e.getBCAgeLET(currentEC, cpurta);
					final Time wcAgeLET = e2e.getWCAgeLET(currentEC, cpurta);
					
					txtField0.setText("" + bcRctIm);
					txtField1.setText("" + wcRctIm);
					txtField2.setText("" + bcInitRctIm);
					txtField3.setText("" + wcInitRctIm);
					txtField4.setText("" + bcRctLET);
					txtField5.setText("" + wcRctLET);
					txtField6.setText("" + bcInitRctLET);
					txtField7.setText("" + wcInitRctLET);
					txtField8.setText("" + bcAgeIm);
					txtField9.setText("" + wcAgeIm);
					txtField10.setText("" + bcAgeLET);
					txtField11.setText("" + wcAgeLET);
					
					txtField12.setText("Not Single-Core.");
					txtField13.setText("Not Single-Core.");
					txtField14.setText("Not Single-Core.");
					txtField15.setText("Not Single-Core.");
					txtField16.setText("Not Single-Core.");
					txtField17.setText("Not Single-Core.");
				} else {
					final Time crSingleInitRct = e2e.getSingleCoreInitialRct(currentEC, pu, SinglecoreLatCase.CriInitRct, cpurta);
					final Time bcSingleInitRct = e2e.getSingleCoreInitialRct(currentEC, pu, SinglecoreLatCase.BCInitRct, cpurta);;
					final Time wcSingleInitRct = e2e.getSingleCoreInitialRct(currentEC, pu, SinglecoreLatCase.WCInitRct, cpurta);;
					final Time crSingleWCRct = e2e.getSingleCoreWCRct(currentEC, pu, SinglecoreLatCase.CriWCRct, cpurta);;
					final Time bcSingleWCRct = e2e.getSingleCoreWCRct(currentEC, pu, SinglecoreLatCase.BCWCRct, cpurta);;
					final Time wcSingleWCRct = e2e.getSingleCoreWCRct(currentEC, pu, SinglecoreLatCase.WCWCRct, cpurta);;
					
					txtField0.setText("Not Multi-Core.");
					txtField1.setText("Not Multi-Core.");
					txtField2.setText("Not Multi-Core.");
					txtField3.setText("Not Multi-Core.");
					txtField4.setText("Not Multi-Core.");
					txtField5.setText("Not Multi-Core.");
					txtField6.setText("Not Multi-Core.");
					txtField7.setText("Not Multi-Core.");
					txtField8.setText("Not Multi-Core.");
					txtField9.setText("Not Multi-Core.");
					txtField10.setText("Not Multi-Core.");
					txtField11.setText("Not Multi-Core.");
					
					txtField12.setText("" + crSingleInitRct);
					txtField13.setText("" + bcSingleInitRct);
					txtField14.setText("" + wcSingleInitRct);
					txtField15.setText("" + crSingleWCRct);
					txtField16.setText("" + bcSingleWCRct);
					txtField17.setText("" + wcSingleWCRct);
				}
				
				frame.revalidate();
				frame.repaint();
			}
		});
		ecSelect.setBounds(356, 569, 90, 25);
		frame.getContentPane().add(ecSelect);
		
		JButton btnNewButton_1 = new JButton("Reset");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < currentTaskChain.size(); i++) {
					ecLabelArray[i].setText(null);
					frame.remove(ecLabelArray[i]);
					ecJTPArray[i].setText(null);
					frame.remove(ecJTPArray[i]);
				}
				selectedECIndex = -1;
				txtField0.setText("");
				txtField1.setText("");
				txtField2.setText("");
				txtField3.setText("");
				txtField4.setText("");
				txtField5.setText("");
				txtField6.setText("");
				txtField7.setText("");
				txtField8.setText("");
				txtField9.setText("");
				txtField10.setText("");
				txtField11.setText("");
				txtField12.setText("");
				txtField13.setText("");
				txtField14.setText("");
				txtField15.setText("");
				txtField16.setText("");
				txtField17.setText("");
				
				frame.revalidate();
				frame.repaint();
			}
		});
		btnNewButton_1.setBounds(445, 569, 70, 25);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblTaskChain_1 = new JLabel("Task Chain >");
		lblTaskChain_1.setFont(new Font("Segoe UI Emoji", Font.BOLD, 13));
		lblTaskChain_1.setBounds(646, 605, 90, 16);
		frame.getContentPane().add(lblTaskChain_1);
		
		JLabel lblApprta = new JLabel("APP4RTA");
		lblApprta.setForeground(Color.DARK_GRAY);
		lblApprta.setFont(new Font("Snap ITC", Font.ITALIC, 20));
		lblApprta.setBounds(1050, 1, 120, 34);
		frame.getContentPane().add(lblApprta);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 557, 1160, 2);
		frame.getContentPane().add(separator);
	}
}
