package org.eclipse.app4mc.gsoc_rta.test

import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.ConstraintsBuilder
import org.junit.Test
import org.eclipse.app4mc.amalthea.model.PuType
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.amalthea.model.FrequencyUnit
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition
import org.eclipse.app4mc.amalthea.model.FrequencyDomain
import java.math.BigInteger
import org.eclipse.app4mc.amalthea.model.TimeUnit
import org.eclipse.app4mc.amalthea.model.Runnable
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.junit.Assert
import org.eclipse.app4mc.gsoc_rta.CPURta
import org.eclipse.app4mc.gsoc_rta.E2ELatency
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.ProcessEvent
import org.eclipse.app4mc.amalthea.model.EventChainItemType
import org.eclipse.app4mc.gsoc_rta.CommonUtils
import org.eclipse.app4mc.gsoc_rta.E2ELatency.SinglecoreLatCase

class E2ELatencyTest_Single {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension HardwareBuilder b2 = new HardwareBuilder
	extension StimuliBuilder b3 = new StimuliBuilder
	extension SoftwareBuilder b4 = new SoftwareBuilder
	extension ConstraintsBuilder b5 = new ConstraintsBuilder
	
	@Test
	def void E2ELatencyTest_Single(){
		org.apache.log4j.BasicConfigurator.configure();
		val model = amalthea [
			hardwareModel [
				// ***** Definitions *****
				definition_ProcessingUnit [
					name = "ARM"
					puType = PuType.CPU
				]
				
				// ***** Domains *****
				domain_Frequency [
					name = "ARM_Domain"
					defaultValue = FactoryUtil.createFrequency(1, FrequencyUnit::HZ)
				]
				
				// ***** Structures *****
				structure [
					module_ProcessingUnit [
						name = "ARM0"
						definition = _find(ProcessingUnitDefinition, "ARM")
						frequencyDomain = _find(FrequencyDomain, "ARM_Domain")
					]
				]
			]
			stimuliModel [
				periodicStimulus [
					name="periodic_03s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(3), TimeUnit.S)
				]
				periodicStimulus [
					name="periodic_05s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(5), TimeUnit.S)
				]
				periodicStimulus [
					name="periodic_06s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(6), TimeUnit.S)
				]
				periodicStimulus [
					name="periodic_10s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(10), TimeUnit.S)
				]
			]
			softwareModel [
				runnable [
					name="r0"
					activityGraph [
						ticks [^default = createDVC(1l)]
					]
				]
				runnable [
					name="r1"
					activityGraph [
						ticks [^default = createDVC(1l)]
					]
				]
				runnable [
					name="r2"
					activityGraph [
						ticks [^default = createDVC(1l)]
					]
				]
				runnable [
					name="r3"
					activityGraph [
						ticks [^default = createDVC(2l)]
					]
				]
				task [
					name="t0"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r0")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_03s"))
				]
				task [
					name="t1"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r1")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_05s"))
				]
				task [
					name="t2"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r2")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_06s"))
				]
				task [
					name="t3"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r3")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_10s"))
				]
			]
			eventModel [
				val pe0 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe0.entity= _find(Task,"t0")
				pe0.name = "pe0"
				val pe1 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe1.entity= _find(Task,"t1")
				pe1.name = "pe1"
				val pe2 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe2.entity= _find(Task,"t2")
				pe2.name = "pe2"
				val pe3 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe3.entity= _find(Task,"t3")
				pe3.name = "pe3"
				events.add(pe0)
				events.add(pe1)
				events.add(pe2)
				events.add(pe3)
			]
			constraintsModel [
				eventChain[
					name = "ec0"
					stimulus = _find(ProcessEvent,"pe0")
					response = _find(ProcessEvent,"pe3")
					subchain	[
						stimulus = _find(ProcessEvent,"pe0")
						response = _find(ProcessEvent,"pe1")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe1")
						response = _find(ProcessEvent,"pe2")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe2")
						response = _find(ProcessEvent,"pe3")
						itemType = EventChainItemType.SEQUENCE
					]
				]
				eventChain[
					name = "ec1"
					stimulus = _find(ProcessEvent,"pe3")
					response = _find(ProcessEvent,"pe0")
					subchain	[
						stimulus = _find(ProcessEvent,"pe3")
						response = _find(ProcessEvent,"pe2")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe2")
						response = _find(ProcessEvent,"pe1")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe1")
						response = _find(ProcessEvent,"pe0")
						itemType = EventChainItemType.SEQUENCE
					]
				]
				eventChain[
					name = "ec2"
					stimulus = _find(ProcessEvent,"pe0")
					response = _find(ProcessEvent,"pe3")
					subchain	[
						stimulus = _find(ProcessEvent,"pe0")
						response = _find(ProcessEvent,"pe2")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe2")
						response = _find(ProcessEvent,"pe1")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe1")
						response = _find(ProcessEvent,"pe3")
						itemType = EventChainItemType.SEQUENCE
					]
				]
				eventChain[
					name = "ec3"
					stimulus = _find(ProcessEvent,"pe3")
					response = _find(ProcessEvent,"pe0")
					subchain	[
						stimulus = _find(ProcessEvent,"pe3")
						response = _find(ProcessEvent,"pe1")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe1")
						response = _find(ProcessEvent,"pe2")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe2")
						response = _find(ProcessEvent,"pe0")
						itemType = EventChainItemType.SEQUENCE
					]
				]
			]
		]
		var cpurta = new CPURta(model);
		var ama = cpurta.getModel();
		var pu = CommonUtils.getPUs(ama).get(0)
		var ecl = new E2ELatency();
		var criInitRct = ecl.getSingleTCLsum(cpurta, pu, SinglecoreLatCase.CriInitRct).adjustUnit().toString();
		var bcInitRct = ecl.getSingleTCLsum(cpurta, pu, SinglecoreLatCase.BCInitRct).adjustUnit().toString();
		var wcInitRct = ecl.getSingleTCLsum(cpurta, pu, SinglecoreLatCase.WCInitRct).adjustUnit().toString();
		var criWCRct = ecl.getSingleTCLsum(cpurta, pu, SinglecoreLatCase.CriWCRct).adjustUnit().toString();
		var bcWCRct = ecl.getSingleTCLsum(cpurta, pu, SinglecoreLatCase.BCWCRct).adjustUnit().toString();
		var wcWCRct = ecl.getSingleTCLsum(cpurta, pu, SinglecoreLatCase.WCWCRct).adjustUnit().toString();
		Assert.assertTrue(criInitRct == "44 s")
		Assert.assertTrue(bcInitRct == "35 s")
		Assert.assertTrue(wcInitRct == "62 s")
		Assert.assertTrue(criWCRct == "47 s")
		Assert.assertTrue(bcWCRct == "49 s")
		Assert.assertTrue(wcWCRct == "76 s")
	}
	private def DiscreteValueConstant createDVC(long upper) {
		val ret = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant
		ret.value=upper
		ret
	}
}
