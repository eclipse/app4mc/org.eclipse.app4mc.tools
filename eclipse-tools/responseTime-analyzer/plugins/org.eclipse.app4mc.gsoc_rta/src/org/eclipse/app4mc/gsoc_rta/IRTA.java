/*******************************************************************************
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     FH Dortmund - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.gsoc_rta;

import java.util.HashMap;

import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType;

public interface IRTA {

	/**
	 * Get the sum of all response times contained in an AMALTHEA model
	 * @param timeType (BCET, WCET supported)
	 * @return Time response time sum across all tasks
	 */
	public Time getRTSum(final TimeType timeType);

	/**
	 * @return Hashmap<Task,Time> all tasks with corresponding response times
	 */
	public HashMap<Task, Time> getTRT();

	/**
	 * Get the response time of a task based on offsets for interProcessStimuli
	 * @param t Task
	 * @return Time reponse time
	 */
	public Time getRT(final Task t, final TimeType timeType);
}
