package org.eclipse.app4mc.gsoc_rta.test

import java.math.BigInteger
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant
import org.eclipse.app4mc.amalthea.model.FrequencyDomain
import org.eclipse.app4mc.amalthea.model.FrequencyUnit
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition
import org.eclipse.app4mc.amalthea.model.Runnable
import org.eclipse.app4mc.amalthea.model.TimeUnit
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.gsoc_rta.CPURta
import org.junit.Assert
import org.junit.Test
import org.eclipse.app4mc.amalthea.model.PuType

class CPURtaTest {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension SoftwareBuilder b2 = new SoftwareBuilder
	extension HardwareBuilder b3 = new HardwareBuilder
	extension StimuliBuilder b4 = new StimuliBuilder
	
	@Test
	def void CPURtaTest(){
		org.apache.log4j.BasicConfigurator.configure();
		val model = amalthea [
			stimuliModel [
				periodicStimulus [
					name="periodic_03s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(3), TimeUnit.S)
				]
				periodicStimulus [
					name="periodic_05s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(5), TimeUnit.S)
				]
				periodicStimulus [
					name="periodic_06s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(6), TimeUnit.S)
				]
				periodicStimulus [
					name="periodic_10s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(10), TimeUnit.S)
				]
			]
			softwareModel [
				runnable [
					name="r0"
					activityGraph [
						ticks [^default = createDVC(1l)]
					]
				]
				runnable [
					name="r1"
					activityGraph [
						ticks [^default = createDVC(1l)]
					]
				]
				runnable [
					name="r2"
					activityGraph [
						ticks [^default = createDVC(1l)]
					]
				]
				runnable [
					name="r3"
					activityGraph [
						ticks [^default = createDVC(2l)]
					]
				]
				task [
					name="t0"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r0")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_03s"))
				]
				task [
					name="t1"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r1")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_05s"))
				]
				task [
					name="t2"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r2")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_06s"))
				]
				task [
					name="t3"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r3")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_10s"))
				]
			]
			hardwareModel [
				// ***** Definitions *****
				definition_ProcessingUnit [
					name = "ARM"
					puType = PuType.CPU
				]
				
				// ***** Domains *****
				domain_Frequency [
					name = "ARM_Domain"
					defaultValue = FactoryUtil.createFrequency(1, FrequencyUnit::HZ)
				]
				
				// ***** Structures *****
				structure [
					module_ProcessingUnit [
						name = "ARM0"
						definition = _find(ProcessingUnitDefinition, "ARM")
						frequencyDomain = _find(FrequencyDomain, "ARM_Domain")
					]
				]
			]
		]
		var CPURta cpurta = new CPURta(model)
		var wcrtSum = cpurta.getRTSum(TimeType.WCET).adjustUnit().toString()
		Assert.assertTrue(wcrtSum == "15 s")
	}
	
	private def DiscreteValueConstant createDVC(long upper) {
		val ret = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant
		ret.value=upper
		ret
	}
}
