package org.eclipse.app4mc.gsoc_rta.test

import java.math.BigInteger
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant
import org.eclipse.app4mc.amalthea.model.Runnable
import org.eclipse.app4mc.amalthea.model.TimeUnit
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.junit.Test
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.ProcessEvent
import org.eclipse.app4mc.amalthea.model.EventChainItemType
import org.eclipse.app4mc.amalthea.model.builder.ConstraintsBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.PuType
import org.eclipse.app4mc.amalthea.model.FrequencyUnit
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition
import org.eclipse.app4mc.amalthea.model.FrequencyDomain
import org.eclipse.app4mc.gsoc_rta.CPURta
import org.eclipse.app4mc.gsoc_rta.E2ELatency
import org.eclipse.app4mc.gsoc_rta.E2ELatency.MulticoreLatCase
import org.junit.Assert

class E2ELatencyTest_Multi {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension HardwareBuilder b2 = new HardwareBuilder
	extension StimuliBuilder b3 = new StimuliBuilder
	extension SoftwareBuilder b4 = new SoftwareBuilder
	extension ConstraintsBuilder b5 = new ConstraintsBuilder
	
	@Test
	def void E2ELatencyTest_Multi(){
		org.apache.log4j.BasicConfigurator.configure();
		val model = amalthea [
			hardwareModel [
				// ***** Definitions *****
				definition_ProcessingUnit [
					name = "ARM"
					puType = PuType.CPU
				]
				
				// ***** Domains *****
				domain_Frequency [
					name = "ARM_Domain"
					defaultValue = FactoryUtil.createFrequency(1, FrequencyUnit::HZ)
				]
				
				// ***** Structures *****
				structure [
					module_ProcessingUnit [
						name = "ARM0"
						definition = _find(ProcessingUnitDefinition, "ARM")
						frequencyDomain = _find(FrequencyDomain, "ARM_Domain")
					]
					module_ProcessingUnit [
						name = "ARM1"
						definition = _find(ProcessingUnitDefinition, "ARM")
						frequencyDomain = _find(FrequencyDomain, "ARM_Domain")
					]
					module_ProcessingUnit [
						name = "ARM2"
						definition = _find(ProcessingUnitDefinition, "ARM")
						frequencyDomain = _find(FrequencyDomain, "ARM_Domain")
					]
				]
			]
			stimuliModel [
				periodicStimulus [
					name="periodic_03s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(3), TimeUnit.S)
				]
				periodicStimulus [
					name="periodic_05s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(5), TimeUnit.S)
				]
				periodicStimulus [
					name="periodic_06s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(6), TimeUnit.S)
				]
				periodicStimulus [
					name="periodic_10s"
					recurrence=FactoryUtil.createTime(BigInteger.valueOf(10), TimeUnit.S)
				]
			]
			softwareModel [
				runnable [
					name="r0"
					activityGraph [
						ticks [^default = createDVC(1l)]
					]
				]
				runnable [
					name="r1"
					activityGraph [
						ticks [^default = createDVC(1l)]
					]
				]
				runnable [
					name="r2"
					activityGraph [
						ticks [^default = createDVC(1l)]
					]
				]
				runnable [
					name="r3"
					activityGraph [
						ticks [^default = createDVC(2l)]
					]
				]
				task [
					name="t0"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r0")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_03s"))
				]
				task [
					name="t1"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r0")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_03s"))
				]
				task [
					name="t2"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r0")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_03s"))
				]
				task [
					name="t3"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r1")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_05s"))
				]
				task [
					name="t4"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r1")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_05s"))
				]
				task [
					name="t5"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r1")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_05s"))
				]
				task [
					name="t6"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r2")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_06s"))
				]
				task [
					name="t7"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r2")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_06s"))
				]
				task [
					name="t8"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r2")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_06s"))
				]
				task [
					name="t9"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r3")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_10s"))
				]
				task [
					name="t10"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r3")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_10s"))
				]
				task [
					name="t11"
					activityGraph [
						runnableCall [
							runnable = _find(Runnable, "r3")
						]
					]
					stimuli.add(_find(PeriodicStimulus, "periodic_10s"))
				]
			]
			eventModel [
				val pe0 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe0.entity= _find(Task,"t0")
				pe0.name = "pe0"
				val pe1 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe1.entity= _find(Task,"t1")
				pe1.name = "pe1"
				val pe2 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe2.entity= _find(Task,"t2")
				pe2.name = "pe2"
				val pe3 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe3.entity= _find(Task,"t3")
				pe3.name = "pe3"
				val pe4 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe4.entity= _find(Task,"t4")
				pe4.name = "pe4"
				val pe5 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe5.entity= _find(Task,"t5")
				pe5.name = "pe5"
				val pe6 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe6.entity= _find(Task,"t6")
				pe6.name = "pe6"
				val pe7 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe7.entity= _find(Task,"t7")
				pe7.name = "pe7"
				val pe8 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe8.entity= _find(Task,"t8")
				pe8.name = "pe8"
				val pe9 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe9.entity= _find(Task,"t9")
				pe9.name = "pe9"
				val pe10 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe10.entity= _find(Task,"t10")
				pe10.name = "pe10"
				val pe11 = AmaltheaFactory.eINSTANCE.createProcessEvent
				pe11.entity= _find(Task,"t11")
				pe11.name = "pe11"
				events.add(pe0)
				events.add(pe1)
				events.add(pe2)
				events.add(pe3)
				events.add(pe4)
				events.add(pe5)
				events.add(pe6)
				events.add(pe7)
				events.add(pe8)
				events.add(pe9)
				events.add(pe10)
				events.add(pe11)
			]
			constraintsModel [
				eventChain[
					name = "ec0"
					stimulus = _find(ProcessEvent,"pe0")
					response = _find(ProcessEvent,"pe9")
					subchain	[
						stimulus = _find(ProcessEvent,"pe0")
						response = _find(ProcessEvent,"pe4")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe4")
						response = _find(ProcessEvent,"pe8")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe8")
						response = _find(ProcessEvent,"pe9")
						itemType = EventChainItemType.SEQUENCE
					]
				]
				eventChain[
					name = "ec1"
					stimulus = _find(ProcessEvent,"pe9")
					response = _find(ProcessEvent,"pe0")
					subchain	[
						stimulus = _find(ProcessEvent,"pe9")
						response = _find(ProcessEvent,"pe7")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe7")
						response = _find(ProcessEvent,"pe5")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe5")
						response = _find(ProcessEvent,"pe0")
						itemType = EventChainItemType.SEQUENCE
					]
				]
				eventChain[
					name = "ec2"
					stimulus = _find(ProcessEvent,"pe0")
					response = _find(ProcessEvent,"pe9")
					subchain	[
						stimulus = _find(ProcessEvent,"pe0")
						response = _find(ProcessEvent,"pe7")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe7")
						response = _find(ProcessEvent,"pe5")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe5")
						response = _find(ProcessEvent,"pe9")
						itemType = EventChainItemType.SEQUENCE
					]
				]
				eventChain[
					name = "ec3"
					stimulus = _find(ProcessEvent,"pe9")
					response = _find(ProcessEvent,"pe0")
					subchain	[
						stimulus = _find(ProcessEvent,"pe9")
						response = _find(ProcessEvent,"pe4")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe4")
						response = _find(ProcessEvent,"pe8")
						itemType = EventChainItemType.SEQUENCE
					]
					subchain	[
						stimulus = _find(ProcessEvent,"pe8")
						response = _find(ProcessEvent,"pe0")
						itemType = EventChainItemType.SEQUENCE
					]
				]
			]
		]
		val ia = #[0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2]
		var cpurta = new CPURta(false, model, ia);
		var ecl = new E2ELatency();
		var bcRctIm = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.BCRctIm).adjustUnit().toString();
		var bcRctLET = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.BCRctLET).adjustUnit().toString();
		var wcRctIm = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.WCRctIm).adjustUnit().toString();
		var wcRctLET = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.WCRctLET).adjustUnit().toString();
		var bcInitRctIm = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.BCInitRctIm).adjustUnit().toString();
		var bcInitRctLET = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.BCInitRctLET).adjustUnit().toString();
		var wcInitRctIm = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.WCInitRctIm).adjustUnit().toString();
		var wcInitRctLET = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.WCInitRctLET).adjustUnit().toString();
		var bcAgeIm = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.BCAgeIm).adjustUnit().toString();
		var bcAgeLET = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.BCAgeLET).adjustUnit().toString();
		var wcAgeIm = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.WCAgeIm).adjustUnit().toString();
		var wcAgeLET = ecl.getMultiTCLsum(cpurta, MulticoreLatCase.WCAgeLET).adjustUnit().toString();
		Assert.assertTrue(bcRctIm == "20 s")
		Assert.assertTrue(bcRctLET == "96 s")
		Assert.assertTrue(wcRctIm == "122 s")
		Assert.assertTrue(wcRctLET == "166 s")
		Assert.assertTrue(bcInitRctIm == "20 s")
		Assert.assertTrue(bcInitRctLET == "96 s")
		Assert.assertTrue(wcInitRctIm == "108 s")
		Assert.assertTrue(wcInitRctLET == "150 s")
		Assert.assertTrue(bcAgeIm == "12 s")
		Assert.assertTrue(bcAgeLET == "26 s")
		Assert.assertTrue(wcAgeIm == "114 s")
		Assert.assertTrue(wcAgeLET == "69 s")
	}
	private def DiscreteValueConstant createDVC(long upper) {
		val ret = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant
		ret.value=upper
		ret
	}
}
