/*******************************************************************************
 * Copyright (c) 2019 Dortmund University of Applied Sciences and Arts.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     FH Dortmund - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.gsoc_rta;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.eclipse.app4mc.gsoc_rta.IRTA;
import org.eclipse.app4mc.gsoc_rta.CommonUtils;
import org.eclipse.app4mc.gsoc_rta.SharedConsts;
import org.eclipse.app4mc.gsoc_rta.Contention;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaServices;
import org.eclipse.app4mc.amalthea.model.ClearEvent;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.Preemption;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.PuType;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SetEvent;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.WaitEvent;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;
import org.eclipse.emf.common.util.EList;

public class CPURta implements IRTA {
	final Logger log = Logger.getLogger(CPURta.class);
	private Amalthea model;
	
	/**
	 * CPURta Constructor
	 * @param forFunctions
	 * 					true: when creating a class instance just to use functions
	 * 					false: when creating a default class instance for CPURta simulation
	 * @param file		target Amalthea file
	 * @param ia		interger array mapping model
	 */
	public CPURta(final boolean forFunctions, final Amalthea model, final int[] ia) {
		if (!forFunctions) {
			if (model == null) {
				log.error("The file is empty. Please check the model path.");
				return;
			}
			this.setModel(model);
			this.setTRT(getDefaultTRT(model));
			this.setIA(ia);
			this.setPUl(CommonUtils.getPUs(model));
			this.setContention(ia, model);
		}
	}

	/**
	 * CPURta Constructor -	when manually creating a class instance for CPURta simulation
	 * @param model			the observed Amalthea Model
	 * @param trt			the observed HashMap Task with Response Time (null works => the default testing trt will be assigned)
	 * @param ia			the observed IntegerArray Mapping Model
	 * @param pul			the observed List of ProcessingUnits
	 */
	public CPURta(final Amalthea model, final HashMap<Task, Time> trt, final int[] ia, final List<ProcessingUnit> pul) {
		if (model == null) {
			log.error("Model is empty. Please check the model path.");
			return;
		}
		this.setModel(model);
		if (trt == null) {
			this.setTRT(this.getDefaultTRT(model));
		} else {
			this.setTRT(trt);
		}
		this.setIA(ia);
		if (pul == null) {
			this.setPUl(CommonUtils.getPUs(model));
		} else {
			this.setPUl(pul);
		}
		this.setContention(ia, model);
	}

	/**
	 * CPURta Constructor - when there is only one processing unit inside the model. (Therefore, IA mapping is not required.)
	 * @param model
	 */
	public CPURta(final Amalthea model) {
		if (model == null) {
			log.error("Model is empty. Please check the model path.");
		}
		this.setModel(model);
		final int numOfTasks = model.getSwModel().getTasks().size();
		final int[] ia = new int[numOfTasks];
		for (int i = 0; i < numOfTasks; i++) {
			ia[i] = 0;
		}
		this.setTRT(getDefaultTRT(model));
		this.setIA(ia);
		this.setPUl(CommonUtils.getPUs(model));
		this.setContention(ia, model);
	}
	
	/**
	 * Since this method is used by RTARuntimeUtil, the visibility should be 'public'
	 * @return
	 * 			The Amalthea model instance of the current CPURta class
	 */
	public Amalthea getModel() {
		return this.model;
	}

	/**
	 * Set the Amalthea model object of the current CPURta class as the given Amalthea model parameter.
	 * @param pAmalthea			the parameter Amalthea model which would reinitialize the Amalthea model
	 */
	public void setModel(final Amalthea pAmalthea) {
		this.model = pAmalthea;
		this.setGTCL(this.model);
	}

	private HashMap<Task, Time> trt = null;

	/**
	 * Since this method is used by GAMapping, the visibility should be 'public'
	 * @return
	 * 			trt(task with response time) HashMap variable
	 */
	@Override
	public HashMap<Task, Time> getTRT() {
		return this.trt;
	}

	/**
	 * Set the trt(task with response time) HashMap variable as the given trtp parameter variable.
	 * Which would contains GPU tasks' response times from the beginning
	 * @param trtp				the HashMap variable parameter which would reinitialize the trt variable of the current CPURta class.
	 */
	public void setTRT(final HashMap<Task, Time> trtp) {
		this.trt = trtp;
	}

	/**
	 * Get the default trt(task with response time) value in case of running CPURta class itself.
	 * @param model				the current class's Amalthea model which is used to get the trt value out of it
	 * @return
	 * 			the trt value which is derived out of the given parameter Amalthea model
	 */
	public HashMap<Task, Time> getDefaultTRT(final Amalthea model) {
		final HashMap<Task, Time> trt = new HashMap<Task, Time>();
		final EList<Task> allTaskList = model.getSwModel().getTasks();
		final long val = 2000000000;
		for (final Task t : allTaskList) {
			if (this.gpuTaskList.contains(t)) {
				trt.put(t, FactoryUtil.createTime(BigInteger.valueOf(val), TimeUnit.PS));
			}
			else {
				trt.put(t, FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS));
			}
		}
		return trt;
	}

	private int[] tpuMapping = null;

	/**
	 * Since this method is used by RTARuntimeUtil, the visibility should be 'protected'
	 *
	 * @return
	 * 			The tpuMapping integer array
	 */
	public int[] getIA() {
		return this.tpuMapping;
	}

	/**
	 * Set the tpuMapping integer array (which would be used to modify the current task mapping to different Processing Units)
	 * as the given tpumap parameter variable.
	 * (The integer array would be used to modify the current task mapping to different Processing Units)
	 * @param tpumap			the parameter integer array which would reinitialize the tpuMapping array of the current CPURta class.
	 */
	public void setIA(final int[] tpumap) {
		if (tpumap == null) {
			this.tpuMapping = null;
		} else if (tpumap.length == this.model.getSwModel().getTasks().size()) {
			this.tpuMapping = tpumap;
		} else {
			log.error("integer array size MUST match the number of tasks");
		}
	}

	private List<ProcessingUnit> pul = new ArrayList<>();

	/**
	 * Get a list of ProcessingUnits
	 * @return
	 * 			this.pul
	 */
	public List<ProcessingUnit> getPUl() {
		return this.pul;
	}

	/**
	 * Set the pul, the list of Processing Units variable (which would be used to map tasks along with tpuMapping array)
	 * as the given pul parameter variable.
	 * @param pul 				the parameter pul list which would reinitialize the pul list variable of the current CPURta class
	 */
	public void setPUl(final List<ProcessingUnit> pul) {
		this.pul = pul;
	}

	private Contention ct = null;

	/**
	 * Get the contention instance
	 * @return
	 * 			this.ct
	 */
	protected Contention getCT() {
		return this.ct;
	}

	/**
	 * Set the ct (Contention) variable which would be used to calculate memory contention of the given task that will be added up to the response time result
	 * as the newly generated contention instance with the given integer array
	 * and Amalthea model parameters.
	 * @param ia				Integer Array which is referred to map tasks of the given model
	 * @param model				Amalthea model
	 */
	public void setContention(final int[] ia, final Amalthea model) {
		this.ct = new Contention(ia, model);
	}

	private List<Task> gpuTaskList = new ArrayList<Task>();

	/**
	 * Since this method is used by RTARuntimeUtil, the visibility should be 'protected'
	 * @return
	 * 			gpuTaskList which contains tasks (which are originally designed for GPU) of the Amalthea model
	 */
	protected List<Task> getGpuTaskList() {
		return this.gpuTaskList;
	}

	private final List<Task> triggeringTaskList = new ArrayList<Task>();
	/**
	 * Since this method is used by RTARuntimeUtil, the visibility should be 'protected'
	 * @return
	 * 			triggeringTaskList which contains tasks (which contain InterProcessTrigger) of the Amalthea model
	 */
	protected List<Task> getTriggeringTaskList() {
		return this.triggeringTaskList;
	}

	/* this runnable is used to calculate execution time in RTARuntimeUtil class */
	protected Runnable offloadingAsyncRunnable = null;

	private final HashMap<Task, List<List<Label>>> gpuToCpuLabels = new HashMap<Task, List<List<Label>>>();

	/**
	 * Since this method is used by RTARuntimeUtil, the visibility should be 'protected'
	 * @return
	 * 			gpuToCpuLabels HashMap which contains required labels (of the corresponding task)
	 * 			that need to be taken into account when GPU tasks are mapped to CPU
	 */
	protected HashMap<Task, List<List<Label>>> getGTCL() {
		return this.gpuToCpuLabels;
	}

	/**
	 * Not only set gpuToCpuLabels HashMap, this also set gpuTaskList (only contains GPU tasks),
	 * triggeringTaskList (only contains tasks with InterProcessTrigger)
	 * and offloadingAsyncRunnable (the Runnable that is taken into account for triggering tasks when the mode is asynchronous)
	 * @param model				the parameter Amalthea model which is used to derived required labels of the corresponding GPU task
	 */
	private void setGTCL(final Amalthea model) {
		final EList<Task> allTaskList = model.getSwModel().getTasks();
		this.gpuTaskList = allTaskList.stream().filter(s -> s.getStimuli().get(0) instanceof InterProcessStimulus).collect(Collectors.toList());
		/* find the triggering tasks */
		for (final Task t : allTaskList) {
			final List<ActivityGraphItem> triggerList = SoftwareUtil.collectActivityGraphItems(t.getActivityGraph(), null, (call -> call instanceof InterProcessTrigger));
			if (triggerList.size() != 0) {
				this.triggeringTaskList.add(t);
				if (RTARuntimeUtil.doesTaskHaveAsyncRunnable(t, this)) {
					final List<ActivityGraphItem> cList = SoftwareUtil.collectActivityGraphItems(t.getActivityGraph(), null, (call -> call instanceof RunnableCall
							|| call instanceof InterProcessTrigger || call instanceof ClearEvent || call instanceof SetEvent || call instanceof WaitEvent));
					final int waitIndex = cList.indexOf(cList.stream().filter(s -> s instanceof WaitEvent).iterator().next());
					final int asyncOffloadingIndex = waitIndex + 1;
					if (cList.get(asyncOffloadingIndex) instanceof RunnableCall) {
						this.offloadingAsyncRunnable = ((RunnableCall) cList.get(asyncOffloadingIndex)).getRunnable();
					}
				}
			}
		}
		for (final Task t : this.gpuTaskList) {
			final InterProcessStimulus ips = (InterProcessStimulus) (t.getStimuli().get(0));
			Task triggeringTask = null;
			for (final Task tt : this.triggeringTaskList) {
				final InterProcessTrigger ipt = (InterProcessTrigger) SoftwareUtil
						.collectActivityGraphItems(tt.getActivityGraph(), null, (call -> call instanceof InterProcessTrigger)).stream().iterator().next();
				if (ips.equals(ipt.getStimulus())) {
					triggeringTask = tt;
					break;
				}
			}
			final List<Label> readLabelList = new ArrayList<Label>();
			final List<Label> writeLabelList = new ArrayList<Label>();
			if (null != triggeringTask) {
				final List<ActivityGraphItem> callList = SoftwareUtil.collectActivityGraphItems(triggeringTask.getActivityGraph(), null,
						(call -> call instanceof RunnableCall || call instanceof InterProcessTrigger || call instanceof ClearEvent || call instanceof SetEvent
								|| call instanceof WaitEvent));
				final InterProcessTrigger ipt = (InterProcessTrigger) callList.stream().filter(s -> s instanceof InterProcessTrigger).iterator().next();
				final int indexforTrigger = callList.indexOf(ipt);
				for (int i = 0; i < callList.size(); i++) {
					Runnable thisRunnable = null;
					/* Pre-processing Runnable */
					if ((i < indexforTrigger) && (callList.get(i) instanceof RunnableCall)) {
						thisRunnable = ((RunnableCall) callList.get(i)).getRunnable();
						final List<LabelAccess> thisLAList = SoftwareUtil.getLabelAccessList(thisRunnable, null);
						for (final LabelAccess la : thisLAList) {
							if (la.getAccess().equals(LabelAccessEnum.READ)) {
								readLabelList.add(la.getData());
							}
						}
					}
					/* Post-processing Runnable */
					else if ((i > indexforTrigger) && (callList.get(i) instanceof RunnableCall)) {
						thisRunnable = ((RunnableCall) callList.get(i)).getRunnable();
						final List<LabelAccess> thisLAList = SoftwareUtil.getLabelAccessList(thisRunnable, null);
						for (final LabelAccess la : thisLAList) {
							if (la.getAccess().equals(LabelAccessEnum.WRITE)) {
								writeLabelList.add(la.getData());
							}
						}
					}
				}
			}
			final List<List<Label>> aryofLabelList = new ArrayList<List<Label>>();
			aryofLabelList.add(readLabelList);
			aryofLabelList.add(writeLabelList);
			this.gpuToCpuLabels.put(t, aryofLabelList);
		}
	}

	/* To calculate pure memory access latency cost (without contention & pure computation(Ticks)) */
	protected Time cumuAcTime = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);

	/**
	 * @return
	 * 			cumuAcTime, the total time of latency (Read & Write memory access) from CPUs
	 */
	public Time getCumulatedMemAccCosts() {
		return this.cumuAcTime;
	}
	
	/**
	 * Initializing cumuAcTime to 0 ps
	 */
	public void initCumulatedMemAccCosts() {
		this.cumuAcTime = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
	}

	/* To calculate pure latency cost (without memory access latency & pure computation(Ticks)) */
	protected Time cumuConTime = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
	
	/**
	 * @return
	 * 			cumulatedCon, the total time of latency (Contention of the task)
	 */
	public Time getCumulatedContention() {
		return this.cumuConTime;
	}
	
	/**
	 * Initializing cumulatedCon to 0 ps
	 */
	public void initCumulatedContention() {
		this.cumuConTime = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
	}
	
	HashMap<Task, Time> accessCosts = new HashMap<>();

	public HashMap<Task, Time> getAccessCosts() {
		return this.accessCosts;
	}

	/**
	 * Calculate the total sum of response times of the tasks of the given Amalthea model with the mapping model (tpuMapping)
	 * @param executionCase		BCET, ACET, WCET
	 * @return
	 * 			total sum of all tasks' response times of the given mapped model (tpuMapping)
	 */
	@Override
	public Time getRTSum(final TimeType executionCase) {
		if (this.model == null) {
			log.error("No Model Loaded!");
			return null;
		} else if (this.trt == null) {
			log.error("No HashMap Loaded!");
			return null;
		} else if (this.tpuMapping == null) {
			log.error("No IntegerArray Loaded!");
			return null;
		} else if (this.pul == null) {
			log.error("No PUList Loaded!");
			return null;
		}
		this.setContention(this.getIA(), this.getModel());
		Time time = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
		final EList<Task> allTaskList = this.model.getSwModel().getTasks();
		for (int i = 0; i < this.tpuMapping.length; i++) {
			final Time rt = getTaskRTIA(allTaskList.get(i), executionCase);
			if (rt.getValue().equals(BigInteger.valueOf(Long.MAX_VALUE)) && !SharedConsts.ignoreInfeasibility) {
				return rt;
			}
			time = time.add(rt);
			if (!rt.getValue().equals(BigInteger.ZERO)) {
				/* only put CPU tasks */
				this.trt.put(this.model.getSwModel().getTasks().get(i), rt);
			}
		}
		return time;
	}

	/**
	 * Calculate worst-case response time of the given task of the given Amalthea model with the mapping model (tpuMapping)
	 * @param task				the observed task
	 * @param executionCase		BCET, ACET, WCET
	 * @return
	 * 			response time of the observed task
	 */
	protected Time getTaskRTIA(final Task task, final TimeType executionCase) {
		/* 1. validate thisTask is mapped to CPU */
		final int tindex = this.model.getSwModel().getTasks().indexOf(task);
		final int puindex = this.tpuMapping[tindex];
		final ProcessingUnit pu = this.pul.get(puindex);
		if (!pu.getDefinition().getPuType().equals(PuType.CPU)) {
			Logger.getLogger(CPURta.class).debug(task.getName() + " is not mapped to a CPU");
			return FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
		}
		/* 2. get all tasks mapped to this CPU */
		final List<Task> puTaskList = new ArrayList<Task>();
		for (int i = 0; i < this.tpuMapping.length; i++) {
			if (this.tpuMapping[i] == puindex) {
				puTaskList.add(this.model.getSwModel().getTasks().get(i));
			}
		}
		final List<Task> sortedTaskList = taskSorting(puTaskList);
		if (executionCase.equals(TimeType.BCET)) {
			return this.bestCaseCPURT(task, sortedTaskList, pu);
		} else if (executionCase.equals(TimeType.WCET)) {
			return SharedConsts.levelIBusyPeriod ? this.responseTimeLevelI(task, sortedTaskList, TimeType.WCET, pu)
					: this.preciseTestCPURT(task, sortedTaskList, TimeType.WCET, pu);
		}
		return SharedConsts.levelIBusyPeriod ? this.responseTimeLevelI(task, sortedTaskList, TimeType.WCET, pu)
				: this.preciseTestCPURT(task, sortedTaskList, TimeType.ACET, pu);
	}

	/**
	 * Sort out the given list of tasks (in order of shorter period first - Rate Monotonic Scheduling)
	 * @param taskList			list of tasks that is mapped to the same core
	 * @return
	 * 			the sorted list of tasks
	 */
	protected List<Task> taskSorting(final List<Task> taskList) {
		/* Getting stimuliList out of the given taskList (because it is RMS) */
		final List<Time> stimuliList = new ArrayList<>();
		for (final Task t : taskList) {
			stimuliList.add(CommonUtils.getStimInTime(t));
		}
		/* Sorting (Shortest Period(Time) first) */
		Collections.sort(stimuliList, new TimeCompIA());
		/* Sort tasks to the newTaskList in order of Period length (shortest first
		 * longest last)-(according to the stimuliList) */
		final List<Task> newTaskList = new ArrayList<>();
		for (int i = 0; i < stimuliList.size(); i++) {
			for (final Task t : taskList) {
				if ((!newTaskList.contains(t)) && (stimuliList.get(i).compareTo(CommonUtils.getStimInTime(t)) == 0)) {
					newTaskList.add(t);
				}
			}
		}
		return newTaskList;
	}

	/**
	 * Calculate response time of the observed task according to the worst-case RMS response time analysis algorithm.
	 * @param task				the observed task
	 * @param taskList			list of tasks that is mapped to the same core
	 * @param executionCase		BCET, ACET, WCET
	 * @param pu				ProcessingUnit that would compute the given runnable (A57 or Denver)
	 * @return
	 * 			the worst-case response time of the observed task
	 */
	public Time preciseTestCPURT(final Task task, final List<Task> taskList, final TimeType executionCase, final ProcessingUnit pu) {
		Time thisRT = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
		Time period = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
		if (taskList.size() == 0) {
			log.debug("!!! This taskList is empty so I am returning MAX !!!");
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		/* to check if the given task is in the taskList */
		int flag = 0;
		int index = 0;
		for (int i = 0; i < taskList.size(); i++) {
			if (task.equals(taskList.get(i))) {
				flag = 1;
				index = i;
				break;
			}
		}
		if (flag == 0) {
			log.debug("!!! Nothing in the taskList matches the given Task !!! So I am returning 0s" + " --- thisTask: " + task.getName());
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		final RTARuntimeUtil rtaut = new RTARuntimeUtil();
		period = CommonUtils.getStimInTime(taskList.get(index));
		if (index == 0) {
			thisRT = rtaut.getExecutionTimeforCPUTaskIA(taskList.get(index), pu, executionCase, this);
			if (thisRT.compareTo(period) <= 0) {
				/* To analyze the pure latency time */
				if (thisRT.compareTo(FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS)) > 0) {
					this.cumuAcTime = this.cumuAcTime.add(rtaut.getTaskMemoryAccessTime(task, pu, executionCase));
					if (this.getIA() != null) {
						this.cumuConTime = this.cumuConTime.add(this.getCT().contentionForTask(task));
					}
				}
				return thisRT;
			}
			log.debug("!!! This is non schedulable...!!! Because of the period " + period + " being less than the execution time of " + thisRT
					+ " for task " + task.getName());
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		else {
			/* In the case of a COOPERATIVE Preemption typed Task */
			if (taskList.get(index).getPreemption().equals(Preemption.COOPERATIVE)) {
				// TODO: Blocking
			}
			final Time thisExeTime = rtaut.getExecutionTimeforCPUTaskIA(taskList.get(index), pu, executionCase, this);
			if (thisExeTime.compareTo(FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS)) == 0) {
				return thisExeTime;
			} else if (thisExeTime.compareTo(period) > 0) {
				log.debug("!!! This is non schedulable...!!! Because of the period " + period + " being less than the execution time of " + thisRT
						+ " for task " + task.getName());
				return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
			}
			Time cumulativeRT = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
			/* 1. add all the execution time till the index - RT_0 */
			for (int j = 0; j < index + 1; j++) {
				final Time thisTime = rtaut.getExecutionTimeforCPUTaskIA(taskList.get(j), pu, executionCase, this);
				cumulativeRT = cumulativeRT.add(thisTime);
			}
			if (cumulativeRT.compareTo(period) <= 0) {
				while (true) {
					Time excepThisExeTime = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
					for (int j = 0; j < index; j++) {
						Time localPeriod = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
						localPeriod = CommonUtils.getStimInTime(taskList.get(j));
						final Time preExeTime = rtaut.getExecutionTimeforCPUTaskIA(taskList.get(j), pu, executionCase, this);
						final double ri_period = Math.ceil(cumulativeRT.divide(localPeriod));
						excepThisExeTime = excepThisExeTime.add(preExeTime.multiply(ri_period));
					}
					Time cumulativeRT_x = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
					cumulativeRT_x = thisExeTime.add(excepThisExeTime);
					if (cumulativeRT_x.compareTo(period) <= 0) {
						if (cumulativeRT_x.compareTo(cumulativeRT) == 0) {
							thisRT = cumulativeRT_x;
							/* To analyze the pure latency time */
							if (thisRT.compareTo(FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS)) > 0) {
								this.cumuAcTime = this.cumuAcTime.add(rtaut.getTaskMemoryAccessTime(task, pu, executionCase));
								if (this.getIA() != null) {
									this.cumuConTime = this.cumuConTime.add(this.getCT().contentionForTask(task));
								}
							}
							return thisRT;
						}
						cumulativeRT = cumulativeRT_x;
					} else {
						log.debug("!!! This is non schedulable...!!! Because of the period " + period
							+ " being less than the response time (cumulativeRT_x) of " + cumulativeRT_x + " for task " + task.getName());
						if (SharedConsts.ignoreInfeasibility) {
							return cumulativeRT_x;
						}
						return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
					}
				}
			}
			log.debug("!!! This is non schedulable...!!! Because of the period " + period + " being less than the response time of " + cumulativeRT
				+ " for task " + task.getName());
			if (SharedConsts.ignoreInfeasibility) {
				return cumulativeRT;
			}
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
	}
	
	/**
	 * Calculate response time of the observed task according to the best-case RMS response time analysis algorithm.
	 * (source: https://www.researchgate.net/publication/3958297_Exact_best-case_response_time_analysis_of_fixed_priority_scheduled_tasks (equ_16))
	 * @param task				the observed task
	 * @param taskList			list of tasks that is mapped to the same core
	 * @param pu				ProcessingUnit that would compute the given runnable (A57 or Denver)
	 * @return
	 * 			the best-case response time of the observed task
	 */
	public Time bestCaseCPURT(final Task task, final List<Task> taskList, final ProcessingUnit pu) {
		Time bcrt = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
		if (taskList.size() == 0) {
			log.debug("!!! This taskList is empty so I am returning MAX !!!");
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		} else if (!taskList.contains(task)) {
			log.debug("!!! This taskList is empty so I am returning MAX !!!");
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		/* to check if the given task is in the taskList */
		int flag = 0;
		int index = 0;
		for (int i = 0; i < taskList.size(); i++) {
			if (task.equals(taskList.get(i))) {
				flag = 1;
				index = i;
				break;
			}
		}
		if (flag == 0) {
			log.debug("!!! Nothing in the taskList matches the given Task !!! So I am returning 0s" + " --- thisTask: " + task.getName());
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		final RTARuntimeUtil rtaut = new RTARuntimeUtil();
		Time bR_0 = this.preciseTestCPURT(task, taskList, TimeType.BCET, pu);
		for (int i = 0; i < index + 1; i++) {
			if (index == 0) {
				bcrt = bR_0;
				break;
			}
			else if (i == index) {
				final Time i_BCET = rtaut.getExecutionTimeforCPUTaskIA(task, pu, TimeType.BCET, this);
				final Time rt_0 = this.preciseTestCPURT(task, taskList, TimeType.BCET, pu);
				Time rt_n = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
				boolean isItFirst = true;
				while (true) {
					Time sum = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
					for (int j = 0; j < i; j++) {
						final Task curTask = taskList.get(j);
						final Time curTask_period = CommonUtils.getStimInTime(curTask);
						final Time curTask_BCET = rtaut.getExecutionTimeforCPUTaskIA(curTask, pu, TimeType.BCET, this);
						if (isItFirst) {
							double ceil_value = Math.ceil((rt_0.subtract(curTask_period)).divide(curTask_period));
							if (ceil_value < 0) {
								ceil_value = 0;
							}
							final Time curIterTime = curTask_BCET.multiply(ceil_value);
							sum = sum.add(curIterTime);
						} else {
							double ceil_value = Math.ceil((rt_n.subtract(curTask_period)).divide(curTask_period));
							if (ceil_value < 0) {
								ceil_value = 0;
							}
							final Time curIterTime = curTask_BCET.multiply(ceil_value);
							sum = sum.add(curIterTime);
						}
					}
					Time temp = i_BCET.add(sum);
					if(isItFirst) {
						if (temp.compareTo(rt_0) == 0) {
							bcrt = temp;
							break;
						} else {
							rt_n = temp;
						}
					} else {
						if (temp.compareTo(rt_n) == 0) {
							bcrt = temp;
							break;
						} else {
							rt_n = temp;
						}
					}
					isItFirst = false;
				}
			}
		}
		return bcrt;
	}

	/** New method implement from the news paper with Level_i
	 * @param thisTask
	 * @param thisTaskList
	 * @param executionCase
	 * @param pu
	 * @return
	 */
	private Time responseTimeLevelI(final Task thisTask, final List<Task> thisTaskList, final TimeType executionCase, final ProcessingUnit pu) {
		Time thisRT = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
		Time period = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);

		if (thisTaskList.size() == 0) {
			log.debug("!!! This taskList is empty !!!");
			// TODO: Catch Exception
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}

		/* to check if the given task is in the thisTaskList */
		int flag = 0;
		int index = 0;
		for (int i = 0; i < thisTaskList.size(); i++) {
			if (thisTask.equals(thisTaskList.get(i))) {
				flag = 1;
				index = i;
				break;
			}
		}
		/*
		 * the problem seems like once a gpu task (mapped to cpu) changes its stimulus it becomes completely new (or maybe not)
		 */
		if (flag == 0) {
			log.debug("!!! Nothing in the taskList matches the given Task !!!" + " --- thisTask: " + thisTask.getName());

			// TODO: Catch Exception
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		final RTARuntimeUtil rtaut = new RTARuntimeUtil();
		final Time checkingTime = rtaut.getExecutionTimeforCPUTaskIA(thisTask, pu, executionCase, this);

		if (checkingTime.getValue().longValue() == 0) {

			return checkingTime;
		}

		/* Checking whether task list is schedulable or not */
		double UtilizationValue = 0.0;
		for (final Task task : thisTaskList) {
			final Time exeTime = rtaut.getExecutionTimeforCPUTaskIA(task, pu, executionCase, this);
			final Time periodTime = CommonUtils.getStimInTime(task);
			final BigInteger exeTimePico = AmaltheaServices.convertToPicoSeconds(exeTime);
			final BigInteger periodTimePico = AmaltheaServices.convertToPicoSeconds(periodTime);

			UtilizationValue += (exeTimePico.doubleValue()) / (periodTimePico.doubleValue());
		}

		if (UtilizationValue > 1.0) {
			final Time rpTime = FactoryUtil.createTime(Long.MAX_VALUE, TimeUnit.PS);
			return rpTime;
		}

		/* ----------------------------------------------- */
		/* Start applying level i approach from here */
		for (int i = 0; i < index + 1; i++) {
			period = CommonUtils.getStimInTime(thisTaskList.get(i));
			if (index == 0) {
				thisRT = rtaut.getExecutionTimeforCPUTaskIA(thisTaskList.get(i), pu, executionCase, this);
				if (thisRT.compareTo(period) <= 0) {
					if (thisRT.compareTo(FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS)) > 0) {
						this.cumuAcTime = this.cumuAcTime.add(rtaut.getTaskMemoryAccessTime(thisTask, pu, executionCase));
					}
					return thisRT;
				}
				log.debug("!!! This is non schedulable...!!! Because of the period " + period + " is less than the execution time of " + thisRT + " for task "
						+ thisTask.getName());

				// TODO: Catch Exception
				return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
			}
			else if (i == index) {
				/* In the case of a COOPERATIVE Preemption typed Task */
				if (thisTaskList.get(i).getPreemption().equals(Preemption.COOPERATIVE)) {
					// TODO: Blocking
				}
				final Time thisExeTime = rtaut.getExecutionTimeforCPUTaskIA(thisTaskList.get(i), pu, executionCase, this);
				if (thisExeTime.compareTo(FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS)) == 0) {
					return thisExeTime;
				}
				else if (thisExeTime.compareTo(period) > 0) {
					log.debug("!!! This is non schedulable...!!! Because of the period " + period + " being less than the execution time of " + thisRT
							+ " for task " + thisTask.getName());
					// TODO: Catch Exception
					return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
				}

				BigDecimal l_value = BigDecimal.valueOf(1.0);
				BigDecimal currentL_value = BigDecimal.valueOf(1.0);
				BigDecimal temp = BigDecimal.valueOf(2.0);
				BigDecimal localSum = BigDecimal.valueOf(0.0);
				while (localSum.compareTo(temp) != 0) {
					localSum = BigDecimal.valueOf(0);
					for (int j = 0; j < i + 1; j++) {
						temp = currentL_value;
						Time thePeriod = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
						Time localExe = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
						thePeriod = CommonUtils.getStimInTime(thisTaskList.get(j));

						localExe = rtaut.getExecutionTimeforCPUTaskIA(thisTaskList.get(j), pu, executionCase, this);
						final double periodInDouble = AmaltheaServices.convertToPicoSeconds(thePeriod).doubleValue();
						final double exeInDouble = AmaltheaServices.convertToPicoSeconds(localExe).doubleValue();
						final BigDecimal periodInDoubleBD = BigDecimal.valueOf(periodInDouble);
						final BigDecimal exeInDoubleBD = BigDecimal.valueOf(exeInDouble);

						/* l_value = Math.ceil(currentL_value / periodInDouble) * exeInDouble; */
						l_value = currentL_value.divide(periodInDoubleBD, 0, BigDecimal.ROUND_UP).multiply(exeInDoubleBD);
						localSum = localSum.add(l_value);
					}
					currentL_value = localSum;

				}
				final BigDecimal taskPeriodBD = BigDecimal.valueOf(AmaltheaServices.convertToPicoSeconds(period).doubleValue());

				/*
				 * final int k_value = (int) Math.ceil(localSum /AmaltheaServices.convertToPicoSeconds(period).doubleValue());
				 */
				final BigDecimal k_value = localSum.divide(taskPeriodBD, 0, BigDecimal.ROUND_UP);
				// System.out.println("k value = " + k_value);
				final List<Double> rtList = new ArrayList<Double>();

				// double fl_value = 1.0;
				BigDecimal fl_value = BigDecimal.valueOf(1.0);
				// double ftemp = 3.0;
				BigDecimal ftemp = BigDecimal.valueOf(3.0);

				for (int j = 1; j <= k_value.intValue(); j++) {
					ftemp = BigDecimal.valueOf(3.0);
					while (fl_value.compareTo(ftemp) != 0) {
						// double flocalSum = 0.0;
						BigDecimal flocalSum = BigDecimal.valueOf(0.0);
						// double fSum = 0.0;
						BigDecimal fSum = BigDecimal.valueOf(0.0);
						// Assign temp for comparation
						ftemp = fl_value;
						for (int m = 0; m < i; m++) {
							Time thePeriod = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
							Time localExe = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
							thePeriod = CommonUtils.getStimInTime(thisTaskList.get(j));
							localExe = rtaut.getExecutionTimeforCPUTaskIA(thisTaskList.get(m), pu, executionCase, this);
							final BigInteger periodInteger = AmaltheaServices.convertToPicoSeconds(thePeriod);
							final BigInteger exeInteger = AmaltheaServices.convertToPicoSeconds(localExe);
							// Value parsing
							final double Tj = periodInteger.doubleValue();
							final double Cj = exeInteger.doubleValue();
							final BigDecimal TjBD = BigDecimal.valueOf(Tj);
							final BigDecimal CjBD = BigDecimal.valueOf(Cj);

							// flocalSum = Math.ceil(fl_value / Tj) * Cj;
							flocalSum = fl_value.divide(TjBD, 0, BigDecimal.ROUND_UP).multiply(CjBD);

							fSum = fSum.add(flocalSum);
						}
						final double Ci = AmaltheaServices.convertToPicoSeconds(thisExeTime).doubleValue();
						// Fvalue of 1 iteration
						// fl_value = fSum + j * Ci;
						fl_value = BigDecimal.valueOf(fSum.doubleValue() + j * Ci);
					}
					// Final fl_value landed here
					final double this_rt = fl_value.doubleValue() - (j - 1) * AmaltheaServices.convertToPicoSeconds(period).doubleValue();
					rtList.add(this_rt);
				}
				final double rpt = Collections.max(rtList);
				final Time responseTime = FactoryUtil.createTime(BigInteger.valueOf((long) rpt), TimeUnit.PS);
				if (thisRT.compareTo(FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS)) > 0) {
					this.cumuAcTime = this.cumuAcTime.add(rtaut.getTaskMemoryAccessTime(thisTask, pu, executionCase));
				}
				return responseTime;
			}
		}
		return null;
	}

	/**
	 * Calculate the worst-case response time of the observed task according to the implicit periodic tasks response time analysis algorithm.
	 * @param task				the observed task
	 * @param taskList			list of tasks that is mapped to the same core
	 * @param executionCase		BCET, ACET, WCET
	 * @param pu				ProcessingUnit that would compute the given runnable (A57 or Denver)
	 * @param cpurta			the instance of CPURta class that contains model & mapping IA info
	 * @return
	 * 				the worst-case response time of the observed task (implicit communication paradigm)
	 */
	public Time implicitPreciseTest(final Task task, final List<Task> taskList, final TimeType executionCase, 
			final ProcessingUnit pu, final CPURta cpurta) {
		Time thisRT = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
		Time period = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
		if (taskList.size() == 0) {
			log.debug("!!! This taskList is empty so I am returning MAX !!!");
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		/* to check if the given task is in the taskList */
		int flag = 0;
		int index = 0;
		for (int i = 0; i < taskList.size(); i++) {
			if (task.equals(taskList.get(i))) {
				flag = 1;
				index = i;
				break;
			}
		}
		if (flag == 0) {
			log.debug("!!! Nothing in the taskList matches the given Task !!! So I am returning 0s" + " --- thisTask: " + task.getName());
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		final RTARuntimeUtil rtaut = new RTARuntimeUtil();
		for (int i = 0; i < index + 1; i++) {
			period = CommonUtils.getStimInTime(taskList.get(i));
			if (index == 0) {
				thisRT = rtaut.getExecutionTimeforCPUTaskIA(taskList.get(i), pu, executionCase, cpurta);
				final Time[] ta = rtaut.getLocalCopyTimeArray(taskList.get(i), pu, executionCase, cpurta);
				for (int j = 0; j < ta.length; j++) {
					thisRT = thisRT.add(ta[j]);
				}
				if (thisRT.compareTo(period) <= 0) {
					return thisRT;
				}
				log.debug("!!! This is non schedulable...!!! Because of the period " + period + " being less than the execution time of " + thisRT
						+ " for task " + task.getName());
				return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
			} else if (i == index) {
				/* In the case of a COOPERATIVE Preemption typed Task */
				Time thisExeTime = rtaut.getExecutionTimeforCPUTaskIA(taskList.get(i), pu, executionCase, cpurta);
				final Time[] ta = rtaut.getLocalCopyTimeArray(taskList.get(i), pu, executionCase, cpurta);
				for (int j = 0; j < ta.length; j++) {
					thisExeTime = thisExeTime.add(ta[j]);
				}
				if (thisExeTime.compareTo(FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS)) == 0) {
					return thisExeTime;
				} else if (thisExeTime.compareTo(period) > 0) {
					log.debug("!!! This is non schedulable...!!! Because of the period " + period + " being less than the execution time of " + thisRT
							+ " for task " + task.getName());
					return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
				}
				Time cumulativeRT = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
				/* 1. add all the execution time till the index */
				for (int j = 0; j < i + 1; j++) {
					Time thisTime = rtaut.getExecutionTimeforCPUTaskIA(taskList.get(j), pu, executionCase, cpurta);
					final Time[] ta_ = rtaut.getLocalCopyTimeArray(taskList.get(j), pu, executionCase, cpurta);
					for (int k = 0; k < ta_.length; k++) {
						thisTime = thisTime.add(ta[k]);
					}
					cumulativeRT = cumulativeRT.add(thisTime);
				}
				if (cumulativeRT.compareTo(period) <= 0) {
					while (true) {
						Time excepThisExeTime = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
						for (int j = 0; j < i; j++) {
							Time localPeriod = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
							localPeriod = CommonUtils.getStimInTime(taskList.get(j));
							Time preExeTime = rtaut.getExecutionTimeforCPUTaskIA(taskList.get(j), pu, executionCase, cpurta);
							final Time[] ta_ = rtaut.getLocalCopyTimeArray(taskList.get(j), pu, executionCase, cpurta);
							for (int k = 0; k < ta_.length; k++) {
								preExeTime = preExeTime.add(ta_[k]);
							}
							final double ri_period = Math.ceil(cumulativeRT.divide(localPeriod));
							excepThisExeTime = excepThisExeTime.add(preExeTime.multiply(ri_period));
						}
						Time cumulativeRT_x = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
						cumulativeRT_x = thisExeTime.add(excepThisExeTime);
						if (cumulativeRT_x.compareTo(period) <= 0) {
							if (cumulativeRT_x.compareTo(cumulativeRT) == 0) {
								thisRT = cumulativeRT_x;
								return thisRT;
							}
							cumulativeRT = cumulativeRT_x;
						} else {
							log.debug("!!! This is non schedulable...!!! Because of the period " + period
									+ " being less than the response time (cumulativeRT_x) of " + cumulativeRT_x + " for task " + task.getName());
							if (SharedConsts.ignoreInfeasibility) {
								return cumulativeRT_x;
							}
							return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
						}
					}
				}
				log.debug("!!! This is non schedulable...!!! Because of the period " + period + " being less than the response time of " + cumulativeRT
						+ " for task " + task.getName());
				if (SharedConsts.ignoreInfeasibility) {
					return cumulativeRT;
				}
				return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
			}
		}
		return thisRT;
	}

	/**
	 * Calculate response time of the observed task according to the best-case RMS response time analysis algorithm with Implicit Communication.
	 * (source: https://www.researchgate.net/publication/3958297_Exact_best-case_response_time_analysis_of_fixed_priority_scheduled_tasks (equ_16))
	 * @param task				the observed task
	 * @param taskList			list of tasks that is mapped to the same core
	 * @param pu				ProcessingUnit that would compute the given runnable (A57 or Denver)
	 * @return
	 * 			the best-case response time of the observed task with Implicit Communication
	 */
	public Time implicitBCCPURT(final Task task, final List<Task> taskList, final ProcessingUnit pu, final CPURta cpurta) {
		Time bcrt = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
		if (taskList.size() == 0) {
			log.debug("!!! This taskList is empty so I am returning MAX !!!");
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		} else if (!taskList.contains(task)) {
			log.debug("!!! This taskList is empty so I am returning MAX !!!");
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		/* to check if the given task is in the taskList */
		int flag = 0;
		int index = 0;
		for (int i = 0; i < taskList.size(); i++) {
			if (task.equals(taskList.get(i))) {
				flag = 1;
				index = i;
				break;
			}
		}
		if (flag == 0) {
			log.debug("!!! Nothing in the taskList matches the given Task !!! So I am returning 0s" + " --- thisTask: " + task.getName());
			return FactoryUtil.createTime(BigInteger.valueOf(Long.MAX_VALUE), TimeUnit.PS);
		}
		final RTARuntimeUtil rtaut = new RTARuntimeUtil();
		Time bR_0 = this.implicitPreciseTest(task, taskList, TimeType.BCET, pu, cpurta);
		for (int i = 0; i < index + 1; i++) {
			if (index == 0) {
				bcrt = bR_0;
				break;
			}
			else if (i == index) {
				Time i_BCET = rtaut.getExecutionTimeforCPUTaskIA(task, pu, TimeType.BCET, this);
				final Time[] ta = rtaut.getLocalCopyTimeArray(task, pu, TimeType.BCET, this);
				for (int j = 0; j < ta.length; j++) {
					i_BCET = i_BCET.add(ta[j]);
				}
				final Time rt_0 = this.implicitPreciseTest(task, taskList, TimeType.BCET, pu, cpurta);
				Time rt_n = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
				boolean isItFirst = true;
				while (true) {
					Time sum = FactoryUtil.createTime(BigInteger.ZERO, TimeUnit.PS);
					for (int j = 0; j < i; j++) {
						final Task curTask = taskList.get(j);
						final Time curTask_period = CommonUtils.getStimInTime(curTask);
						Time curTask_BCET = rtaut.getExecutionTimeforCPUTaskIA(curTask, pu, TimeType.BCET, this);
						final Time[] ta_ = rtaut.getLocalCopyTimeArray(curTask, pu, TimeType.BCET, this);
						for (int k = 0; k < ta_.length; k++) {
							curTask_BCET = curTask_BCET.add(ta[k]);
						}
						if (isItFirst) {
							double ceil_value = Math.ceil((rt_0.subtract(curTask_period)).divide(curTask_period));
							if (ceil_value < 0) {
								ceil_value = 0;
							}
							final Time curIterTime = curTask_BCET.multiply(ceil_value);
							sum = sum.add(curIterTime);
						} else {
							double ceil_value = Math.ceil((rt_n.subtract(curTask_period)).divide(curTask_period));
							if (ceil_value < 0) {
								ceil_value = 0;
							}
							final Time curIterTime = curTask_BCET.multiply(ceil_value);
							sum = sum.add(curIterTime);
						}
					}
					Time temp = i_BCET.add(sum);
					System.out.println("sum: " + sum + ", temp: " + temp);
					if(isItFirst) {
						if (temp.compareTo(rt_0) == 0) {
							bcrt = temp;
							break;
						} else {
							rt_n = temp;
						}
					} else {
						if (temp.compareTo(rt_n) == 0) {
							bcrt = temp;
							break;
						} else {
							rt_n = temp;
						}
					}
					isItFirst = false;
				}
			}
		}
		return bcrt;
	}

	/**
	 * get the processing unit that the observed task is mapped to
	 * @param task			the observed task
	 * @param cpurta		the instance of CPURta class
	 * @return
	 * 				pu
	 */
	protected ProcessingUnit getPU(final Task task, final CPURta cpurta) {
		final int tindex = cpurta.getModel().getSwModel().getTasks().indexOf(task);
		final int puindex = cpurta.getIA()[tindex];
		final ProcessingUnit pu = cpurta.getPUl().get(puindex);
		return pu;
	}

	/**
	 * get the sorted task listed based on the given integer array mapping
	 * @param task			the observed task
	 * @param pu			ProcessingUnit that would compute the given method
	 * @param cpurta		the instance of CPURta class
	 * @return
	 * 				sortedTaskList
	 */
	protected List<Task> getSortedTaskList(final Task task, final ProcessingUnit pu, final CPURta cpurta) {
		final int puIndex = cpurta.getPUl().indexOf(pu);
		/* 2. get all tasks mapped to this CPU */
		final List<Task> puTaskList = new ArrayList<Task>();
		for (int i = 0; i < cpurta.getIA().length; i++) {
			if (cpurta.getIA()[i] == puIndex) {
				puTaskList.add(cpurta.getModel().getSwModel().getTasks().get(i));
			}
		}
		final List<Task> sortedTaskList = taskSorting(puTaskList);
		return sortedTaskList;
	}
	
	/**
	 * get a list of tasks that are mapped to the same core and have a higher priority than the observed task has
	 * @param task			the observed task
	 * @param cpurta		the instance of CPURta class
	 * @return
	 * 				hpList
	 */
	protected List<Task> getHigherPriorityTasks(final Task task, CPURta cpurta) {
		final ProcessingUnit pu = getPU(task, cpurta);
		final List<Task> hpList = getSortedTaskList(task, pu, cpurta);
		final int index = hpList.indexOf(task);
		final int count = hpList.size() - index;
		for (int i = 0; i < count; i++) {
			hpList.remove(hpList.size()-1);
		}
		return hpList;
	}
	
	/**
	 * get a list of tasks that are mapped to the same core and have a higher priority than the observed task has
	 * @param task			the observed task
	 * @param cpurta		the instance of CPURta class
	 * @return
	 * 				hpList
	 */
	protected List<Task> getHigherPriorityTasksInChain(final Task task, final List<Task> chain) {
		if (!chain.contains(task)) {
			System.out.println("ERROR: The observed task chain does not contain the observed task.");
			return null;
		}
		List<Time> periodList = chain.stream().map(s -> CommonUtils.getStimInTime(s)).collect(Collectors.toList());
		/* Sorting (Shortest Period(Time) first) */
		Collections.sort(periodList, new TimeCompIA());
		final List<Task> sortedChain = new ArrayList<Task>();
		for (int i = 0; i < periodList.size(); i++) {
			final Time thisPeriod = periodList.get(i);
			for (int j = 0; j < chain.size(); j++) {
				final Task thisTask = chain.get(j);
				if (thisPeriod.compareTo(CommonUtils.getStimInTime(thisTask)) == 0) {
					if (!sortedChain.contains(thisTask)) {
						sortedChain.add(thisTask);
						break;
					}
				}
			}
		}
		final int index = sortedChain.indexOf(task);
		final List<Task> hpList = new ArrayList<Task>();
		for (int i = 0; i < index; i++) {
			hpList.add(sortedChain.get(i));
		}
		return hpList;
	}
	
	/**
	 * Visibility - public (This method is for EffectChainLatency)
	 * It returns HashMap<Integer, List<Task>> Type reference that contains an Integer(number of Processing Unit index) and the corresponding List of Tasks
	 * This is to visualize which task is mapped to which processing unit.
	 * @return
	 * 			HashMap<Integer, List<Task>> puListHashMap
	 */
	public HashMap<Integer, List<Task>> getPUTaskListHashMap(final Amalthea model) {
		final HashMap<Integer, List<Task>> puListHashMap = new HashMap<>();
		final EList<Task> allTaskList = model.getSwModel().getTasks();
		for (int i = 0; i < this.pul.size(); i++) {
			final List<Task> puTaskList = new ArrayList<Task>();
			for (int j = 0; j < this.tpuMapping.length; j++) {
				final int puIndex = this.tpuMapping[j];
				if (i == puIndex) {
					puTaskList.add(allTaskList.get(j));
				}
			}
			puListHashMap.put(i, taskSorting(puTaskList));
		}
		return puListHashMap;
	}

	@Override
	public Time getRT(final Task t, final TimeType timeType) {
		if (null == getTRT().get(t)) {
			getRTSum(timeType);
		}
		return getTRT().get(t);
	}
}

/**
 * @author Junhyung Ki
 *			this inner class is used for the method "taskSorting" to help compare between two tasks' periods (which is longer)
 */
class TimeCompIA implements Comparator<Time> {
	@Override
	public int compare(final Time arg0, final Time arg1) {
		if (arg0.compareTo(arg1) < 0) {
			return -1;
		}
		return 1;
	}
}
