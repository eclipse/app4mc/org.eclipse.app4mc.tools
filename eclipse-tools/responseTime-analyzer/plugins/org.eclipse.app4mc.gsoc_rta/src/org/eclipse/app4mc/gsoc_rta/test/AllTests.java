package org.eclipse.app4mc.gsoc_rta.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CPURtaTest.class, E2ELatencyTest_Single.class, E2ELatencyTest_Multi.class })

public class AllTests {
	// empty class to start test suite
}
