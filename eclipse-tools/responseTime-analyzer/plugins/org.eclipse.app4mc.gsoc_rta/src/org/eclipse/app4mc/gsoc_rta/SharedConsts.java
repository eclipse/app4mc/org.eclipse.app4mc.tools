/*******************************************************************************
 * Copyright (c) 2019 Dortmund University of Applied Sciences and Arts.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     FH Dortmund - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.gsoc_rta;

import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType;

public class SharedConsts {

	public static int RUNTIMESEC = 2 * 60 * 60;
	public static final String SINGLE_CORE_MODEL = "model-input/SingleTCs/SingleTCs.amxmi";
	
	/*-----------Measurement Configuration--------------–*/
	public static TimeType timeType = TimeType.WCET;
	public static boolean onlyWrittenLabelsCE = true;
	public static boolean synchronousOffloading = false;
	public static boolean useModelTimeSlices = false;
	public static boolean ignoreInfeasibility = false;
	public static int[] timeSlices;
	public static long timeSliceLengthPS = 1000000000l;
	public static boolean levelIBusyPeriod = false;
	public static TS_DERIV tsDeriv = TS_DERIV.TSxPrio;
	public static OPT_TYPE optimize = OPT_TYPE.RESPONSETIMESUM;
	public static final boolean considerGlobalBlocking = false;
	public static final boolean considerLocalBlocking = false;
	public static boolean offsetCPURTA = true;
	public static final boolean zeroHigh = true;
	/*-----------End Measur. Configuration--------------–*/

	public enum OPT_TYPE {
		TASKCHAINS, RESPONSETIMESUM, LOADBALANCING
	}

	public enum TS_DERIV {
		TS, TSxPrio, TSxUtil, TSxUtilxNbTasks
	}

	public enum ComParadigm {
		DIRECT, IMPLICIT
	}
}
