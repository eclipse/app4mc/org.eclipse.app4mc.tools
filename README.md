
# Eclipse APP4MC - Tools

This is the repository of additional Eclipse APP4MC tool components:

    eclipse-tools/emf-graphical-viewer  | Generic graphical viewer for EMF (meta-) models
    eclipse-tools/sca2amalthea          | Model generation based on sourcecode analysis

## Build

The project uses Maven/Tycho to build.

The builds create a P2 update site.

From the root folder of the components:

```
$ mvn clean verify
```
You need Maven 3.6.3 or higher to perform the POM-less Tycho build.

## License

[Eclipse Public License (EPL) v2.0][1]

[1]: https://www.eclipse.org/legal/epl-2.0/